<?php declare(strict_types = 1);
/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180219193552 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6F89329D25');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6F89329D25 FOREIGN KEY (resource_id) REFERENCES resources (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6F89329D25');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6F89329D25 FOREIGN KEY (resource_id) REFERENCES resources (id)');
    }
}
