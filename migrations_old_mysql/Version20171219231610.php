<?php declare(strict_types=1);
/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171219231610 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('
            CREATE TABLE `options` (
              `id` int(11) NOT NULL,
              `option_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT \'Key of the option\',
              `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT \'Value of selected key\'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
            
            ALTER TABLE `options`
              ADD PRIMARY KEY (`id`),
              ADD UNIQUE KEY `UNIQ_D035FA873CEE7BEE` (`option_key`);
            
            
            ALTER TABLE `options`
              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
              ');
    }

    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE IF EXISTS `options`');
    }
}
