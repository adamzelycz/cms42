<?php declare(strict_types = 1);
/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171222163652 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE urls (id INT AUTO_INCREMENT NOT NULL, presenter VARCHAR(255) DEFAULT NULL COMMENT \'Destination Presenter (with modules)\', action VARCHAR(255) DEFAULT NULL COMMENT \'Destination action of Presenter\', url_path VARCHAR(255) NOT NULL COMMENT \'Fake path (URL)\', internal_id INT DEFAULT NULL COMMENT \'Internal id passed to the action method (optional)\', UNIQUE INDEX UNIQ_2A9437A1D3B56AC8 (presenter), UNIQUE INDEX UNIQ_2A9437A1150042B7 (url_path), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE urls');
    }
}
