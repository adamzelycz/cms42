<?php declare(strict_types = 1);
/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180128113912 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE permissions (id INT AUTO_INCREMENT NOT NULL, resource_id INT DEFAULT NULL, role_id INT DEFAULT NULL, allow TINYINT(1) NOT NULL, `read` TINYINT(1) NOT NULL, INDEX IDX_2DEDCC6F89329D25 (resource_id), INDEX IDX_2DEDCC6FD60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE resources (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COMMENT \'Unique resource string\', UNIQUE INDEX UNIQ_EF66EBAE5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6F89329D25 FOREIGN KEY (resource_id) REFERENCES resources (id)');
        $this->addSql('ALTER TABLE permissions ADD CONSTRAINT FK_2DEDCC6FD60322AC FOREIGN KEY (role_id) REFERENCES resources (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6F89329D25');
        $this->addSql('ALTER TABLE permissions DROP FOREIGN KEY FK_2DEDCC6FD60322AC');
        $this->addSql('DROP TABLE permissions');
        $this->addSql('DROP TABLE resources');
    }
}
