<?php declare(strict_types = 1);
/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180215223041 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE option_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COMMENT \'Type of option for Form\', UNIQUE INDEX UNIQ_64D0FDCA5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE options ADD option_type_id INT DEFAULT NULL, ADD name VARCHAR(255) NOT NULL COMMENT \'Human readable name of settings\'');
        $this->addSql('ALTER TABLE options ADD CONSTRAINT FK_D035FA87DDB89BE6 FOREIGN KEY (option_type_id) REFERENCES option_type (id)');
        $this->addSql('CREATE INDEX IDX_D035FA87DDB89BE6 ON options (option_type_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE options DROP FOREIGN KEY FK_D035FA87DDB89BE6');
        $this->addSql('DROP TABLE option_type');
        $this->addSql('DROP INDEX IDX_D035FA87DDB89BE6 ON options');
        $this->addSql('ALTER TABLE options DROP option_type_id, DROP name');
    }
}
