$(function () {
    $('.flashes--nonajax').delay(3000).fadeOut(300); //Static Flash messages hide
    $('.leave-confirm').on('click', function () {
        return confirm("Do you really discard changes and leave a page?");
    }); //TODO AJAX
});

//NITTRO ROUTES

window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.character-counter')
        .on('match', function (evt) {
            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    let counters = $('.character-counter');
                    counters.after('<h6 class="character-count pull-right"></h6>');
                    counters.keyup(function () {
                        let textMax = $(this).attr('maxlength');
                        let countMessage = $(this).next();
                        countMessage.html(textMax + ' remaining');
                        let textLength = $(this).val().length;
                        let textRemaining = textMax - textLength;

                        countMessage.html(textRemaining + ' remaining');
                    });
                })
        });

    router.getDOMRoute('.modal--dismiss')
        .on('match', function (evt) {
            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    $('.modal--dismiss').on('click', function () {
                        $('#' + $(this).data('dismiss')).modal('hide');
                    });
                })
        });
}, {
    DOM: 'Utils.DOM'
}]);
