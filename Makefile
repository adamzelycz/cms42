# Project: CMS42
# Author: Adam Zelycz
# Date: 9. 1. 2018

deploy: cs lint analyse schema reserved-words test acceptance

make install-prod: clean-cache composer-prod da set-permissions migrations-migrate set-permissions fixtures clean-dev cron acl

make install-prod-docker: clean-cache composer-prod-docker da-docker set-permissions-sudo migrations-docker set-permissions-sudo fixtures-docker clean-dev cron-docker acl

make update-prod: composer-prod da clean-cache set-permissions-sudo migrations-docker set-permissions-sudo clean-dev cron-docker

make install: clean-cache composer-dev reset da-dev acl cron

make cron: cron-start cron-stats-load

make cron-docker: cron-start-docker cron-stats-load-docker

start-dev:
	docker-compose -f docker-compose.yml -f docker/development.yml up -d --force-recreate

start-prod:
	docker-compose -f docker-compose.yml -f docker/production.yml up -d --force-recreate

build-prod:
	docker-compose -f docker-compose.yml -f docker/production.yml build

build-dev:
	docker-compose -f docker-compose.yml -f docker/development.yml build

cron-start-docker:
	docker exec cms42_php make cron-start

cron-start:
	service cron start || service cron start

enter:
	docker exec --user aidam -it cms42_php bash

enter-prod:
	docker exec -it cms42_php bash

cron-stats-load-docker:
#	crontab -l | { cat; echo "* * * * * /var/www/bin/console stats:load"; } | crontab -
	docker exec cms42_php bash -c 'make cron-stats-load'

cron-stats-load:
	echo "0 * * * * /var/www/bin/console stats:load" | crontab -

stop-dev:
	docker-compose -f docker-compose.yml -f docker/development.yml stop

stop-prod:
	docker-compose -f docker-compose.yml -f docker/production.yml stop

composer-prod:
	rm -Rf vendor/*
	rm -Rf .composer
	composer install --no-dev

composer-prod-docker:
	docker exec cms42_php make composer-prod

composer-dev:
	composer install

clean-cache:
	rm -Rf temp/cache

set-permissions-sudo:
	docker exec cms42_php make set-permissions

migrations-docker:
	docker exec cms42_php make migrations-migrate

migrations-migrate:
	bin/console migrations:status
	bin/console migrations:migrate --no-interaction

fixtures-docker:
	docker exec cms42_php make fixtures

fixtures:
	bin/console fixtures:load -q --no-interaction

#SUDO
set-permissions:
	chown -R www-data .
	chmod -R g+s .
	chmod -R a+rw temp log
	chmod +x bin/console
	chmod -R 777 docker/db-data/redis

acl:
	setfacl -d -m o::rw log

clean-dev:
	rm -Rf tests migrations_old_mysql stack ruleset.xml phpstan.neon .gitlab-ci.yml dynamicReturnTypeMeta.json
	find . -name ".gitignore" -exec rm -rf {} \;

reset:
	composer dump-autoload -o
	bin/console orm:schema-tool:drop --force --no-interaction
	-bin/console migrations:reset --no-interaction
	bin/console migrations:migrate --no-interaction
	bin/console fixtures:load --no-interaction

analyse:
	vendor/bin/phpstan analyse -c phpstan.neon src plugins app tests/Unit tests/Mocks tests/Helpers tests/Traits --level 7

test:
	vendor/bin/tester -c tests/unix.ini tests/Unit

coverage:
	"vendor/bin/tester -c tests/coverage/coverage.ini tests/Unit --coverage tests/coverage/app.html --coverage-src app"
	"vendor/bin/tester -c tests/coverage/coverage.ini tests/Unit --coverage tests/coverage/src.html --coverage-src src"
	"vendor/bin/tester -c tests/coverage/coverage.ini tests/Unit --coverage tests/coverage/plugins.html --coverage-src plugins"

lint:
	find app src plugins tests -type f \( -iname \*.php -o -iname \*.phpt \) -exec php -l {} \; | (! grep -v "No syntax errors detected" )

schema:
	php bin/console orm:validate-schema

acceptance:
	php -S localhost:8085 --docroot www &>/dev/null&
	vendor/bin/codecept -c tests/Codeception run acceptance $(NAME)

cest:
	vendor/bin/codecept -c tests/Codeception/ generate:cest acceptance $(NAME)

da:
	composer dump-autoload -o --no-dev

da-docker:
	docker exec cms42_php make da

da-dev:
	composer dump-autoload -o

cs:
	vendor/bin/phpcs --standard=ruleset.xml --extensions=php,phpt --encoding=utf-8 --colors -sp src tests/Unit tests/Helpers tests/Mocks tests/Traits app plugins

cf:
	vendor/bin/phpcbf --standard=ruleset.xml --extensions=php,phpt --encoding=utf-8 --colors -sp src tests/Unit tests/Helpers tests/Mocks tests/Traits app plugins

reserved-words:
	php bin/console dbal:reserved-words
