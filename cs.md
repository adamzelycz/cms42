CMS42 [EN](readme.md) [CS]
=========================
![alt text](www/static/cms_logo.png "CMS42 Logo")

----> [LIVE DEMO](https://cms42.adamzelycz.cz "CMS42 - Adam Zelycz") <---- 
--------
**Pro vstup do administrace použijte přihlašovací údaje: jméno: _user_, heslo: _user_.
Z pochopitelných důvodů však s těmito údaji nelze provádět všechny operace a vstupovat do některých sekcí
administrace! Pokud chcete administrátorská oprávnění, [napište mně](mailto:contact@adamzelycz.cz)**.

O projektu
--------
Tento CMS (Content Management System) vyvíjím jen tak pro zábavu a slouží jen jako případná ukázka mé tvorby.
Celá aplikace běží na PHP7.2 a frameworku [Nette](https://nette.org/cs/ "Nette Framework")

CMS není dokončen a pravděpodobně ani nikdy nebude, pracuji na něm pouze příležitostně. Přesto však
poskytuje celou řadu funkcí. Na té veřejně přístupné části naleznete sice pouze kontaktní a přihlašovací
formulář, ale nejvíce vychytávek se skrývá právě v administraci. Ta obsahuje obecná nastavení, ale také
pokročilou práci s oprávněním na základě rolí, zdrojů a práv.

Také zde najdete datagridy pro práci s uživateli, URL adresami nebo třeba jazyky.
Nachází se zde také pokročilý WYSIWYG editor stránek včetně vkládání dynamických prvků (komponent).

Do CMS je možné stahovat pluginy a rozšiřovat tak funkce. Systém komunikuje s extérní aplikací, která
přes REST API poskytuje procházení katalogu s pluginy a samozřejmě také jejich stahování a updatování. Těchto
pluginů sice není mnoho (pouze 3.... a z toho jeden pouze testovací). Ale nikomu teoreticky nebrání nějaký
plugin vytvořit :D.

Pro Front-endovou část byl použit Bootstrap 4. Platí však, že se dá vzhled libovolně přizpůsobovat a to
buď vlastním CSS nebo stáhnutím již hotové šablony přes systém pluginů. (Tedy kdyby nějaké šablony existovaly :D)

Nepřehlédnutelnou součástí je také plně Ajaxové procházení stránek. K tomu jsem využil
[Nittro](https://www.nittro.org/ "Nittro Framework"). To lze zase v případě potřeby vypnout přímo v administraci.

Použité technologie
--------
- Doctrine 2 (Kdyby/Doctrine)
- Monolog
- Guzzle + OAuth2
- Doctrine migrations, fixtures
- PHPStan
- Mockery
- Nette Tester
- Codeception
- Slevomat/CodingStandards
- Gitlab CI
- Gulp
- PhpStorm
- Docker
- PostgreSQL
- Blackfire 
- Nginx
- ....

O mně
--------
Ahoj! Jmenuji se Adam Zelycz. Mým největším koníčkem, hned po trávení času s
[mým pejskem](https://www.adamzelycz.cz/images/aileenka.jpg "AIleenka") je programování.

S programováním v PHP jsem začal na [střední škole](http://www.vda.cz/ "VDA.cz"). Nyní jsem plně ponořen
do programování
webových aplikací s využitím frameworku [Nette](https://nette.org/cs/ "Nette Framework").
Také aktuálně sháním part-time job při mém studiu [FIT VUT](http://www.fit.vutbr.cz/ "FIT VUT") v Brně.

Vždy se snažím psát čistý kód a neustále se zlepšovat.

Více info o mně naleznete na: [https://www.adamzelycz.cz](https://www.adamzelycz.cz "Adam Zelycz")

Kontakt: [kontakt@adamzelycz.cz](mailto:kontakt@adamzelycz.cz) nebo
[kontaktní formulář](https://www.adamzelycz.cz#contact)

Požadavky
--------
- PHP >= 7.2
- PostgreSQL (9.6 doporučené)
- Redis
- Nginx
- _!!!! Nebo použijte Docker !!! :)_

Instalace
--------
**Nejdříve se ujistěte, že splňujete požadavky**
1. Clone this repository - `git clone https://gitlab.com/adamzelycz/cms42.git`
2. Find configuration file in `app/config/config.local.template.neon` and rename it to
`app/config/config.local.neon`. After that Fill this file with correct data (like database connection).
If you use Docker, also fill file `docker/production.yml`.
3. If you use docker, run containers using
`docker-compose -f docker-compose.yml -f docker/production.yml up` or simply `make start-prod`.
4. Run `make install-prod` or `make install-prod-docker`.
5. It is recommended to also edit configuration file `app/config/config.neon` to your needs.

Pokud se objeví nějaké problémy, neváhejte mě [kontaktovat](mailto:kontakt@adamzelycz.cz).
