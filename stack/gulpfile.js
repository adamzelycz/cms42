var gulp = require("gulp"),
    concat = require("gulp-concat"),
    RevAll = require('gulp-rev-all'),
    del = require('del'),
    browserSync = require('browser-sync').create();

var config = {
    filesCss: [
        "../www/css/style.css"
    ],
    build: "../www/assets",
    filesJs: [
        "../www/js/popper.min.js",
        "../www/js/bootstrap.min.js",
        "../www/js/front.js",
    ]
};

gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: {
            target: "nginx",//custom proxy (Docker image)
            ws: true
            // proxyOptions: { changeOrigin: false },
        }
    });
});

gulp.task("styles", function () {
    var cleanCss = require('gulp-clean-css'),
        autoprefixer = require("autoprefixer"),
        postcss = require('gulp-postcss');

    return gulp.src(config.filesCss)
        .pipe(concat("style.css"))
        .pipe(postcss([autoprefixer({browsers: ['last 2 versions']})]))
        .pipe(cleanCss({compatibility: "ie8"}))
        .pipe(RevAll.revision())
        .pipe(gulp.dest(config.build))
});

gulp.task("scripts", function () {
    var uglify = require("gulp-uglify");

    return gulp.src(config.filesJs)
        .pipe(concat("front.js"))
        .pipe(uglify({mangle: false}))
        .pipe(RevAll.revision())
        .pipe(gulp.dest(config.build))
});

gulp.task("revision", function () {
    gulp
        .src("../www/assets/*.css")
        .pipe(RevAll.revision())
        .pipe(gulp.dest("../www/assets"));
});

gulp.task('cleanBuild', function () {
    del([config.build + "/*.js", config.build + "/*.css"], {force: true})
});

//sledovátory
gulp.task('watch', ['browser-sync'], function () {
    gulp.watch('../app/**/*.latte').on('change', browserSync.reload);//sledování latte a reload
    gulp.watch('../src/**/*.latte').on('change', browserSync.reload);
    gulp.watch('../plugins/**/*.latte').on('change', browserSync.reload);
    gulp.watch('../www/css/*.css').on('change', browserSync.reload);//sledování css a reload
    gulp.watch('../www/js/*.js').on('change', browserSync.reload);
});

gulp.task('deploy', ["cleanBuild", "styles", "scripts"]);