<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use function class_exists;

class PluginsFixture extends AbstractFixture
{

    /**
     * @param ObjectManager $manager
     * @inheritdoc
     */
    public function load(ObjectManager $manager): void
    {
        $aidamAidam = '\Aidam\Aidam\DI\AidamExtension';
        if (class_exists($aidamAidam)) {
            $plugin = (new $aidamAidam())->getPlugin();
            $manager->persist($plugin);
        }

        $aidamNittro = '\Aidam\Nittro\DI\NittroExtension';
        if (class_exists($aidamNittro)) {
            $plugin = (new $aidamNittro())->getPlugin();
            $manager->persist($plugin);
        }

        $manager->flush();
    }

}
