<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Navigation\Menu;
use Navigation\MenuItem;
use Url\Url;

class MenuFixture extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        /** @var Url $contactPageUrl */
        $contactPageUrl = $this->getReference('page_contact_url');
        /** @var Url $homePageUrl */
        $homePageUrl = $this->getReference('page_home_url');

        $menus = [];

        $menu = new Menu();
        $menu->setName('Default');

        $item = new MenuItem();
        $item->setTitle('_homepage.title');
        $item->setInternalUrl($homePageUrl);
        $menu->addItem($item);

        $item = new MenuItem();
        $item->setTitle('_contact.title');
        $item->setInternalUrl($contactPageUrl);
        $menu->addItem($item);

//        $item = new MenuItem();
//        $item->setTitle('WWW');
//        $item->setTarget(new MenuItemTarget(MenuItemTarget::BLANK));
//        $item->setExternalUrl(new ExternalUrl('https://www.adamzelycz.cz/'));
//        $menu->addItem($item);

        $menu->select();
        $menus[] = $menu;

        foreach ($menus as $menu) {
            $manager->persist($menu);
        }
        $manager->flush();
    }

    /** @return string[] */
    public function getDependencies(): array
    {
        return [
            UrlFixture::class,
            PagesFixture::class,
        ];
    }

}
