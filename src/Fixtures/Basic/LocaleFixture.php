<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Locale\Locale;

class LocaleFixture extends AbstractFixture
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $cs = new Locale('cs', 'čeština', true);
        $en = new Locale('en', 'english');

        $this->addReference('locale_cs', $cs);
        $this->addReference('locale_en', $en);

        $manager->persist($cs);
        $manager->persist($en);
        $manager->flush();
    }

}
