<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Contact\Components\ContactForm\IContactFormFactory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Locale\Locale;
use Pages\Components\DynamicControls\DynamicControls;
use Pages\Page;
use Pages\PageContent;
use Pages\PageLocale;
use Url\Url;

class PagesFixture extends AbstractFixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     * @inheritdoc
     */
    public function load(ObjectManager $manager): void
    {
        /** @var Locale $cs */
        $cs = $this->getReference('locale_cs');
        /** @var Locale $en */
        $en = $this->getReference('locale_en');

        //---- CONTACTS ----
        $page = new Page();

        //locales
        //cs
        $pageLocale = new PageLocale($page, $cs);
        $pageLocale->setTitle('Kontakty');
        $pageLocale->setDescription('Kontaktní formulář');
        $content = new PageContent();
        $content->setContent(
            '<p>' . DynamicControls::generateName(IContactFormFactory::class) . '</p>' .
            '<p><em class="ql-size-small">Svou zprávu můžete šifrovat pomocí </em><a href="key.asc" target="_blank" class="ql-size-small"><em>tohoto klíče</em></a><em class="ql-size-small">. Nebo rovnou využijte </em><a href="https://keybase.io/encrypt#adamzelycz" target="_blank" class="ql-size-small"><em>Keybase.io</em></a><em class="ql-size-small">.</em></p>'
        );
        $pageLocale->addContent($content);
        $manager->persist($pageLocale);

        //en
        $pageLocale = new PageLocale($page, $en);
        $pageLocale->setTitle('Contacts');
        $pageLocale->setDescription('Contact form');
        $content = new PageContent();
        $content->setContent(
            '<p>' . DynamicControls::generateName(IContactFormFactory::class) . '</p>' .
            '<p><em class="ql-size-small">You can encrypt your message with </em><a href="key.asc" target="_blank" class="ql-size-small" style="background-color: rgb(249, 250, 251);"><em>this key</em></a><em class="ql-size-small">. Or just use </em><a href="https://keybase.io/encrypt#adamzelycz" target="_blank" class="ql-size-small" style="background-color: rgb(249, 250, 251);"><em>Keybase.io</em></a><em class="ql-size-small" style="background-color: rgb(249, 250, 251);">.</em></p>'
        );
        $pageLocale->addContent($content);
        $manager->persist($pageLocale);

        //save page
        $page->publish();
        $manager->persist($page);
        $manager->flush();

        //url
        $url = new Url();
        $url->setInternalId($page->getId());
        $url->setDestination('Pages:Front:Pages');
        $url->setUrlPath('contact');

        //save url
        $manager->persist($url);
        $page->setUrl($url);
        $manager->persist($page);
        $this->addReference('page_contact_url', $url);

        //---- HOMEPAGE ----
        $page = new Page();

        //locales
        //cs
        $pageLocale = new PageLocale($page, $cs);
        $pageLocale->setTitle('Úvodní strana');
        $pageLocale->setDescription('Systém pro správu obsahu CMS42 vytvořený jen tak pro zábavu a procvičení programování v PHP.');
        $content = new PageContent();
        $content->setContent('<div class="homepage">
        <div class="pb-4"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgUAAACBCAYAAABQIsnqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAB6lSURBVHhe7Z0NbBXXlcfvGJNCncXGYOxQDCmCGC+bGLKsC4kKBNrNpihhiaoUWCGSqiBHyqabkkrZpFZVuclWatgkTSRYGjWwaMFbodKQRpQWSEBqASvL10INgabYpoBt4uIoXtzGfrP+j8/E79nvYz7f3Dv3/KQnzn1JyPPzfPznf849xzAHEAzDMAzDaE8sRUFra6vo7e2llRBdXV2is7OTVoNUVVVRJMQdd9xBEcMwNsPPo8uXL4uenh5aOaOoqEhMmTKFVkOUlJSISZMm0YphGFlQThR0d3eL9vZ2cf78edHU1GS99/3vf9/6Mwi+853vUCREbW2tKCsrE5MnTxZTp06ld/3z/vvvUzTEzZs3rYtw0GS6KIM4i6GOjg5x48YNWg2CYyZIcEyMHTuWVqmUl5eL4uJiWskHvp+rV69axxyOR/u7CvJcckpdXZ2YOHGiFeOcAzjvSktLWTwwTJ6RXhTggoUnlBMnTohDhw6Jt956i/5J/oFgwEULN4PbbrvN08UKP0+ySxElDz74oHjttdcCFTxRg+PjoYceolW07Ny5U6xcuZJW0ZEsACCko7jx+wXnHo7TiooK6/wZM2ZMrI7bTJjdHSLR9j+Di7/0CPPD5sFYMowJ1ULcUkSrkRi3fFYY4ybTKg2jxwz8HZW0kIvuj03RcZ0WSVxsTVDkjM9+xhCTyw1aCVEybkD8lg6tZUE6UYCbJm7+x48fF5s3b6Z35QQ31UWLFllP3LhQOXnylkkUAPwMe/bsoZXa/PjHPxbr16+nlRzA1YriSReOWnNzs9i2bZv055Ef4DLMmDHDOvcgEsaPHx8rsdB/dKtIfPAKreKNMW6xGPXFfxVGcf7PlwuXTHHzz6b4Y7spem4K0dxiij99LMS7bfm7Pa6oHhQIJQPaakalIT43ICBuKzPyLhykEQVHjx5V/gLm5KuUTRSAI0eOiPnz59NKTWT8XgFSFvlM08AN2Ldvn3TiKN9A7NbU1HyaAkQqQrV0mXmzW/TtXkwrPSiY9ZwYdfdXaRUObVdMcfW6KVqvmuK3Z8283vi9AsEwb5YhJpQMCIWJhqicHJ5QiFQUwNY8duyYNHavX1QVBXFwC+rr66W0xfMlCuIgqsNGkucfx/Sf2SsSp5+llR4YZQ+Lwi/X08o/nV0DAqDTFO9fMq2n/93Nah0DmZg1IA7W3DcgFP6mIHAnIRJRYKcI4vQ0g5xnQ0MDrTIj6xOtym6BrN8pCPt7RZrg9ddfF08//TS9w2RCNVHwyY65FOmDH1Fg5/6R63/vXHwEQC4em2eIRfMKxF1VBfSOP4L5WxyCi/fjjz9uXcDjZm+iSlplXnjhBYrUY/v27RTJx/CtsEGCVMGaNWtYEDggeVeRCiT+cIwixikQBF97pU88tzuhjSAAb7xnikc394u7n/pE7NrXb4kjP+RNFKAqHGIgrvamavnK4eD3AwtaNSA0Vaym98vBgwfFtGnTIt2NoxKqifbExV9RxDgF1fy688IvE+K++j5x+ry7nRHJhC4KYG8i3xuXuoFMoJhJdVR0C5CG0gmcTxs3bhRLly6ldxgnqCTazQ/bhNn5M1oxTpFxe19UwDmAa+CFUEUBnuKg0HV4kkN1s+qo5hagUFW3KvsXX3yR0wUeQBMvVUi0cOrAKyjAYwaBa/Ctf++zdlu4ITRRYKcLdEH1mgIbldyCN998kyI9QMpAx1RJEGTq6ikjiXPPU8S4pfo2ChgLbLdc/sM+0XTaeTohcFFg25txTxcMJy6tWFVxC3RzCfDzcsrAO6qIdu0LDP/cRYE3qqexU5COujf6HdcZBCoKdK2GRle1OKGCW6CbS/Dqq69SxHhBFdGue4Gh+dG7FHmjKP0oEmYA1Bmgb0MuAhMFcAieeOIJLauh7WEuTsDgI9mR3S3QzSXA74PTBt5RRbRjzgEXGPoDrYGZzPzb1tzFh4GIAgiCZ555RktBAOzJbk6Am6ICMrsFOrkEOF50S8UFjRvRHiWJ3x+miPHK2M+wKMgGagz2Hs4uDAIRBaiI5vaq8QIC79SpU7SSB91cAgx5Yvwxe/ZsiuTGvMQugV9m3s6iIBdo7oQBUJnwLQoaGxu1tzbjusti165dFMmDTi4BBBCnDfyjwnbERNtpYfbKORaZiR/f3Za586EvUYA+BKtWraKVvmC2exzBDQm/Y1nQzSXAsDDGPyqIdvMPejXhChN7BDGTmXM3TPHr36TfjeBZFKCOgJuoDBKn+e3DkWmugIouwbVr1yhyD6cO9AAjkhOXf0Irxi8l6vSpihQ0N0qHZ1Gwd+9ebQsLk8HY4Tgji1ugqkvgtbAU3zmfX8Ege4tjs+0kRUwQzKhkp8Ap6ZoaeRIFnDYYoqamhqL4IoNboFtfgvPnz1PExJ3E+z+niAmCCdzq2DGN+wMSBZw2GCLOqQObqN0CpKp0qiUATU1NFDF+kH1kstWbwGfDHiaV8eNYFDgFWxSHNzRyLQrQ1IZtzSEqKiooijdRugWHD+u3f5t3HQSD7KKdexMED49QdscfLvsUBSqO1w0THZwCEJVbAJdAt+Y9MtRwxAXZRTv3JggeHqHsjt/93ocoYJdgJGPH6tNsOwq3QEeXoKvL31AYZoiysjKK5IN7E6QHuzH8spiLDR3zo8OpdQWuRAG7BCORvbI5SPLtFujoEoDOzk6KGL+UlpZSJB/mH49TxKTQ3UGBd8bfSgHjiLYrQ26BY1GAmwG7BP7xs29dBvLpFujoEgDVjxGZKC8vp0gurN4EH7xCKyZo5s1ip8ANvX+hYADHouDQIe64NRwvlc2qDETKRL7cAl1dAqD6MSITxcXFFMmFee0cRQwTPRdbh1IIjkSBjlvCnFBSUkKRXuRDIOrqEgDeeRAMMm9HTFz8FUVMGMyY6iozrj09SRP9HX1zzc1cDJMOneoJkoFARIfBsNDZJVCFLVu2iD179lgvNFqyX/Z7eO3cudO6MdsvZhCrN0GnHLsOCqZ8naJ4MeYWChhHNLe4rCl4++23KWKSkbmyOWzC7DCos0sgMxACJ0+eFKZpinXr1lktvvGCOLZf9nt4rVy5UjQ0NHz6wn+HF8TDgQMHrL8vTLFQW1tLkVyY7XI8ZBljqoUonUGreFE5mWsK3NB8lYIBjIGTNHWT4jDwRChrsU7U4OLm1i2or6+PjT3c3t4uJk2aRKtggEsQp7RMXV2d2LRpE62cYRjyXdCOHDki5s+fT6vgQR3FlStXrJ0X6OYYxDkC4bFkyRJayUPfL56SoothwaznrD8T5563/pSFwiU/FUbFTFp5Z/X3+qxpgGFgT2LE8CV71sLnyg0x9jOGuPlnU/yxffD/C1v+t2dNq3Og7Bx/abT1Z05RgN4ECxYsoBWTjJebYpxEAZ708MQYJNjhErfUQY5TbASyiQIv4jcITp06ZaUud+zY4WnnU1SfOxvmh22ib58cx3fh/XtE//Gt0qQybIISBQ1b+sTuZv8348fmGeKuGQVi+sDN36sDgVbC6By472gikM8UBo5FAca3qlxk+OKLL4p77713xH5lXDBsenp6xNmzZ2k1eDFychFye7EHcRIFIGi3QManZL+oLgq8HOdBAyfh4sWL4p133nF8/rS0tEjXcbT/+C4pnsyROih8eIfo+3VDbEXB1t39IxrzOAGNj+6ZbYiaqgIx8/bgz8W9h/vFc7vdf66wcSwK8NSmYn8CFDohrxkEuCD19vbSalBQFBUVebIm4yYKgnQL4upKubmpypY+gajesGEDreQA39GlS5fE/v37sw5nk0HMDKfvZ6ul6GKI1MGou78aa1Hg5uY7q8QQ/7KiQFRNN0TxreGL8guXTPG1V/poJQeORAFuhtOmTaOVOsj4hGATN1EAgnILVBWguXB7c2KnwDmoeTpz5ox4+eWXU44dPBDgwUAmZEsdGBMqYy0KTp9PiEc399MqPU8uLBD3zg3HEcjFt/69T6paA1sUZN19ALtONfAUr8uQIlkIYicCz9WQF/xuZAViFI4dBADOfThXoKamxvpTJhIXDlAULUgdQBDEHRT9pQOuwEsrR4l3GgrFoytGRSIIwJfnySX+bbKKghMnTlCkBqg2lq2wSAeC6FvAczWGkG1PP1I6Kgg2nPtIZd24cSPwAtggkKWtsXH7wxTFm0kTKSAgBjY/Nkrs+G6hWPSFgrykCbLR/iEFkpFVFKjU2hhbv2TcfqQLftwCdgnkB6mdxx9/XIkWzGhtLJtbiImIslAw7QsUxRv7pp8sBmrvctSaJ3S6PzY9FUHmg6zfkEoX6rVr11LERIEft4BdglRk7dOwefNmq8YI4gBCDgV/jDNkmYg4PHUgWz1B0Lz57UKpxACAIPjPN7PXOuSb5FHTGb+pfI7I9QuKisJsrMI4w4tbwC7BSGRPgUEcIKUA8YIty9jCy2RGpomIuqQObGTrbAhB8L0t/eKN9+Qq3k0eNZ1RFFy+fJki+ZExf6gjcAvcPj2yS6A2+J3PmTPH2jFhCwR2EFKRaSKiLqkDGcFuiPvq5dpxkI6MoiDMgTdBI2Olsa64mVvALkF6VJ2pYQsEOAgbN27kFANhtjVRFC3GuMVa7DqQjbYrptVdMdf2yCiZN8tB+iC5w5/s8BZEeUBBmtMbAbsE6RnefVNF0FTITjHoLBCs1MHln9AqWgru+EeKmHyA1sboqrj8h8G0W84XGUWBKg120HGNkQsnbgG7BPqgs0CQKXVglFdTJDfmR1coUhNbDNzf0CftDoPhTChx4BSowty5cyliZMGJW8AuQWbi3GtDN4EgTeqg7GFhFAc70TQszL/8H0VqgdbFr/5Xn1JiwAbtnW3SigKV6gmmTJlCESMT2dwCpwOndMbuzBdn4i4QpEodzPh7ipiggRhAzQBmGci2q8AJGAOd3MgprShARzBV4A6GcpLNLdi1axdFTCZqa2sp0oNkgYBdDCo9mGRCqtRBxSyKmKCwCwghBlSqGRjOwppUGaB8+oCRl3RuAfpfxG0gVBjovKMGuxjKy8st90ClfinDkSV1UDDl68IYW0yrIcxu9YVXFKhaQJiJv56Z2sshrShQpUeBbD3imVTSuQXbt2+niMmFDimEbMA9qKqq+rSDokrIlDowKjO4TgOfkXGOigWEuUAL6LJSB6Kgp6eHIobxR7JbwC6BO3RLIWTC7qAIcaCKc2C2naQoego+zw2L/BBHMWDz8PxUQQCUFgV80ZSfZLeAXQJ3cFOuVCAO4Byg5kD2gsTE+z+nKFoKpn+TIsYtaEkcVzFgs+jvRkqAtKJApcZFjPzALWCXwBu6pxDSgZoDFCQePHiQ3pEL5OrNj96lVbQU3H4vRYwbDh0bbEkcVzEAnlxYMCJ1ANKKAlVQsR3s9evXKdIHuAXsEnhj0aJFFDHDWbp0qZQpBbO9maJosSYiVsykFeMEpAqwo+CpRnlbEgfFskXpb/9KiwIV28HCAtURdgm8gS233LUzM3ZKQaZCRFlSB8YdayhinAB3AKmCOOwoyMXzK9K7BEBpUcAwOvCNb3yDIiYTKESUoSGWVKmDyr+liMkGagd0cQds7rk7862fRQHDSE5xcbE4cOAArZhMIE2FIsQokSZ1oFBb4yhBAyLUDujgDtjAJUjuYDgcFgUMowBLlizhvhwOQBFifX19ZLsTpNl1wG2Nc3L6fMJqQKQTaGn8wMJRtEoPiwKGUYR169ZRxGQD9SvPPPNM3oWBTKkDbmucHdQPPLpZn3QBWFxpiCdXZxcEIK0o4KIwhpGPqVOnij179tCKyQYKECEM8oksqQP0JkjX1pgZBL0HdKofsHlq5aisaQMbdgoYRiEefPBB7l3gEAiDxsZGWoVP4sx/UBQt3JsgMxAEce49kImtdaNE5eTcggCwKGAYxUAagYWBM1atWpWXPgbmh23C7I3eKeDeBJnZe1hPQbD5sVHirirnt3oWBQyjICwMnIM+BmHXFyRajlEULdybID1NpxPiud16CoLau9zd5tP+21zlzDDyw8LAOWE3gDIv/YyiaOHeBCPBtsO6N/SrIfAiCAA7BQyjMCwMnIHi6bBmJciSOiiY8nXuTTAMCALdth2CN79d6EkQABYFTCzQuRUwhAHvSsgNZiV0dHTQKjikSR18nudkDOclzXYZzCoxLEHgtKgwHSwKmFiAqnydwc/f0tIi6urq6B0mHWfOnKEoOGRIHaDAsKDyLloxAIWF77bp06nwsXmG2LTB+S6DTLAoyDNcrxE8+E4xOEh30Mdg06ZN3BI5Cy+//DJFwSDNrgMuMEwB8wx0KixE/cA//1Ohoz4EuWBRwFjs3LmTIvVYs4YviMmgJXJ7ezsL0DRgaFKQWxRlSR1wgWEqu3+thyBA2+J99d7rB9KhtCg4f/48RYxfioqKlMxLs0uQnkmTJomGhgZx8uRJFgfDOHToEEX+Ma/8hqLosDoYcoHhp6C4MO79CFA7AHegfn1hxhHIXkkrCmBDMvqxcOFCitSBXYLs1NTUWOIAAprFwSAYmhRE3wJZZh1wB8NUtv4i3sWFmHKI2oEg3YFk0v6tFRUVFMlNPjqV6QRG9KrkFrBL4Bx8TxAHSCvwFkYhmpv91wHIMusAmNcuDL0GxIquYPJhXMcgP7mwwEoVYMphELUDmVA6fXDjxg2KmKBQyS1gl8A9SCtgCyPOnSNHjmi7lXPbtm0UeUeWMcl9Bx9Jfb19v/hkx9ycL/y7cWPr2/FLG6Bu4L+/WSgeXTEq8FRBOtKKgrKyMork5vr16xQxQQG3QIUnSXYJ/IHf8/z588WGDRssgYDaA50cBAxL8oNMY5KZQTq7zFhtQUTdAAYZoW5g5u3hiwGbtKKgtLSUIrnxe2Iz6Vm+fDlF8sIuQXBAIKD2AA6CaZraCAQ/6UeZUgfMIL+7EB9BgLqBHd8tdDXIKCiUTh+AMDqU6Q4sZplvCuwShEs6gaB7c6jhyJI6YIY4fVH91AHEwDsNg3UDUZFWFJSXl1MkP1evXqWICRKZ3QJ2CfKHLRBQgIqOifgzLl0TvW5p5tSBfKBZ0RvvqesUPPsPQ2IgzCJCJ6QVBbATVSGIKmJmJLK6BewSRAe2KsMxQNdEu1BRx22OnDqQj/MfqCkI7OZDX70/ejFgkzF9oIpdePbsWYqYoJHRLVi2bBlFI2GLO3/YhYr2Nkd0xFTt+/daU5BoOUwRIwvH/le91IFdRJiPHQVuyCgKYBuqAEaiMuEgm1uAmw5uRJlQ5ZiNGzhOVq5caaUWVOpz4WVLs3mzW5id0Q9AYlJRKXWAfgNIFURRROiEjJ+qtraWIvk5deoURUzQyOQWPPvssxQxsgLhhly9Cq6Bl+uGee0cRYwsXLikjiBAa2L0G5AlVZCOjKJAlV4FYP/+/RQxQSOLW5DLJWDkATUfr7/+Oq3kBcOR3GK2NVHEMO6AIAirNXGQZPyEqvQqAE8//TRvTQwRGdwCdgnUAmIyLrsUbJA6SFz+Ca0YWbjYKn89wUsr1RAEIOOnVK3C++DBgxQxXrh27RpFI4naLdDdJUBBnIopsq985SsUxQSNZwrkk4KJMyiKD3Nmy5suGE5W6aJCXtBm1apVorW1lVaMW3J9d1G6Bbq7BMjRz5kzRxiGITZu3GgJYBWcsWxCk2GCov1DCiTlsXmG1DUEw8kqChYtWkSRGjzxxBOBjERlRgK3IIrhOVxLIERT01AeG6mypUuXWg3GHnroIdHY2CjttFAvOft8otJDD5OZtk65Cw2/cKcaaQObrJ927ty5FKkBLkK4cbEwCIcoLqJcS5B58BeOdzhkVVVVKS6CDI4ZPpvsooC3sDL5YPw4dVwCkFUUzJihXm4HfQtKSkoCy8HiAosnMftlX+x0rGFAnUk+O9ixSzCI08Fftoswbdo0SyTU19dbx2q+nYSjR49aLkbcMD+6QhEjE7ub1W1vLCOGiaknWcDFRWVQIHfnnXem7KZI7nne09OT0hURYsLpE06Ory4tuFDL2HAJN3t0p8sFbjB4Ms0HaKPrRhTI+t16OU5s4HpB5AYBdgMgJVhdXS3Gjx9vtS0OEnzWw4cPKyMI0IURTZec0n9mr0icZucqbAqX/FQYFTNplZu7n/qEIjnBzoPJ5c7uoyXjROQdDnOKAliSeAJhRoL2rsi1u0F1UQDy8TPAJXDbHS+OoiBsEQahMHHiRKtZGXqTQDy73XmEosdjx44p5w7g+HKTEmNRkB/ciAIMQrqvvo9WTDoWVxpi/K20GMa8WYa45+6ClELInKIAVuCCBQtoxSQDx8HtBTQOoiAfboFblwDEURREef7hhpmcd4ezUFFR8am7hloHp6kNGWFRICdxcwpkB22X0WXRJmdZ5PTp0ylihtPV1UWRXuSjtgAWNyNEZ2cnRfkHaTSILPu1fv16yw1AcSPWKgsCkK80GMPIzI8OpzZ/yikK4tiZLCiivGBHzZo1aygKHjzBeRnfHXSOXAbyXSTIMIzeONpAuXbtWoqYZLw0Z5k9ezZFahOmW7Bw4UKK3AFrWzb8fkdeJvkxzhgzZgxFDMPYOBIFbOWmx8t+8KKiIorUJwy3wKtLEFdkrJGIA6gliKOzxDB+cSQKcJGOopud7Og+sjkMt8CrS8Awbli9ejVFDMMk40gUgC996UsUMTZO+xnEmSDdAnYJUuF6gvC45557KGIYJhnHogBbk9xs39EF3YcwBekWsEvA5AOvqQPjls9SxDDxxbEoANyHfiS9vb0U6cuyZcso8g67BCNJ7rzJBIfX1IExbjJFDBNfXIkCNJNhtyCVy5cvU6QvQRwX7BKMBE2CmODh1AHDZMaVKADsFqTCF+5B/BwX7BKkJ3kmBxMMvOuAYbLjWhSwW5AKF4MN4ue4YJcgPbrvbgkDX7sOit3NOWE8Mpr7R0SJa1EAeHviEHFpLhPEfngvbkGQLkHc2tby7pbg8ZM6MMYWi1Ff3CaMcYvpHSYMjAmVFDFRkHMgUiYaGxutHuiMu4E3uNDLOk3Oz+AeG/xsbm5mEFVBiYJ8DGpyC3ZmOB00lQwmD5aXl9OKCQK3o5KDxLzZLUR3B63kIHHpNyLxwSu0kofRq09Q5AweiOSf4y+NpsijUwAeeOABTiMwI3DjFmzZsoVrCTLA7Y2DBdcqXLOiAi4DJv/J9BK3suhkRuJZFOBi/tprr9FKb7iuYAg3tQXLly+nKL6UlJRQ5A7e1RIsSHmyAGWY3HgWBQBVvMgJM87RofLZiVsAlwATOOMOmjt5gXe1BAfSBl5/DwyjG75EAcBTIS7wOuOmyczYsWMpii9O3AIdXAI/sPsUDFGnDZjwWVFtUMQEgW9RANatW6e9MGBSyeYW6OIS+IFrCoKB0wYM445ARAGAMNB1q2JTUxNFjE02t4BdgtzwyGT/HDhwgNMGDOOSwEQB2LBhg5W/YxiQzi1glyA33d3dFDFeOXLkiFiyZAmtGIZxSqCiAGAfMHLsTivQ4wA/1aWnurraOhZQjIoX4kceeYT+afB4rfSXjfb2dooYL7S0tFhOFcMw7glcFABYdtu3b+fOh5qDXC6OBQhEvBCHmd+NiwNx8+ZNihg31NXVWYKAZxswjHdCEQUAF3+kE2Dj6QBXizNB0draShHjFAiCH/zgBywIGMYnoYkCG9h4sEPR7jXO8NMdExTXrl2jiHEC6pg2bdrEuwxigDGmmiLnzJvFWxKDJHRRAGDrov878spxrTXgpzsmKPhYcgaKVvHAEdU8A9Uxikopkoi/ci8KmGDJiyiwgSCAMDh58mTs+hpwBzomKLhwNTu2GMA2aN7J4h1jrISigImcvIoCm5qaGuuERoMW1BwgH6g6Z8+epYhhmDBgMcAw4ROJKLBBDhA1B8gHompY5R4H169fp4hhhigrK6PIGZw6SAU7mPDgwGKAYfJDpKIgGVQNIzeImf7Yz44LAVINMhcowuHARQufc+3atfRudnhGfrjIdryUlrqzaHt7eynSE5xTeDjANQDXAuxgwoMDiwGGyQ/GwIlnUiw16PKGpwWMlO3o6LDs+nzkXnGRmjhxoqitrRVFRUViypQpVpMcPxepo0ePigULFtBKHhQ5FLJy6tQpUV9fL9566y16J1pwc/PaaheugS0Surq6RGdnpxWjfiU5XaVqDULyuQVHZfr06XzzzyNmd4foe/t+WsmBUfawKPxyPa2cceGSKb72Sh+tGC8cf2k0RQqJAiek6xWQfDEdDtyJ4VML/d7wnWKLnGxAAIVRwJju5wZx6hOfq28EbtZBU1VVRdEQcIbyuVUuk5CwwfeSa9gShJUfUYWCYtQN2eCcwrEVlKhmgqP/+C6ROPc8raLHiyjo7DLF/Q0sCvwQW1HAMAzDuMO82T3wlNJBK+ckrl+kKDtG8eeEMdrhyPjRY4QxoZIWztm1r1+88MsErRi3sChgGIZhYkX3x6bocFDvfbHVuXi42GaKGy7N2j99LMS7A/9d2Kyo9te0Kbnp0wMLR1HEooBhGIZhGEKa3QcMwzAMw0QLiwKGYRiGYQYQ4v8B822MI9qMeOYAAAAASUVORK5CYII=" alt="CMS42"></div>
        <h3 class="h5">Tento CMS (Content Management System) vyvíjím jen tak pro zábavu a slouží jen jako případná ukázka mé tvorby.</h3>

        <p>Celá aplikace běží na PHP7.2 a frameworku <a href="https://nette.org/cs/" rel="nofollow" title="Nette Framework">Nette</a>.</p>

        <p>CMS není dokončen a pravděpodobně ani nikdy nebude, pracuji na něm pouze příležitostně. Přesto však poskytuje celou řadu funkcí.
            Na té veřejně přístupné části naleznete sice pouze <a href="/contact" title="Kontaktní formulář">kontaktní</a> a <a href="/sign/in"
                                                                                                                                title="Přihlásit se">
                přihlašovací</a> formulář, ale nejvíce vychytávek se skrývá právě v administraci. Ta obsahuje obecná nastavení, ale také pokročilou
            práci s oprávněním na základě rolí, zdrojů a práv. Také zde najdete datagridy pro práci s uživateli, URL adresami nebo třeba jazyky.
            Nachází se zde také pokročilý WYSIWYG editor stránek včetně vkládání dynamických prvků (komponent).</p>

        <p>Do CMS je možné stahovat pluginy a rozšiřovat tak funkce.
            Systém komunikuje s extérní aplikací, která přes REST API poskytuje procházení katalogu s pluginy a samozřejmě také jejich stahování a
            updatování. Těchto pluginů sice není mnoho (pouze 3.... a z toho jeden pouze testovací). Ale nikomu teoreticky nebrání nějaký plugin vytvořit
            :D.</p>

        <p>Pro Front-endovou část byl použit Bootstrap 4. Platí však, že se dá vzhled libovolně přizpůsobovat a to buď vlastním CSS nebo stáhnutím
            již hotové šablony přes systém pluginů. (Tedy kdyby nějaké šablony existovaly :D)</p>

        <p>Nepřehlédnutelnou součástí je také plně Ajaxové procházení stránek. K tomu jsem využil <a href="https://www.nittro.org" title="Nittro" rel="nofollow"
            >Nittro</a>. To lze zase v případě potřeby vypnout přímo v administraci.</p>

        <em>Pro vstup do administrace použijte přihlašovací údaje: jméno: <strong>user</strong> a heslo: <strong>user</strong>.</em>&nbsp;<span style="color: rgb(230, 0, 0);">Z&nbsp;pochopitelných důvodů však s těmito údaji nelze provádět všechny operace a vstupovat do některých sekcí administrace!</span><br>

        <h4 class="h--no-style mt-2"><strong>Další použité technologie:</strong></h4>
        <ul>
            <li>Doctrine 2 (Kdyby/Doctrine)</li>
            <li>Monolog</li>
            <li>Guzzle + OAuth2</li>
            <li>Doctrine migrations, fixtures</li>
            <li>PHPStan</li>
            <li>Mockery</li>
            <li>Nette Tester</li>
            <li>Codeception</li>
            <li>Slevomat/CodingStandards</li>
            <li>Gitlab CI</li>
            <li>Gulp</li>
            <li>PhpStorm</li>
            <li>Docker</li>
            <li>PostgreSQL</li>
            <li>Blackfire</li>
            <li>Nginx</li>
        </ul>
    </div>');
        $pageLocale->addContent($content);
        $manager->persist($pageLocale);

        //en
        $pageLocale = new PageLocale($page, $en);
        $pageLocale->setTitle('Home page');
        $pageLocale->setDescription('Content management system (CMS42) was developed just for fun.');
        $content = new PageContent();
        $content->setContent('<div class="homepage">
        <div class="pb-4"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgUAAACBCAYAAABQIsnqAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAB6lSURBVHhe7Z0NbBXXlcfvGJNCncXGYOxQDCmCGC+bGLKsC4kKBNrNpihhiaoUWCGSqiBHyqabkkrZpFZVuclWatgkTSRYGjWwaMFbodKQRpQWSEBqASvL10INgabYpoBt4uIoXtzGfrP+j8/E79nvYz7f3Dv3/KQnzn1JyPPzfPznf849xzAHEAzDMAzDaE8sRUFra6vo7e2llRBdXV2is7OTVoNUVVVRJMQdd9xBEcMwNsPPo8uXL4uenh5aOaOoqEhMmTKFVkOUlJSISZMm0YphGFlQThR0d3eL9vZ2cf78edHU1GS99/3vf9/6Mwi+853vUCREbW2tKCsrE5MnTxZTp06ld/3z/vvvUzTEzZs3rYtw0GS6KIM4i6GOjg5x48YNWg2CYyZIcEyMHTuWVqmUl5eL4uJiWskHvp+rV69axxyOR/u7CvJcckpdXZ2YOHGiFeOcAzjvSktLWTwwTJ6RXhTggoUnlBMnTohDhw6Jt956i/5J/oFgwEULN4PbbrvN08UKP0+ySxElDz74oHjttdcCFTxRg+PjoYceolW07Ny5U6xcuZJW0ZEsACCko7jx+wXnHo7TiooK6/wZM2ZMrI7bTJjdHSLR9j+Di7/0CPPD5sFYMowJ1ULcUkSrkRi3fFYY4ybTKg2jxwz8HZW0kIvuj03RcZ0WSVxsTVDkjM9+xhCTyw1aCVEybkD8lg6tZUE6UYCbJm7+x48fF5s3b6Z35QQ31UWLFllP3LhQOXnylkkUAPwMe/bsoZXa/PjHPxbr16+nlRzA1YriSReOWnNzs9i2bZv055Ef4DLMmDHDOvcgEsaPHx8rsdB/dKtIfPAKreKNMW6xGPXFfxVGcf7PlwuXTHHzz6b4Y7spem4K0dxiij99LMS7bfm7Pa6oHhQIJQPaakalIT43ICBuKzPyLhykEQVHjx5V/gLm5KuUTRSAI0eOiPnz59NKTWT8XgFSFvlM08AN2Ldvn3TiKN9A7NbU1HyaAkQqQrV0mXmzW/TtXkwrPSiY9ZwYdfdXaRUObVdMcfW6KVqvmuK3Z8283vi9AsEwb5YhJpQMCIWJhqicHJ5QiFQUwNY8duyYNHavX1QVBXFwC+rr66W0xfMlCuIgqsNGkucfx/Sf2SsSp5+llR4YZQ+Lwi/X08o/nV0DAqDTFO9fMq2n/93Nah0DmZg1IA7W3DcgFP6mIHAnIRJRYKcI4vQ0g5xnQ0MDrTIj6xOtym6BrN8pCPt7RZrg9ddfF08//TS9w2RCNVHwyY65FOmDH1Fg5/6R63/vXHwEQC4em2eIRfMKxF1VBfSOP4L5WxyCi/fjjz9uXcDjZm+iSlplXnjhBYrUY/v27RTJx/CtsEGCVMGaNWtYEDggeVeRCiT+cIwixikQBF97pU88tzuhjSAAb7xnikc394u7n/pE7NrXb4kjP+RNFKAqHGIgrvamavnK4eD3AwtaNSA0Vaym98vBgwfFtGnTIt2NoxKqifbExV9RxDgF1fy688IvE+K++j5x+ry7nRHJhC4KYG8i3xuXuoFMoJhJdVR0C5CG0gmcTxs3bhRLly6ldxgnqCTazQ/bhNn5M1oxTpFxe19UwDmAa+CFUEUBnuKg0HV4kkN1s+qo5hagUFW3KvsXX3yR0wUeQBMvVUi0cOrAKyjAYwaBa/Ctf++zdlu4ITRRYKcLdEH1mgIbldyCN998kyI9QMpAx1RJEGTq6ikjiXPPU8S4pfo2ChgLbLdc/sM+0XTaeTohcFFg25txTxcMJy6tWFVxC3RzCfDzcsrAO6qIdu0LDP/cRYE3qqexU5COujf6HdcZBCoKdK2GRle1OKGCW6CbS/Dqq69SxHhBFdGue4Gh+dG7FHmjKP0oEmYA1Bmgb0MuAhMFcAieeOIJLauh7WEuTsDgI9mR3S3QzSXA74PTBt5RRbRjzgEXGPoDrYGZzPzb1tzFh4GIAgiCZ555RktBAOzJbk6Am6ICMrsFOrkEOF50S8UFjRvRHiWJ3x+miPHK2M+wKMgGagz2Hs4uDAIRBaiI5vaq8QIC79SpU7SSB91cAgx5Yvwxe/ZsiuTGvMQugV9m3s6iIBdo7oQBUJnwLQoaGxu1tzbjusti165dFMmDTi4BBBCnDfyjwnbERNtpYfbKORaZiR/f3Za586EvUYA+BKtWraKVvmC2exzBDQm/Y1nQzSXAsDDGPyqIdvMPejXhChN7BDGTmXM3TPHr36TfjeBZFKCOgJuoDBKn+e3DkWmugIouwbVr1yhyD6cO9AAjkhOXf0Irxi8l6vSpihQ0N0qHZ1Gwd+9ebQsLk8HY4Tgji1ugqkvgtbAU3zmfX8Ege4tjs+0kRUwQzKhkp8Ap6ZoaeRIFnDYYoqamhqL4IoNboFtfgvPnz1PExJ3E+z+niAmCCdzq2DGN+wMSBZw2GCLOqQObqN0CpKp0qiUATU1NFDF+kH1kstWbwGfDHiaV8eNYFDgFWxSHNzRyLQrQ1IZtzSEqKiooijdRugWHD+u3f5t3HQSD7KKdexMED49QdscfLvsUBSqO1w0THZwCEJVbAJdAt+Y9MtRwxAXZRTv3JggeHqHsjt/93ocoYJdgJGPH6tNsOwq3QEeXoKvL31AYZoiysjKK5IN7E6QHuzH8spiLDR3zo8OpdQWuRAG7BCORvbI5SPLtFujoEoDOzk6KGL+UlpZSJB/mH49TxKTQ3UGBd8bfSgHjiLYrQ26BY1GAmwG7BP7xs29dBvLpFujoEgDVjxGZKC8vp0gurN4EH7xCKyZo5s1ip8ANvX+hYADHouDQIe64NRwvlc2qDETKRL7cAl1dAqD6MSITxcXFFMmFee0cRQwTPRdbh1IIjkSBjlvCnFBSUkKRXuRDIOrqEgDeeRAMMm9HTFz8FUVMGMyY6iozrj09SRP9HX1zzc1cDJMOneoJkoFARIfBsNDZJVCFLVu2iD179lgvNFqyX/Z7eO3cudO6MdsvZhCrN0GnHLsOCqZ8naJ4MeYWChhHNLe4rCl4++23KWKSkbmyOWzC7DCos0sgMxACJ0+eFKZpinXr1lktvvGCOLZf9nt4rVy5UjQ0NHz6wn+HF8TDgQMHrL8vTLFQW1tLkVyY7XI8ZBljqoUonUGreFE5mWsK3NB8lYIBjIGTNHWT4jDwRChrsU7U4OLm1i2or6+PjT3c3t4uJk2aRKtggEsQp7RMXV2d2LRpE62cYRjyXdCOHDki5s+fT6vgQR3FlStXrJ0X6OYYxDkC4bFkyRJayUPfL56SoothwaznrD8T5563/pSFwiU/FUbFTFp5Z/X3+qxpgGFgT2LE8CV71sLnyg0x9jOGuPlnU/yxffD/C1v+t2dNq3Og7Bx/abT1Z05RgN4ECxYsoBWTjJebYpxEAZ708MQYJNjhErfUQY5TbASyiQIv4jcITp06ZaUud+zY4WnnU1SfOxvmh22ib58cx3fh/XtE//Gt0qQybIISBQ1b+sTuZv8348fmGeKuGQVi+sDN36sDgVbC6By472gikM8UBo5FAca3qlxk+OKLL4p77713xH5lXDBsenp6xNmzZ2k1eDFychFye7EHcRIFIGi3QManZL+oLgq8HOdBAyfh4sWL4p133nF8/rS0tEjXcbT/+C4pnsyROih8eIfo+3VDbEXB1t39IxrzOAGNj+6ZbYiaqgIx8/bgz8W9h/vFc7vdf66wcSwK8NSmYn8CFDohrxkEuCD19vbSalBQFBUVebIm4yYKgnQL4upKubmpypY+gajesGEDreQA39GlS5fE/v37sw5nk0HMDKfvZ6ul6GKI1MGou78aa1Hg5uY7q8QQ/7KiQFRNN0TxreGL8guXTPG1V/poJQeORAFuhtOmTaOVOsj4hGATN1EAgnILVBWguXB7c2KnwDmoeTpz5ox4+eWXU44dPBDgwUAmZEsdGBMqYy0KTp9PiEc399MqPU8uLBD3zg3HEcjFt/69T6paA1sUZN19ALtONfAUr8uQIlkIYicCz9WQF/xuZAViFI4dBADOfThXoKamxvpTJhIXDlAULUgdQBDEHRT9pQOuwEsrR4l3GgrFoytGRSIIwJfnySX+bbKKghMnTlCkBqg2lq2wSAeC6FvAczWGkG1PP1I6Kgg2nPtIZd24cSPwAtggkKWtsXH7wxTFm0kTKSAgBjY/Nkrs+G6hWPSFgrykCbLR/iEFkpFVFKjU2hhbv2TcfqQLftwCdgnkB6mdxx9/XIkWzGhtLJtbiImIslAw7QsUxRv7pp8sBmrvctSaJ3S6PzY9FUHmg6zfkEoX6rVr11LERIEft4BdglRk7dOwefNmq8YI4gBCDgV/jDNkmYg4PHUgWz1B0Lz57UKpxACAIPjPN7PXOuSb5FHTGb+pfI7I9QuKisJsrMI4w4tbwC7BSGRPgUEcIKUA8YIty9jCy2RGpomIuqQObGTrbAhB8L0t/eKN9+Qq3k0eNZ1RFFy+fJki+ZExf6gjcAvcPj2yS6A2+J3PmTPH2jFhCwR2EFKRaSKiLqkDGcFuiPvq5dpxkI6MoiDMgTdBI2Olsa64mVvALkF6VJ2pYQsEOAgbN27kFANhtjVRFC3GuMVa7DqQjbYrptVdMdf2yCiZN8tB+iC5w5/s8BZEeUBBmtMbAbsE6RnefVNF0FTITjHoLBCs1MHln9AqWgru+EeKmHyA1sboqrj8h8G0W84XGUWBKg120HGNkQsnbgG7BPqgs0CQKXVglFdTJDfmR1coUhNbDNzf0CftDoPhTChx4BSowty5cyliZMGJW8AuQWbi3GtDN4EgTeqg7GFhFAc70TQszL/8H0VqgdbFr/5Xn1JiwAbtnW3SigKV6gmmTJlCESMT2dwCpwOndMbuzBdn4i4QpEodzPh7ipiggRhAzQBmGci2q8AJGAOd3MgprShARzBV4A6GcpLNLdi1axdFTCZqa2sp0oNkgYBdDCo9mGRCqtRBxSyKmKCwCwghBlSqGRjOwppUGaB8+oCRl3RuAfpfxG0gVBjovKMGuxjKy8st90ClfinDkSV1UDDl68IYW0yrIcxu9YVXFKhaQJiJv56Z2sshrShQpUeBbD3imVTSuQXbt2+niMmFDimEbMA9qKqq+rSDokrIlDowKjO4TgOfkXGOigWEuUAL6LJSB6Kgp6eHIobxR7JbwC6BO3RLIWTC7qAIcaCKc2C2naQoego+zw2L/BBHMWDz8PxUQQCUFgV80ZSfZLeAXQJ3cFOuVCAO4Byg5kD2gsTE+z+nKFoKpn+TIsYtaEkcVzFgs+jvRkqAtKJApcZFjPzALWCXwBu6pxDSgZoDFCQePHiQ3pEL5OrNj96lVbQU3H4vRYwbDh0bbEkcVzEAnlxYMCJ1ANKKAlVQsR3s9evXKdIHuAXsEnhj0aJFFDHDWbp0qZQpBbO9maJosSYiVsykFeMEpAqwo+CpRnlbEgfFskXpb/9KiwIV28HCAtURdgm8gS233LUzM3ZKQaZCRFlSB8YdayhinAB3AKmCOOwoyMXzK9K7BEBpUcAwOvCNb3yDIiYTKESUoSGWVKmDyr+liMkGagd0cQds7rk7862fRQHDSE5xcbE4cOAArZhMIE2FIsQokSZ1oFBb4yhBAyLUDujgDtjAJUjuYDgcFgUMowBLlizhvhwOQBFifX19ZLsTpNl1wG2Nc3L6fMJqQKQTaGn8wMJRtEoPiwKGUYR169ZRxGQD9SvPPPNM3oWBTKkDbmucHdQPPLpZn3QBWFxpiCdXZxcEIK0o4KIwhpGPqVOnij179tCKyQYKECEM8oksqQP0JkjX1pgZBL0HdKofsHlq5aisaQMbdgoYRiEefPBB7l3gEAiDxsZGWoVP4sx/UBQt3JsgMxAEce49kImtdaNE5eTcggCwKGAYxUAagYWBM1atWpWXPgbmh23C7I3eKeDeBJnZe1hPQbD5sVHirirnt3oWBQyjICwMnIM+BmHXFyRajlEULdybID1NpxPiud16CoLau9zd5tP+21zlzDDyw8LAOWE3gDIv/YyiaOHeBCPBtsO6N/SrIfAiCAA7BQyjMCwMnIHi6bBmJciSOiiY8nXuTTAMCALdth2CN79d6EkQABYFTCzQuRUwhAHvSsgNZiV0dHTQKjikSR18nudkDOclzXYZzCoxLEHgtKgwHSwKmFiAqnydwc/f0tIi6urq6B0mHWfOnKEoOGRIHaDAsKDyLloxAIWF77bp06nwsXmG2LTB+S6DTLAoyDNcrxE8+E4xOEh30Mdg06ZN3BI5Cy+//DJFwSDNrgMuMEwB8wx0KixE/cA//1Ohoz4EuWBRwFjs3LmTIvVYs4YviMmgJXJ7ezsL0DRgaFKQWxRlSR1wgWEqu3+thyBA2+J99d7rB9KhtCg4f/48RYxfioqKlMxLs0uQnkmTJomGhgZx8uRJFgfDOHToEEX+Ma/8hqLosDoYcoHhp6C4MO79CFA7AHegfn1hxhHIXkkrCmBDMvqxcOFCitSBXYLs1NTUWOIAAprFwSAYmhRE3wJZZh1wB8NUtv4i3sWFmHKI2oEg3YFk0v6tFRUVFMlNPjqV6QRG9KrkFrBL4Bx8TxAHSCvwFkYhmpv91wHIMusAmNcuDL0GxIquYPJhXMcgP7mwwEoVYMphELUDmVA6fXDjxg2KmKBQyS1gl8A9SCtgCyPOnSNHjmi7lXPbtm0UeUeWMcl9Bx9Jfb19v/hkx9ycL/y7cWPr2/FLG6Bu4L+/WSgeXTEq8FRBOtKKgrKyMork5vr16xQxQQG3QIUnSXYJ/IHf8/z588WGDRssgYDaA50cBAxL8oNMY5KZQTq7zFhtQUTdAAYZoW5g5u3hiwGbtKKgtLSUIrnxe2Iz6Vm+fDlF8sIuQXBAIKD2AA6CaZraCAQ/6UeZUgfMIL+7EB9BgLqBHd8tdDXIKCiUTh+AMDqU6Q4sZplvCuwShEs6gaB7c6jhyJI6YIY4fVH91AHEwDsNg3UDUZFWFJSXl1MkP1evXqWICRKZ3QJ2CfKHLRBQgIqOifgzLl0TvW5p5tSBfKBZ0RvvqesUPPsPQ2IgzCJCJ6QVBbATVSGIKmJmJLK6BewSRAe2KsMxQNdEu1BRx22OnDqQj/MfqCkI7OZDX70/ejFgkzF9oIpdePbsWYqYoJHRLVi2bBlFI2GLO3/YhYr2Nkd0xFTt+/daU5BoOUwRIwvH/le91IFdRJiPHQVuyCgKYBuqAEaiMuEgm1uAmw5uRJlQ5ZiNGzhOVq5caaUWVOpz4WVLs3mzW5id0Q9AYlJRKXWAfgNIFURRROiEjJ+qtraWIvk5deoURUzQyOQWPPvssxQxsgLhhly9Cq6Bl+uGee0cRYwsXLikjiBAa2L0G5AlVZCOjKJAlV4FYP/+/RQxQSOLW5DLJWDkATUfr7/+Oq3kBcOR3GK2NVHEMO6AIAirNXGQZPyEqvQqAE8//TRvTQwRGdwCdgnUAmIyLrsUbJA6SFz+Ca0YWbjYKn89wUsr1RAEIOOnVK3C++DBgxQxXrh27RpFI4naLdDdJUBBnIopsq985SsUxQSNZwrkk4KJMyiKD3Nmy5suGE5W6aJCXtBm1apVorW1lVaMW3J9d1G6Bbq7BMjRz5kzRxiGITZu3GgJYBWcsWxCk2GCov1DCiTlsXmG1DUEw8kqChYtWkSRGjzxxBOBjERlRgK3IIrhOVxLIERT01AeG6mypUuXWg3GHnroIdHY2CjttFAvOft8otJDD5OZtk65Cw2/cKcaaQObrJ927ty5FKkBLkK4cbEwCIcoLqJcS5B58BeOdzhkVVVVKS6CDI4ZPpvsooC3sDL5YPw4dVwCkFUUzJihXm4HfQtKSkoCy8HiAosnMftlX+x0rGFAnUk+O9ixSzCI08Fftoswbdo0SyTU19dbx2q+nYSjR49aLkbcMD+6QhEjE7ub1W1vLCOGiaknWcDFRWVQIHfnnXem7KZI7nne09OT0hURYsLpE06Ory4tuFDL2HAJN3t0p8sFbjB4Ms0HaKPrRhTI+t16OU5s4HpB5AYBdgMgJVhdXS3Gjx9vtS0OEnzWw4cPKyMI0IURTZec0n9mr0icZucqbAqX/FQYFTNplZu7n/qEIjnBzoPJ5c7uoyXjROQdDnOKAliSeAJhRoL2rsi1u0F1UQDy8TPAJXDbHS+OoiBsEQahMHHiRKtZGXqTQDy73XmEosdjx44p5w7g+HKTEmNRkB/ciAIMQrqvvo9WTDoWVxpi/K20GMa8WYa45+6ClELInKIAVuCCBQtoxSQDx8HtBTQOoiAfboFblwDEURREef7hhpmcd4ezUFFR8am7hloHp6kNGWFRICdxcwpkB22X0WXRJmdZ5PTp0ylihtPV1UWRXuSjtgAWNyNEZ2cnRfkHaTSILPu1fv16yw1AcSPWKgsCkK80GMPIzI8OpzZ/yikK4tiZLCiivGBHzZo1aygKHjzBeRnfHXSOXAbyXSTIMIzeONpAuXbtWoqYZLw0Z5k9ezZFahOmW7Bw4UKK3AFrWzb8fkdeJvkxzhgzZgxFDMPYOBIFbOWmx8t+8KKiIorUJwy3wKtLEFdkrJGIA6gliKOzxDB+cSQKcJGOopud7Og+sjkMt8CrS8Awbli9ejVFDMMk40gUgC996UsUMTZO+xnEmSDdAnYJUuF6gvC45557KGIYJhnHogBbk9xs39EF3YcwBekWsEvA5AOvqQPjls9SxDDxxbEoANyHfiS9vb0U6cuyZcso8g67BCNJ7rzJBIfX1IExbjJFDBNfXIkCNJNhtyCVy5cvU6QvQRwX7BKMBE2CmODh1AHDZMaVKADsFqTCF+5B/BwX7BKkJ3kmBxMMvOuAYbLjWhSwW5AKF4MN4ue4YJcgPbrvbgkDX7sOit3NOWE8Mpr7R0SJa1EAeHviEHFpLhPEfngvbkGQLkHc2tby7pbg8ZM6MMYWi1Ff3CaMcYvpHSYMjAmVFDFRkHMgUiYaGxutHuiMu4E3uNDLOk3Oz+AeG/xsbm5mEFVBiYJ8DGpyC3ZmOB00lQwmD5aXl9OKCQK3o5KDxLzZLUR3B63kIHHpNyLxwSu0kofRq09Q5AweiOSf4y+NpsijUwAeeOABTiMwI3DjFmzZsoVrCTLA7Y2DBdcqXLOiAi4DJv/J9BK3suhkRuJZFOBi/tprr9FKb7iuYAg3tQXLly+nKL6UlJRQ5A7e1RIsSHmyAGWY3HgWBQBVvMgJM87RofLZiVsAlwATOOMOmjt5gXe1BAfSBl5/DwyjG75EAcBTIS7wOuOmyczYsWMpii9O3AIdXAI/sPsUDFGnDZjwWVFtUMQEgW9RANatW6e9MGBSyeYW6OIS+IFrCoKB0wYM445ARAGAMNB1q2JTUxNFjE02t4BdgtzwyGT/HDhwgNMGDOOSwEQB2LBhg5W/YxiQzi1glyA33d3dFDFeOXLkiFiyZAmtGIZxSqCiAGAfMHLsTivQ4wA/1aWnurraOhZQjIoX4kceeYT+afB4rfSXjfb2dooYL7S0tFhOFcMw7glcFABYdtu3b+fOh5qDXC6OBQhEvBCHmd+NiwNx8+ZNihg31NXVWYKAZxswjHdCEQUAF3+kE2Dj6QBXizNB0draShHjFAiCH/zgBywIGMYnoYkCG9h4sEPR7jXO8NMdExTXrl2jiHEC6pg2bdrEuwxigDGmmiLnzJvFWxKDJHRRAGDrov878spxrTXgpzsmKPhYcgaKVvHAEdU8A9Uxikopkoi/ci8KmGDJiyiwgSCAMDh58mTs+hpwBzomKLhwNTu2GMA2aN7J4h1jrISigImcvIoCm5qaGuuERoMW1BwgH6g6Z8+epYhhmDBgMcAw4ROJKLBBDhA1B8gHompY5R4H169fp4hhhigrK6PIGZw6SAU7mPDgwGKAYfJDpKIgGVQNIzeImf7Yz44LAVINMhcowuHARQufc+3atfRudnhGfrjIdryUlrqzaHt7eynSE5xTeDjANQDXAuxgwoMDiwGGyQ/GwIlnUiw16PKGpwWMlO3o6LDs+nzkXnGRmjhxoqitrRVFRUViypQpVpMcPxepo0ePigULFtBKHhQ5FLJy6tQpUV9fL9566y16J1pwc/PaaheugS0Surq6RGdnpxWjfiU5XaVqDULyuQVHZfr06XzzzyNmd4foe/t+WsmBUfawKPxyPa2cceGSKb72Sh+tGC8cf2k0RQqJAiek6xWQfDEdDtyJ4VML/d7wnWKLnGxAAIVRwJju5wZx6hOfq28EbtZBU1VVRdEQcIbyuVUuk5CwwfeSa9gShJUfUYWCYtQN2eCcwrEVlKhmgqP/+C6ROPc8raLHiyjo7DLF/Q0sCvwQW1HAMAzDuMO82T3wlNJBK+ckrl+kKDtG8eeEMdrhyPjRY4QxoZIWztm1r1+88MsErRi3sChgGIZhYkX3x6bocFDvfbHVuXi42GaKGy7N2j99LMS7A/9d2Kyo9te0Kbnp0wMLR1HEooBhGIZhGEKa3QcMwzAMw0QLiwKGYRiGYQYQ4v8B822MI9qMeOYAAAAASUVORK5CYII=" alt="CMS42"></div>
        <h3 class="h5">This CMS (Content Management System) is developed just for fun and it is possible example of my work.</h3>

        <p>
Entire application runs on PHP 7.2 and <a href="https://nette.org/cs/" rel="nofollow" title="Nette Framework">Nette Framework</a>.</p>

        <p>CMS is not complete and probably will never be, I work on it only occasionally. 
It provides a variety of features. On the publicly accessible site you can find
only <a href="/contact" title="Contact form">contact</a> and <a href="/sign/in" title="Sign in">login</a> forms
but the most features are hidden in the administration. This includes general settings but also advanced work
with permissions.</p>

        <p>CMS is based on plugins and it is possible to extend system this way.
It ensures my external app (something like Plugins Store) that communicates with client side app via REST Api and
provides listing of plugins catalog and downloading them.</p>

        <p>For Front-end I used Bootstrap 4. Design can be edited by downloading templates or with custom CSS and JS.</p>

        <p>An unavoidable part is also fully Ajax browsing. For this purpose, I used
<a href="https://www.nittro.org" title="Nittro Framework" rel="nofollow"
            >Nittro Framework</a>. It can be of course disabled in the administration.</p>

        <em>To enter the administration use following login credentials: username: <strong>user</strong> and password: <strong>user</strong>.</em>&nbsp;<span style="color: rgb(230, 0, 0);">
For obvious reasons, you can not do all operations and enter some part of administration with this account.
If you want administration right, feel free to <a href="/contact" title="Contact form">contact me</a>.</span><br>

        <h4 class="h--no-style mt-2"><strong>Used technologies:</strong></h4>
        <ul>
            <li>Doctrine 2 (Kdyby/Doctrine)</li>
            <li>Monolog</li>
            <li>Guzzle + OAuth2</li>
            <li>Doctrine migrations, fixtures</li>
            <li>PHPStan</li>
            <li>Mockery</li>
            <li>Nette Tester</li>
            <li>Codeception</li>
            <li>Slevomat/CodingStandards</li>
            <li>Gitlab CI</li>
            <li>Gulp</li>
            <li>PhpStorm</li>
            <li>Docker</li>
            <li>PostgreSQL</li>
            <li>Blackfire</li>
            <li>Nginx</li>
        </ul>
    </div>');
        $pageLocale->addContent($content);
        $manager->persist($pageLocale);

        //save page
        $page->hideTitle();
        $page->publish();
        $manager->persist($page);
        $manager->flush();

        //url
        $url = new Url();
        $url->setInternalId($page->getId());
        $url->setDestination('Pages:Front:Pages');
        $url->setUrlPath('/'); //Slash will be removed

        //save url
        $manager->persist($url);
        $page->setUrl($url);
        $manager->persist($page);
        $this->addReference('page_home_url', $url);

        $manager->flush();
    }

    /** @return string[] */
    public function getDependencies(): array
    {
        return [LocaleFixture::class];
    }

}
