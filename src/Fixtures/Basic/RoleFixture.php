<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Security\Role;

class RoleFixture extends AbstractFixture
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $guest = new Role();
        $guest->setName(Role::GUEST);
        $manager->persist($guest);

        $user = new Role();
        $user->setName(Role::USER);
        $user->setParent($guest);
        $manager->persist($user);

        $admin = new Role();
        $admin->setName(Role::ADMIN);
        $admin->setParent($user);
        $manager->persist($admin);

        $root = new Role();
        $root->setName(Role::ROOT);
        $manager->persist($root);

        $manager->flush();

        $this->addReference('role_user', $user);
        $this->addReference('role_admin', $admin);
        $this->addReference('role_root', $root);
        $this->addReference('role_guest', $guest);
    }

}
