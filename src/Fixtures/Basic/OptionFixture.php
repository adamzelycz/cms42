<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Fixtures\Basic;

use DateTimeZone;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Settings\Option;
use Settings\Parameter;
use Settings\Types\Color;
use Settings\Types\Email;
use Settings\Types\Selectize;
use Settings\Types\Text;
use function array_map;

class OptionFixture extends AbstractFixture
{

    /** @var Text */
    private $text;

    /** @var Email */
    private $email;

    /** @var Color */
    private $color;

    public function __construct()
    {
        $this->text = new Text();
        $this->email = new Email();
        $this->color = new Color();
    }

    /**
     * @param ObjectManager $manager
     * @inheritdoc
     */
    public function load(ObjectManager $manager): void
    {
        $options = [];
        $option = new Option('site_name');
        $option->setValue('CMS42');
        $option->setName('Site Name');
        $option->setHint('Text that will be displayed in logo, in the browser tab and in search engines');
        $option->setType($this->text);
        $options[] = $option;

        $option = new Option('site_description');
        $option->setValue('Systém pro správu obsahu CMS42 vytvořený jen tak pro zábavu a procvičení programování v PHP.');
        $option->setName('Site description');
        $option->setHint('Text that will be displayed in search engines (very important for SEO)');
        $option->setType($this->text);
        $options[] = $option;

        $option = new Option('title_separator');
        $option->setValue('|');
        $option->setName('Title Separator');
        $option->setType($this->text);
        $options[] = $option;

        $option = new Option('color');
        $option->setValue('#2f333e');
        $option->setName('Browser color (mobile)');
        $option->setHint('Color of mobile browser tab');
        $option->setType($this->color);
        $options[] = $option;

        $option = new Option('email');
        $option->setName('E-mail for notifications');
        $option->setType($this->email);
        $options[] = $option;

        $option = new Option('google_verification');
        $option->setName('Html code for Google console');
        $option->setType($this->text);
        $options[] = $option;

//        $option = new Option('homepage_redirect');
//        $option->setName('Default path to homepage');
//        $option->setHint('You can edit default homepage in Pages section');
//        $option->setType($this->select);
//        $manager->persist($option);

        $option = new Option('datetime_format');
        $option->setName('Date-Time format');
        $option->setValue('j. n. Y G:i:s');
        $options[] = $option;

        $option = new Option('timezone');
        $option->setName('Timezone');
        $option->setType(new Selectize());
        $option->setRenderOptions([
            'selectize_mode' => 'select',
            'selectize_create' => false,
            'selectize_maxItems' => 1,
            'selectize_valueField' => 'value',
            'selectize_labelField' => 'title',
            'selectize_searchField' => 'title',
        ]);
        $option->setParameters(array_map(function (string $timezone): Parameter {
            return new Parameter($timezone, $timezone);
        }, DateTimeZone::listIdentifiers()));
        $option->setValue('Europe/Prague');
        $options[] = $option;

        foreach ($options as $option) {
            $manager->persist($option);
        }
        $manager->flush();
    }

}
