<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Contact\DI;

use App\Extensions\CompilerExtension;
use Contact\Components\ContactForm\IContactFormFactory;
use Pages\Helpers\RenderableControl;

class ContactExtension extends CompilerExtension
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function beforeCompile(): void
    {
        $contactForm = new RenderableControl(IContactFormFactory::class, 'Contact Form');
        $contactForm->setDescription('Simple contact form with adjustable email address');
        $this->setRenderableControls([$contactForm]);
    }

}
