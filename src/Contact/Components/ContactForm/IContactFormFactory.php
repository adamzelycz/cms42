<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Contact\Components\ContactForm;

interface IContactFormFactory
{

    public function create(): ContactForm;

}
