<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 * @ORM\Table(name="urls")
 * @method string getUrlPath()
 * @method string|null getPresenter()
 * @method string|null getAction()
 * @method int|null getInternalId()
 * @method Url|null getRedirect()
 * @method setRedirect(?Url $url)
 * @method setInternalId(int | null $id)
 */
class Url
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Destination Presenter (with modules)"})
     * @var string|null
     */
    protected $presenter;

    /**
     * @ORM\Column(type="string", name="url_action", nullable=true, options={"comment":"Destination action of Presenter"})
     * @var string|null
     */
    protected $action;

    /**
     * @ORM\Column(type="string", unique=true, options={"comment":"Fake path (URL)"})
     * @var string
     */
    protected $urlPath;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment":"Internal id passed to the action method (optional)"})
     * @var int|null
     */
    protected $internalId;

    /**
     * @ORM\ManyToOne(targetEntity="Url\Url", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     * @var Url
     */
    protected $redirect;

    public function getDestination(): string
    {
        return $this->presenter . ':' . $this->action;
    }

    public function getAbsoluteDestination(): string
    {
        return ':' . $this->getDestination();
    }

    public function setDestination(?string $presenter, ?string $action = 'default'): void
    {
        $this->presenter = $presenter;
        $this->action = $action;
    }

    public function setUrlPath(string $urlPath): void
    {
        $this->urlPath = Strings::webalize(trim($urlPath, " \t\n\r\0\x0B/"), '/_.~!%@#');
    }

}
