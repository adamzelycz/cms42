<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\FrontModule\Presenters;

use App\FrontModule\Presenters\BasePresenter;
use Url\Components\Sitemap\ISitemapFactory;
use Url\Components\Sitemap\Sitemap;

class SitemapPresenter extends BasePresenter
{

    protected function createComponentSitemap(ISitemapFactory $factory): Sitemap
    {
        return $factory->create();
    }

}
