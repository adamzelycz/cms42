<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\DI;

use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;

class UrlExtension extends CompilerExtension implements IEntityProvider
{

    public function loadConfiguration(): void
    {
        $builder = $this->getContainerBuilder();
        $this->parseConfig($builder, __DIR__ . '/services.neon');
        $builder->removeDefinition('routing.router');
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping(
            $this->getContainerBuilder(), ['Url' => 'Url\\*Module\\Presenters\\*Presenter']
        );
    }

    /**
     * Returns associative array of Namespace => mapping definition
     * @return array
     */
    public function getEntityMappings(): ?array
    {
        return ['Url' => __DIR__ . '/..'];
    }

}
