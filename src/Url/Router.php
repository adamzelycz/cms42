<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url;

use Doctrine\DBAL\Exception\TableNotFoundException;
use Kdyby\Doctrine\EntityManager;
use Locale\Locale;
use Monolog\Logger;
use Nette\Application\IRouter;
use Nette\Application\Request;
use Nette\Application\Request as AppRequest;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Http\IRequest as HttpRequest;
use Nette\Utils\Strings;
use const PREG_GREP_INVERT;
use function array_flip;

/**
 * Class Router
 * @package Url
 * TODO hodně souborů v cache (pomalý loading)
 */
class Router implements IRouter
{

    public const CACHE_NAMESPACE = 'CMS.Router';

    /** @var EntityManager */
    private $em;

    /** @var Logger */
    private $monolog;

    /** @var Cache */
    private $cache;

    /** @var array|mixed[] (string code => bool default) */
    private $locales;

    public function __construct(EntityManager $em, Logger $monolog, IStorage $storage)
    {
        $this->em = $em;
        $this->monolog = $monolog;

        if (\PHP_SAPI === 'cli') {
            $storage = new DevNullStorage();
            $this->cache = new Cache($storage, self::CACHE_NAMESPACE);

            try { // Try to load database tables
                $this->locales = $this->em->getRepository(Locale::class)->findPairs([], 'default', [], 'code');
            } catch (TableNotFoundException $exception) { //Probably first run without migrations
                $this->monolog->addError('Database table not found. You should run migrations');
                $this->locales = ['cs' => true, 'en' => false]; //some random values in case of first run
            }
        } else {
            $this->cache = new Cache($storage, self::CACHE_NAMESPACE);
            $this->locales = $this->cache->load('locales', function (& $dependencies) {
                $dependencies = [Cache::TAGS => [self::CACHE_NAMESPACE . '/locales']];
                return $this->em->getRepository(Locale::class)->findPairs([], 'default', [], 'code');
            });
        }

    }

    /**
     * HTTP URL => Intérní Request
     * @inherit
     * @param HttpRequest $httpRequest
     * @return AppRequest|null
     */
    public function match(HttpRequest $httpRequest): ?AppRequest
    {
        /*
         * 1) URL editing
         */
        $url = $httpRequest->getUrl();
        $urlPath = $url->getPath();
        $urlPath = substr($urlPath, \strlen($url->getBasePath()));
        $urlPath = trim(rawurldecode($urlPath), '/');

        /*
         * 2) LOCALE
         */
        $locale = null;
        $firstOfPath = explode('/', $urlPath)[0];
        if (isset($this->locales[$firstOfPath])) {
            $locale = $firstOfPath;
//            if ($this->locales[$firstOfPath]) {
            $urlPath = substr($urlPath, \strlen($firstOfPath) + 1) ?: '';
//            }
        } else {
            $locale = array_search(true, $this->locales, true);
        }

        /*
         * 3) Loading destination
         */
        $destination = $this->cache->load('route/' . $urlPath, function (& $dependencies) use ($urlPath) {
            $destination = $this->em->getRepository(Url::class)->findOneBy(['urlPath eq' => $urlPath]);
            if ($destination === null) {
                return null;
            }
            $dependencies = [Cache::TAGS => ['route/' . $destination->getId()]];
            return $destination;
        });

        /*
         * 4) handling errors
         */
        if ($destination === null) {
            $this->monolog->addError(sprintf('Router: %s does not match any destination. IP: %s',
                $url->getPath(), $httpRequest->getRemoteAddress()));
            return null;
        }

        /*
         * 5) Redirects and Internal IDs
         */
        $redirect = $destination->getRedirect();
        if ($redirect !== null) {
            $destination = $redirect;
        }
        $internalId = $destination->getInternalId();

        /*
         * 6)) Creating Request
         */
        $parameters = $httpRequest->getQuery();
        $destination = $destination->getDestination();
        $pos = (int)strrpos($destination, ':'); //could be false (?)
        $presenter = substr($destination, 0, $pos);
        $parameters['action'] = substr($destination, $pos + 1);
        $parameters['locale'] = $locale;
        if ($internalId) {
            $parameters['id'] = $internalId;
        }
        return new AppRequest(
            $presenter,
            $httpRequest->getMethod(),
            $parameters,
            $httpRequest->getPost(),
            $httpRequest->getFiles(),
            [Request::SECURED => $httpRequest->isSecured()]
        );
    }

    /**
     * Internal Request => HTTP URL
     * @inheritdoc
     * @param AppRequest $appRequest
     * @param \Nette\Http\Url $refUrl
     * @return null|string
     */
    public function constructUrl(AppRequest $appRequest, \Nette\Http\Url $refUrl): ?string
    {

        /*
         * 1) Getting things :)
         */
        $presenter = $appRequest->getPresenterName();
        $parameters = $appRequest->getParameters();
        $action = $parameters['action'];
        $destination = $presenter . ':' . $action;

        /*
         * 2) Loading things :) NAME, PARAM['action!], PARAM['do']
         */
        //Filter grid urls
        $newParams = array_intersect_key($appRequest->getParameters(),
            array_flip(preg_grep('/-grid-/', array_keys($appRequest->getParameters()), PREG_GREP_INVERT)));
        $shortRequest = $appRequest;
        $shortRequest->setParameters($newParams);
        $result = $this->cache->load($shortRequest, function (& $dependencies) use ($presenter, $action, $parameters) {
            $fallback = false; //true if ID was not found but destination yes
            if (isset($parameters['id'])) {
                $url = $this->em->getRepository(Url::class)->findOneBy([
                    'presenter eq' => $presenter,
                    'action eq' => $action,
                    'internalId eq' => $parameters['id'],
                ]);
                if ($url === null) {
//                    $this->monolog->addDebug(sprintf('Cannot find cool route for destination %s. Fallback will be used.', $presenter . ':' . $action), [
//                        'internalId' => $parameters['id'],
//                    ]);
                    $url = $this->em->getRepository(Url::class)->findOneBy([
                        'presenter eq' => $presenter,
                        'action eq' => $action,
                    ]);
                    $fallback = true;
                }
            } else {
                $url = $this->em->getRepository(Url::class)->findOneBy([
                    'presenter eq' => $presenter,
                    'action eq' => $action,
                ]);
            }

            if ($url === null) {
                return null;
            }

            $dependencies = [Cache::TAGS => ['route/' . $url->getId()]];
            return [$url, $fallback];
        });
        /** @var null|Url $url */
        [$url, $fallback] = $result;

        /*
         * 3) Error handling
         */
        if (!$url) {
            $this->monolog->addError(sprintf('Router: Url for this destination does not exists. %s called at %s',
                $destination, $refUrl->getAbsoluteUrl()));
            return null;
        }

        /*
         * 4) Redirects
         */
        if ($url->getRedirect()) {
            $url = $url->getRedirect();
        }

        /*
         * 5) Constructing Url
         */
        $scheme = $refUrl->getScheme() . '://';
        $authority = $refUrl->getAuthority();
        $basePath = $refUrl->getBasePath();
        $locale = isset($parameters['locale']) && isset($this->locales[$parameters['locale']]) &&
        !$this->locales[$parameters['locale']] ? $parameters['locale'] : '';
        $urlPath = Strings::webalize($url->getUrlPath(), '/_.~!%@#');

        unset($parameters['action'], $parameters['locale']);
        if (!$fallback && isset($parameters['id'])) {
            unset($parameters['id']);
        }
        $paramsQuery = http_build_query($parameters);
        if ($paramsQuery) {
            $paramsQuery = '?' . $paramsQuery;
        }

        if ($urlPath && $locale) {
            $locale .= '/';
        }

        $finalUrl = rtrim($scheme . $authority . $basePath . $locale . $urlPath, '/');

        return $finalUrl . $paramsQuery;
    }

}
