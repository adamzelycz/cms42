<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Url\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Url\Components\UrlGrid\IUrlGridFactory;
use Url\Components\UrlGrid\UrlGrid;
use Url\Url;

class UrlPresenter extends BasePresenter
{

    public function actionDefault(): void
    {
        $this->setTitle('Urls');
    }

    protected function createComponentUrlGrid(IUrlGridFactory $factory): UrlGrid
    {
        $control = $factory->create();
        $control->onExistingUrl[] = function (Url $url): void {
            $message = sprintf('Url with path "%s" already exists', $url->getUrlPath());
            $this->monolog->addError($message);
            $this->flashMessage($message, Flashes::ERROR);
            $this->sendPayload();
        };
        return $control;
    }

}
