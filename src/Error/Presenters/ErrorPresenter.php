<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Error\Presenters;

use App\FrontModule\Presenters\BasePresenter;
use Nette\Application\BadRequestException;
use Tracy\ILogger;

//TODO AJAX šipka zpátky, Bez layoutu
class ErrorPresenter extends BasePresenter
{

    /** @var ILogger */
    private $logger;

    public function __construct(ILogger $logger)
    {
        parent::__construct();
        $this->logger = $logger;
    }

    public function renderDefault(\Throwable $exception): void
    {
        $errors = [400, 403, 404, 405, 410, 500];
        if ($exception instanceof BadRequestException) {
            $code = $exception->getCode();
            $this->setView(\in_array($code, $errors, true) ? $code : '4xx');
            $this->logger->log(sprintf('HTTP ERROR %s: %s. in %s:%d',
                $code, $exception->getMessage(), $exception->getFile(), $exception->getLine()));
            $this->setTitle('Error ' . $code);
        } else {
            $this->setView('500');
            $this->logger->log($exception, ILogger::EXCEPTION);
        }
    }

}
