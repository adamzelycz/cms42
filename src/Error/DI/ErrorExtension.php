<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Error\DI;

use App\Extensions\CompilerExtension;

class ErrorExtension extends CompilerExtension
{

    public function beforeCompile(): void
    {
        $this->setPresenterMapping($this->getContainerBuilder(), ['Error' => 'Error\\*Module\\Presenters\\*Presenter']);
    }

}
