<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations\DI;

use Doctrine\DBAL\Migrations\Configuration\Configuration;
use Kdyby;
use Migrations\Command\ResetCommand;
use Nette;

class MigrationsExtension extends Nette\DI\CompilerExtension
{

    /** @var array|string[] */
    private $commands = [
        ResetCommand::class,
    ];

    /** @var array|mixed[] */
	private $defaults = [
		'table' => 'doctrine_migrations',
		'directory' => '%appDir%/../migrations',
		'namespace' => 'Migrations',
	];

	public function loadConfiguration(): void
    {
		$containerBuilder = $this->getContainerBuilder();
		$config = $this->getConfig($this->defaults);

		$configuration = $containerBuilder
			->addDefinition($this->prefix('configuration'))
			->setClass(Configuration::class)
			->addSetup('setMigrationsTableName', [$config['table']])
			->addSetup('setMigrationsDirectory', [$config['directory']])
			->addSetup('setMigrationsNamespace', [$config['namespace']]);
//			->addSetup('registerMigrationsFromDirectory', [$config['directory']]);

		foreach ($this->loadFromFile(__DIR__ . '/commands.neon') as $i => $command) {
			$containerBuilder->addDefinition($this->prefix('cli.migrations.' . $i))
				->addTag(Kdyby\Console\DI\ConsoleExtension::COMMAND_TAG)
				->setInject(false)// lazy injects
				->setClass($command)
				->addSetup('setMigrationConfiguration', [$configuration]);
		}

        foreach ($this->commands as $i => $command) {
            $containerBuilder->addDefinition($this->prefix('cli.' . $i))
                ->addTag(Kdyby\Console\DI\ConsoleExtension::TAG_COMMAND)
                ->setInject(false)// lazy injects
                ->setClass($command);
        }
	}

}
