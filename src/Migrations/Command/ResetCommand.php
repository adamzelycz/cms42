<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Migrations\Command;

use Kdyby\Doctrine\Connection;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetCommand extends Command
{

    /** @var Connection @inject */
    public $connection;

    /** @var Logger @inject */
    public $monolog;

    protected function configure(): void
    {
        $this
            ->setName('migrations:reset')
            ->setDescription('Reset migrations in your database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        try {
            $this->connection->query('DELETE FROM doctrine_migrations');
            return 0;//no error
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
            $this->monolog->addError($exception->getMessage());
            return 1;//error(s)
        }
    }

}
