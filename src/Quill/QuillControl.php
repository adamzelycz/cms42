<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Quill;

use Nette\Forms\Container;
use Nette\Forms\Controls\HiddenField;
use Nette\Utils\Html;

class QuillControl extends HiddenField
{

    public const
        THEME_SNOW = 'snow',
        THEME_BUBBLE = 'bubble';

    /** @var array|mixed[] */
    private $config;

    /** @param array|mixed[] $config */
    public function __construct(array $config)
    {
        parent::__construct();
        $this->config = $config;
    }

    public function getControl(): Html
    {
        /** @var \Nette\Forms\Controls\HiddenField $control */
        $hidden = parent::getControl();
        $editor = Html::el('div', ['class' => 'quill-editor']);
        $control = Html::el('div', [
            'data-config' => $this->config,
        ]);
        $control->addHtml($hidden);
        $control->addHtml($editor);
        return $control;
    }

    /**
     * @param array|mixed[] $config
     * @param string $method
     */
    public static function register(array $config, string $method = 'addQuill'): void
    {
        Container::extensionMethod($method, function (Container $container, $name) use ($config) {
            $control = new QuillControl($config);
            $container[$name] = $control;
            return $container[$name];
        });
    }

    public function setPlaceholder(string $placeholder): QuillControl
    {
        $this->config['placeholder'] = $placeholder;
        return $this;
    }

    public function setTheme(string $theme): QuillControl
    {
        $this->config['theme'] = $theme;
        return $this;
    }

    public function setReadOnly(bool $readOnly = true): QuillControl
    {
        $this->config['readOnly'] = $readOnly;
        return $this;
    }

    /** @param string[] $tools */
    public function addTools(array $tools): QuillControl
    {
        $this->config['modules']['toolbar'][] = $tools;
        return $this;
    }

}
