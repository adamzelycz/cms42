<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security\Components\Resources;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Czubehead\BootstrapForms\Enums\RenderMode;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Security\Authorizator;
use Security\Resource;

/**
 * Class Resources
 * @package Security\Components\Resources
 * @method onEdit(string $name)
 * @method onExistingName(string $name)
 * @method onCreate(string $name)
 * @method onDelete(string $name)
 */
class Resources extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/Resources.latte';

    /** @var callable[] */
    public $onEdit;

    /** @var callable[] */
    public $onExistingName;

    /** @var callable[] */
    public $onCreate;

    /** @var callable[] */
    public $onDelete;

    /** @var EntityManager */
    private $em;

    /** @var \Security\Resource[] */
    private $resources;

    /** @var Cache */
    private $cache;

    public function __construct(EntityManager $em, IStorage $storage)
    {
        parent::__construct();
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
        $this->em = $em;
        $this->resources = $em->getRepository(Resource::class)->findAll();
    }

    public function render(): void
    {
        $this->template->resources = $this->resources;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleDelete(int $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        /** @var \Security\Resource $resource */
        $resource = $this->em->getRepository(Resource::class)->find($id);
        if ($resource !== null) {
            $this->em->remove($resource);
            $this->em->flush();
            $this->cleanCache();
            $this->onDelete($resource->getName());
        }
        $this->redirect('this');
    }

    protected function createComponentEditForm(): Multiplier
    {
        return new Multiplier(function ($index) {
            /** @var \Security\Resource $resource */
            $resource = $this->resources[$index];
            $form = $this->getForm(RenderMode::Inline);
            $form->addProtection();
            $form->addHidden('resource_id')
                ->setDefaultValue($resource->getId());
            $form->addText('name')->setRequired()->setDefaultValue($resource->getName());
            $form->addSubmit('edit', 'Edit');
            $form->onSuccess[] = [$this, 'editFormSucceeded'];
            return $form;
        });
    }

    /** @param array|mixed[] $values */
    public function editFormSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        /** @var \Security\Resource $resource */
        $resource = $this->em->getRepository(Resource::class)->find($values['resource_id']);
        $resource->setName($values['name']);
        try {
            $this->em->persist($resource);
            $this->em->flush($resource);
            $this->cleanCache();
            $this->onEdit($resource->getName());
        } catch (UniqueConstraintViolationException $exception) {
            $this->onExistingName($resource->getName());
        }
        $this->redirect('this');
    }

    protected function createComponentNewForm(): Form
    {
        $form = $this->getForm(RenderMode::Inline);
        $form->addProtection();
        $form->addText('name')
            ->setRequired();
        $form->addSubmit('new', 'New');
        $form->onSuccess[] = [$this, 'newFormSucceeded'];
        return $form;
    }

    /** @param array|mixed[] $values */
    public function newFormSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::CREATE);
        $resource = new Resource($values['name']);
        try {
            $this->em->persist($resource);
            $this->em->flush($resource);
            $this->cleanCache();
            $this->onCreate($resource->getName());
        } catch (UniqueConstraintViolationException $exception) {
            $this->onExistingName($resource->getName());
        }
        $this->redirect('this');
    }

    private function cleanCache(): void
    {
        $this->cache->clean([Cache::TAGS => [
            Authorizator::CACHE_NAMESPACE . '/permissions',
            Authorizator::CACHE_NAMESPACE . '/roles',
            Authorizator::CACHE_NAMESPACE . '/resources',
        ]]);
    }

}
