<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security\Components\Permissions;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Security\Authorizator;
use Security\Permission;
use Security\Resource;
use Security\Role;

class Permissions extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/Permissions.latte';

    /** @var EntityManager */
    private $em;

    /** @var Permission[] */
    private $permissions;

    /** @var Cache */
    private $cache;

    public function __construct(EntityManager $em, IStorage $storage)
    {
        parent::__construct();
        $this->cache = new Cache($storage, Authorizator::CACHE_NAMESPACE);
        $this->em = $em;
        //TODO PermissionsQuery
        $qb = $em->getRepository(Permission::class)->createQueryBuilder('p', 'p.id')
            ->leftJoin('p.role', 'ro')->addSelect('ro')
            ->leftJoin('p.resource', 're')->addSelect('re');
        $this->permissions = $qb->getQuery()->getResult();
    }

    public function render(): void
    {
        $this->template->permissions = $this->permissions;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleDelete(int $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        $permission = $this->em->getRepository(Permission::class)->find($id);
        if ($permission) {
            $this->em->remove($permission);
            $this->em->flush();
            $this->cleanCache();
        }
        $this->flashMessage('Permission was deleted');
        $this->redirect('this');
    }

    protected function createComponentUpdateForm(): Form
    {
        $form = $this->getForm();
        $form->addProtection();
        /** @var Permission $permission */
        foreach ($this->permissions as $permission) {
            $form->addSelect($permission->getId() . '_method', null, [1 => 'allow', 0 => 'deny'])->setDefaultValue((int)$permission->getAllow());
            $form->addCheckbox($permission->getId() . '_' . Authorizator::READ)->setDefaultValue($permission->getRead());
            $form->addCheckbox($permission->getId() . '_' . Authorizator::CREATE)->setDefaultValue($permission->getCreate());
            $form->addCheckbox($permission->getId() . '_' . Authorizator::UPDATE)->setDefaultValue($permission->getUpdate());
            $form->addCheckbox($permission->getId() . '_' . Authorizator::DELETE)->setDefaultValue($permission->getDelete());
        }
        $form->addSubmit('save', 'Save');
        $form->onSuccess[] = [$this, 'formUpdateSucceeded'];

        return $form;
    }

    /** @param array|mixed[] $values */
    public function formUpdateSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::UPDATE);

        if ($this->permissions) {
            /** @var Permission $permission */
            foreach ($this->permissions as $permission) {
                $permission->setAllow((bool)$values[$permission->getId() . '_method'])
                    ->setRead($values[$permission->getId() . '_' . Authorizator::READ])
                    ->setCreate($values[$permission->getId() . '_' . Authorizator::CREATE])
                    ->setUpdate($values[$permission->getId() . '_' . Authorizator::UPDATE])
                    ->setDelete($values[$permission->getId() . '_' . Authorizator::DELETE]);
                $this->em->persist($permission);
            }
            $this->em->flush();
            $this->cleanCache();
            $this->onSuccess($this);
        }
        $this->presenter->redirect('this');
    }

    protected function createComponentCreateForm(): Form
    {
        $form = $this->getForm();
        $form->addProtection();
        $resources = $this->em->getRepository(Resource::class)->findPairs([], 'name', [], 'id');
        $roles = $this->em->getRepository(Role::class)->findPairs([], 'name', [], 'id');

        $form->addSelect('resource_id', 'Resource', $resources)->setRequired();
        $form->addSelect('role_id', 'Role', $roles)->setRequired();
        $form->addSelect('allow', 'Method', [1 => 'allow', 0 => 'deny'])->setRequired();
        $form->addCheckbox(Authorizator::READ, 'Read');
        $form->addCheckbox(Authorizator::CREATE, 'Create');
        $form->addCheckbox(Authorizator::UPDATE, 'Update');
        $form->addCheckbox(Authorizator::DELETE, 'Delete');

        $form->addSubmit('create_new', 'Create');
        $form->onSuccess[] = [$this, 'formCreateSucceeded'];
        return $form;
    }

    /** @param array|mixed[] $values */
    public function formCreateSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::CREATE);

        /** @var \Security\Resource|null $resource */
        $resource = $this->em->getRepository(Resource::class)->find($values['resource_id']);
        /** @var Role|null $role */
        $role = $this->em->getRepository(Role::class)->find($values['role_id']);
        if ($resource !== null && $role !== null) {
            $permission = new Permission();
            $permission->setAllow($values['allow'])
                ->setResource($resource)
                ->setRole($role)
                ->setRead($values[Authorizator::READ])
                ->setCreate($values[Authorizator::CREATE])
                ->setUpdate($values[Authorizator::UPDATE])
                ->setDelete($values[Authorizator::DELETE]);

            $this->em->persist($permission);
            $this->em->flush();
            $this->cleanCache();
        }
        $this->redirect('this');
    }

    private function cleanCache(): void
    {
        $this->cache->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/permissions']]);
        $this->cache->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/roles']]);
        $this->cache->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/resources']]);
    }

}
