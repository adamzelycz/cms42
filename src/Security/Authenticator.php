<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security;

use Kdyby\Doctrine\EntityManager;
use Nette\Http\Request;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Users\User as UserEntity;

class Authenticator implements IAuthenticator
{

    /** @var EntityManager */
    private $em;

    /** @var Request */
    private $request;

    public function __construct(EntityManager $em, Request $request)
    {
        $this->em = $em;
        $this->request = $request;
    }

    /** @param array|string[] $credentials */
    public function authenticate(array $credentials): IIdentity
    {
        [$username, $password] = $credentials;

        /** @var UserEntity|null $user */
        $user = $this->em->getRepository(UserEntity::class)->findOneBy(['username eq' => $username]);
        if (!$user) {
            throw new AuthenticationException('exceptions.authentication.badLogin', self::IDENTITY_NOT_FOUND);
        }
        if (!Passwords::verify($password, $user->getPassword())) {
            throw new AuthenticationException('exceptions.authentication.badPassword', self::INVALID_CREDENTIAL);
        }

        if (Passwords::needsRehash($user->getPassword())) {
            $user->setPassword(Passwords::hash($password));
        }

        $ip = $this->request->getRemoteAddress();
        if ($ip) {
            $user->setIp($ip);
            $this->em->persist($user);
            $this->em->flush();
        }

        $roles = [];
        /** @var Role $role */
        foreach ($user->getRoles() as $role) {
            $roles[] = $role->getName();
        }

        $data = [
            'username' => $username,
            'ip' => $ip,
        ];
        return new Identity($user->getId(), $roles, $data);
    }

}
