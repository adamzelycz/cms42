<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security;

use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\InvalidStateException;
use Nette\Security\IAuthorizator;

class Authorizator implements IAuthorizator
{

    public const
        CREATE = 'create',
        READ = 'read',
        UPDATE = 'update',
        DELETE = 'delete';

    public const CACHE_NAMESPACE = 'CMS.Authorizator';

    /** @var \Nette\Security\Permission */
    private $acl;

    public function __construct(EntityManager $em, IStorage $storage)
    {
        $this->acl = new \Nette\Security\Permission();
        $cache = new Cache($storage, self::CACHE_NAMESPACE);

        /** @var array $roles */
        $roles = $cache->load('roles', function (& $dependencies) use ($em) {
            $dependencies = [Cache::TAGS => ['roles' => self::CACHE_NAMESPACE . '/roles']];
            return $em->getRepository(Role::class)->findAll();
        });
        /** @var Role $role */
        foreach ($roles as $role) {
            $parent = $role->getParent();
            $this->acl->addRole($role->getName(), $parent ? $parent->getName() : null);
        }

        /** @var array $resources */
        $resources = $cache->load('resources', function (& $dependencies) use ($em) {
            $dependencies = [Cache::TAGS => [self::CACHE_NAMESPACE . '/resources']];
            return $em->getRepository(Resource::class)->findAll();
        });
        /** @var \Security\Resource $resource */
        foreach ($resources as $resource) {
            $this->acl->addResource($resource->getName());
        }

        /** @var array $permissions */
        $permissions = $cache->load('permissions', function (& $dependencies) use ($em) {
            $dependencies = [Cache::TAGS => [self::CACHE_NAMESPACE . '/permissions']];
            return $em->getRepository(Permission::class)->findAll();
        });
        /** @var Permission $permission */
        foreach ($permissions as $permission) {
            $method = $permission->getMethod();
            $resource = $permission->getResource();
            $role = $permission->getRole();

            if ($permission->getCreate()) {
                $this->acl->$method($role->getName(), $resource->getName(), self::CREATE);
            }
            if ($permission->getRead()) {
                $this->acl->$method($role->getName(), $resource->getName(), self::READ);
            }
            if ($permission->getUpdate()) {
                $this->acl->$method($role->getName(), $resource->getName(), self::UPDATE);
            }
            if ($permission->getDelete()) {
                $this->acl->$method($role->getName(), $resource->getName(), self::DELETE);
            }
        }
        $this->acl->allow(Role::ROOT);
    }

    /**
     * @inheritdoc
     */
    public function isAllowed($role, $resource, $privilege): bool
    {
        while ($resource) {
            try {
                return $this->acl->isAllowed($role, $resource, $privilege);
            } catch (InvalidStateException $_) {}
            $pos = strrpos($resource, ':');
            if ($pos === false) {
                return false;
            }
            $resource = substr($resource, 0, $pos);
        }
        return false;
    }

}
