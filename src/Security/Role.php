<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Security;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="roles")
 * @method string getName()
 * @method Role|null getParent()
 * @method setParent(Role|null $role)
 */
class Role
{

    public const ROOT = 'root';
    public const ADMIN = 'admin';
    public const USER = 'user';
    public const GUEST = 'guest';

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string", unique=true, options={"comment"="Název role"})
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Security\Role")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Role|null
     */
    protected $parent;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function setName(string $name): void
    {
        $this->name = strtolower($name);
    }

}
