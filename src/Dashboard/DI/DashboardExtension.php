<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Dashboard\DI;

use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use App\Extensions\Providers\IImagesProvider;

class DashboardExtension extends CompilerExtension implements IJsAdminProvider, IImagesProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping(
            $this->getContainerBuilder(), ['Dashboard' => 'Dashboard\\*Module\\Presenters\\*Presenter']
        );
    }

    /** @return string[] */
    public function getJsAdminFiles(): array
    {
        return [\dirname(__DIR__) . '/assets/js/dashboard.js'];
    }

    /**@return array|string[]|string*/
    public function getImages()
    {
        return \dirname(__DIR__) . '/assets/images';
    }

}
