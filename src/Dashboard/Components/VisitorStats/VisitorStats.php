<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Dashboard\Components\VisitorStats;

use App\Components\BaseControl;
use Dashboard\Components\VisitorStats\Chart\Chart;
use Dashboard\Components\VisitorStats\Chart\IChartFactory;
use Dashboard\Components\VisitorStats\Panels\Panel;
use Dashboard\Components\VisitorStats\Panels\PanelValue;
use Stats\StatsFacade;

//TODO testy (Unit)
class VisitorStats extends BaseControl
{

    private const PANELS_TEMPLATE = __DIR__ . '/templates/Panels.latte',
        MAP_TEMPLATE = __DIR__ . '/templates/Map.latte',
        CHART_TEMPLATE = __DIR__ . '/templates/Chart.latte';

    /** @var StatsFacade */
    private $statsFacade;

    public function __construct(StatsFacade $statsFacade)
    {
        parent::__construct();
        $this->statsFacade = $statsFacade;
    }

    public function render(): void
    {
        $this->renderPanels();
        $this->renderMap();
        $this->renderChart();
    }

    public function renderMap(): void
    {
        $this->template->setFile(self::MAP_TEMPLATE);
        $this->template->render();
    }

    public function renderChart(): void
    {
        $this->template->setFile(self::CHART_TEMPLATE);
        $this->template->render();
    }

    public function renderPanels(): void
    {
        $avgVisits = $this->statsFacade->getAvg();
        $avgUniqueVisits = $this->statsFacade->getAvg(null, true);
        $this->template->panels = [
            (new Panel('Total Visits'))
                ->addValue(new PanelValue('All time', $this->statsFacade->countTotal(), null))
                ->addValue(new PanelValue('Week', $this->statsFacade->countTotal('-7 days'),
                    $this->difference($avgVisits, $this->statsFacade->getAvg(7))))
                ->addValue(new PanelValue('Today', $this->statsFacade->countTotal('today'),
                    $this->difference($avgVisits, $this->statsFacade->getAvg(1)))),
            (new Panel('Unique Visitors'))
                ->addValue(new PanelValue('All time', $this->statsFacade->countTotal(null, true), null))
                ->addValue(new PanelValue('Week', $this->statsFacade->countTotal('-7 days', true),
                    $this->difference($avgUniqueVisits, $this->statsFacade->getAvg(7, true))))
                ->addValue(new PanelValue('Today', $this->statsFacade->countTotal('today', true),
                    $this->difference($avgUniqueVisits, $this->statsFacade->getAvg(1, true)))),
        ];
        $this->template->setFile(self::PANELS_TEMPLATE);
        $this->template->render();
    }

    protected function createComponentChart(IChartFactory $factory): Chart
    {
        return $factory->create();
    }

    private function difference(float $first, float $second): float
    {
        return $first === 0.0 ? 0 : 100 * $second / $first - 100;
    }

}
