<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Dashboard\Components\VisitorStats\Panels;

/** Value object */
class PanelValue
{

    /** @var string */
    private $name;

    /** @var int */
    private $value;

    /** @var float|null */
    private $trend;

    public function __construct(string $name, int $value, ?float $trend = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->trend = $trend;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function getTrend(): ?float
    {
        return $this->trend;
    }

    public function setName(string $name): PanelValue
    {
        $this->name = $name;
        return $this;
    }

    public function setValue(int $value): PanelValue
    {
        $this->value = $value;
        return $this;
    }

    public function setTrend(?float $trend): PanelValue
    {
        $this->trend = $trend;
        return $this;
    }

}
