let chart, charts, ctx, json;
window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.chartjs')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;
            let chart, charts, ctx, json;

            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    chartjs()
                })
                .teardown(function () {
                    setup = false;
                    chart = chart = charts = ctx = json = null;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);
chartjs();
function chartjs() {
    charts = document.getElementsByClassName("chartjs");
    for (let i = 0; i < charts.length; i++) {
        ctx = charts[i].getContext('2d');
        json = JSON.parse(charts[i].dataset.chart);
        chart = new Chart(ctx, json);
    }

}
