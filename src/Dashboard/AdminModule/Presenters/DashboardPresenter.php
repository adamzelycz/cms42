<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Dashboard\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use Dashboard\Components\VisitorStats\IVisitorStatsFactory;
use Dashboard\Components\VisitorStats\VisitorStats;
use Stats\Components\LastRefresh\ILastRefreshFactory;
use Stats\Components\LastRefresh\LastRefresh;

class DashboardPresenter extends BasePresenter
{

    public function actionDefault(): void
    {
        $this->setTitle('DashBoard');
        $this->addHint('DashBoard',
            'This is a Dashboard, you can check your daily stats. If you have admin rights you can see all
         stats, summaries and logs by clicking "Show all" or choose "Stats" section in the menu');
    }

    protected function createComponentVisitorStats(IVisitorStatsFactory $factory): VisitorStats
    {
        return $factory->create();
    }

    protected function createComponentLastRefresh(ILastRefreshFactory $factory): LastRefresh
    {
        return $factory->create();
    }

}
