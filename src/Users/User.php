<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @method string getPassword()
 * @method string getUsername()
 * @method string getIp()
 * @method \Security\Role[] getRoles()
 * @method setPassword(string $password)
 * @method setUsername(string $username)
 * @method setIp(string $ip)
 * @method addRole(\Security\Role $role)
 */
class User
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="string", unique=true, options={"comment"="Login of user"})
     * @var string
     */
    protected $username;

    /**
     * @ORM\Column(type="string", options={"comment"="Hashed password of user"})
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment"="Last Ip used for connection of user"})
     * @var string
     */
    protected $ip;

    /**
     * @ORM\ManyToMany(targetEntity="Security\Role")
     * @var \Security\Role[]|ArrayCollection
     */
    protected $roles;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
        $this->roles = new ArrayCollection();
    }

    public function cleanRoles(): void
    {
        $this->roles->clear();
    }

}
