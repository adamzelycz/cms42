<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users\DI;

use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;
use Pages\Helpers\RenderableControl;
use Users\Components\SignInForm\ISignInFormFactory;

class UsersExtension extends CompilerExtension implements IEntityProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function getEntityMappings(): array
    {
        return ['User' => __DIR__ . '/../'];
    }

    public function beforeCompile(): void
    {
        $control = new RenderableControl(ISignInFormFactory::class, 'Sign in form');
        $this->setRenderableControls([$control]);
        $this->setPresenterMapping($this->getContainerBuilder(), ['Users' => 'Users\\*Module\\Presenters\\*Presenter']);
    }

}
