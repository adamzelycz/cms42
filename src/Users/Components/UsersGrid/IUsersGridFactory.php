<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Users\Components\UsersGrid;

interface IUsersGridFactory
{

    public function create(): UsersGrid;

}
