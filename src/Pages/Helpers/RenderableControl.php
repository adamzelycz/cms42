<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Helpers;

use Nette\SmartObject;
use function interface_exists;

/** Value Object */
class RenderableControl
{

    use SmartObject;

    /** @var string automatic assign*/
    private $hash;

    /** @var string */
    private $name;

    /** @var string Path to a control class or factory interface*/
    private $classPath;

    /** @var string */
    private $description;

    public function __construct(string $classPath, string $name)
    {
        if (!class_exists($classPath) && !interface_exists($classPath)) {
            throw new \InvalidArgumentException('Class ' . $classPath . ' does not exists');
        }
        $this->classPath = $classPath;
        $this->name = $name;

        $this->hash = md5($classPath);
    }

    /** @return string[] */
    public function toArray(): array
    {
        return [
            'hash' => $this->hash,
            'name' => $this->name,
            'description' => $this->description,
            'classPath' => $this->classPath,
        ];
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getClassPath(): string
    {
        return $this->classPath;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

}
