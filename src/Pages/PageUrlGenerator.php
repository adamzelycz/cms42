<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Url\Url;

class PageUrlGenerator
{

    public function generate(Page $page, ArrayHash $values): Url
    {
        $url = new Url();
        $url->setUrlPath($values->url ?? Strings::webalize($values->title));
        $url->setDestination('Pages:Front:Pages');
        $url->setInternalId($page->getId());
        return $url;
    }

}
