<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Locale\Locale;
use Url\Url;
use Users\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="pages")
 * @method bool getHideTitle()
 * @method string|null getPassword()
 * @method User[]|ArrayCollection getAuthors()
 * @method Url|null getUrl()
 * @method string getCustomCss()
 * @method string getCustomJs()
 * TODO permissions, ogs, files, index
 */
class Page
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\OneToMany(targetEntity="Pages\PageLocale", mappedBy="page", cascade={"persist"})
     * @var PageLocale[]|ArrayCollection
     */
    protected $pageLocales;

    /**
     * @ORM\Column(type="boolean", options={"comment":"Hide title on the page"})
     * @var bool
     */
    protected $hideTitle = false;

    /**
     * @ORM\OneToOne(targetEntity="Url\Url", cascade={"persist"})
     * @ORM\JoinColumn(name="url_id", referencedColumnName="id", onDelete="SET NULL")
     * @var Url|null
     */
    protected $url;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Password protection for pages"})
     * @var string|null
     */
    protected $password;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $isPublished = false;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @var \DateTimeImmutable|null
     */
    protected $publishedAt;

    /**
     * @ORM\ManyToMany(targetEntity="Users\User")
     * @ORM\JoinTable(name="page_author")
     * @var ArrayCollection|User[]
     */
    protected $authors;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $customCss = '';

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $customJs = '';

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->pageLocales = new ArrayCollection();
    }

    //PAGE LOCALES VARIANTS
    public function addPageLocale(PageLocale $pageLocale): void
    {
        if ($this->getPageLocale($pageLocale->getLocale()) === null) {
            $this->pageLocales[] = $pageLocale;
        }
        $pageLocale->setPage($this);
    }

    public function clearPageLocales(): void
    {
        $this->pageLocales->clear();
    }

    public function getPageLocale(Locale $locale): ?PageLocale
    {
        foreach ($this->getPageLocales() as $pageLocale) {
            if ($pageLocale->getLocale()->getId() === $locale->getId()) {
                return $pageLocale;
            }
        }
        return null;
    }

//    public function getPageLocale(Locale $locale): PageLocale
//    {
//        $pageLocale =  $this->pageLocales[$locale->getId()];
//        if ($pageLocale === null && $createIfNotExists) {
//            $pageLocale = new PageLocale($this, $locale);
//            $this->addPageLocale($pageLocale);
//        }
//        return $pageLocale;
//
//        return $this->pageLocales[$locale->getId()];
//    }

    /** @return PageLocale[] */
    public function getPageLocales(): array
    {
        return $this->pageLocales->toArray();
    }

    //PUBLISH
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    public function publish(bool $published = true, ?\DateTimeImmutable $publishedAt = null): Page
    {
        $this->isPublished = $published;
        if ($published && $publishedAt === null) {
            $publishedAt = new \DateTimeImmutable();
        }
        $this->setPublishedAt($publishedAt);
        return $this;
    }

    //TITLE + HTML TAGS
//    public function setTitle(Locale $locale, string $title): void
//    {
//        $this->getPageLocale($locale, true)->setTitle($title);
//    }

    public function hideTitle(bool $hide = true): void
    {
        $this->hideTitle = $hide;
    }

    public function setPublishedAt(?\DateTimeImmutable $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

//    public function setHtmlTitle(?string $htmlTitle): void
//    {
//        $this->htmlTitle = $htmlTitle;
//    }

//    public function setDescription(?string $description): void
//    {
//        $this->description = $description;
//    }

    //CUSTOM ASSETS
    public function setCustomCss(string $customCss): void
    {
        $this->customCss = $customCss;
    }

    public function setCustomJs(string $customJs): void
    {
        $this->customJs = $customJs;
    }

    //URL
    public function setUrl(Url $url): void
    {
        $this->url = $url;
    }

    //PERMISSIONS
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function isProtected(): bool
    {
        return $this->password !== null;
    }

    //CONTENT
//    /** @return ArrayCollection|PageContent[]|PersistentCollection */
//    public function getAllContent()
//    {
//        return $this->content;
//    }

//    public function addContent(PageContent $content): void
//    {
//        $this->content[] = $content;
//        $content->setPageLocale($this);
//    }

//    public function getLatestContent(): ?PageContent
//    {
//        return $this->content->last() ?: null;
//    }

    //AUTHORS
    public function addAuthor(User $author): void
    {
        if (!$this->authors->contains($author)) {
            $this->authors->add($author);
        }
    }

    public function removeAuthor(User $author): void
    {
        $this->authors->removeElement($author);
    }

    public function clearAuthors(): void
    {
        $this->authors->clear();
    }

}
