<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Url\Exceptions\RouteAlreadyExists;
use Users\User;

class PagesFacade
{

    /** @var EntityManager */
    private $em;

    /** @var Logger */
    private $monolog;

    /** @var PagesFiller */
    private $filler;

    /** @var PagesDeleter */
    private $deleter;

    /** @var PagesPublisher */
    private $publisher;

    public function __construct(
        EntityManager $em,
        Logger $monolog,
        PagesFiller $filler,
        PagesDeleter $deleter,
        PagesPublisher $publisher
    )
    {
        $this->em = $em;
        $this->monolog = $monolog;
        $this->filler = $filler;
        $this->deleter = $deleter;
        $this->publisher = $publisher;
    }

    public function save(Page $page, PageLocale $pageLocale, ArrayHash $values): void
    {
        $pageLocale = $this->fillPageValues($page, $pageLocale, $values);
        $page = $pageLocale->getPage();
        $this->em->transactional(function () use ($page, $values, $pageLocale): void {
            $this->em->persist($pageLocale);
            $this->em->persist($page);
            $this->em->flush();
            $this->setUrl($page, $values);
        });
        $this->monolog->addDebug(sprintf('Saving page with ID %s', $page->getId()));
    }

    public function publish(Page $page, bool $publish = true): void
    {
        $this->monolog->addDebug(sprintf('Publishing page with ID %s', $page->getId()));
        $this->publisher->publish($page, $publish);
    }

    public function deletePage(Page $page): void
    {
        $this->monolog->addDebug('Deleting page', [$page->getId()]);
        $this->deleter->deletePage($page);
    }

    public function deletePageLocale(PageLocale $pageLocale): void
    {
        $this->monolog->addDebug('Deleting page locale', [$pageLocale->getId()]);
        $this->deleter->deletePageLocale($pageLocale);
    }

    private function fillPageValues(Page $page, PageLocale $pageLocale, ArrayHash $values): PageLocale
    {
        //LOCALE
        if ($this->haveToChangeLocale($pageLocale, $values)) {
            $pageLocale = $this->filler->setLocale($page, $pageLocale, $values);
        }

        //BASIC
        $pageLocale->setTitle($values->title);
        $pageLocale->setHtmlTitle($values->htmlTitle === '' ? null : $values->htmlTitle);
        $pageLocale->setDescription($values->description === '' ? null : $values->description);
        $this->filler->setContent($pageLocale, $values);

        $page->hideTitle($values->hideTitle);
        $page->setCustomCss($values->customCss);
        $page->setCustomJs($values->customJs);

        //PERMISSIONS
        if ($values->passwordNone) {
            $page->setPassword(null);
        } elseif ($values->password !== '') {
            $page->setPassword(Passwords::hash($values->password));
        } //else let same password

        //AUTHORS
        $page->clearAuthors();
        if (\is_array($values->authors) || $values->authors instanceof \Traversable) {
            foreach ($values->authors as $authorId) {
                /** @var User $author */
                $author = $this->em->getPartialReference(User::class, $authorId);
                $page->addAuthor($author);
            }
        }
//        $page->addPageLocale($pageLocale);
        return $pageLocale;
    }

    private function setUrl(Page $page, ArrayHash $values): void
    {
        try {
            $this->filler->setUrl($page, $values);
        } catch (UniqueConstraintViolationException $exception) {
            $this->monolog->addError('Route with URL Path %s already exists');
            throw new RouteAlreadyExists('Route with this URL Path already exists. Change it to something else please');
        }
    }

    private function haveToChangeLocale(PageLocale $pageLocale, ArrayHash $values): bool
    {
        return $pageLocale->getLocale()->getCode() !== $values['locale'];
    }

}
