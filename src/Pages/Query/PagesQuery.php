<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Query;

use Kdyby\Doctrine\QueryBuilder;
use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;

class PagesQuery extends QueryObject
{

    /** @var array|\Closure[] */
    private $filter = [];

    /** @var array|\Closure[] */
    private $select = [];

    public function isPublished(): PagesQuery
    {
        $this->filter[] = function (QueryBuilder $qb): void {
            $qb->whereCriteria(['p.isPublished' => true]);
        };
        return $this;
    }

    public function whereId(int $id): PagesQuery
    {
        $this->filter[] = function (QueryBuilder $qb) use ($id): void {
            $qb->andWhere('p.id = :id')->setParameter('id', $id);
        };
        return $this;
    }

    public function withPageLocales(): PagesQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->leftJoin('p.pageLocales', 'pl')->addSelect('pl');
        };
        return $this;
    }

    public function withLocale(): PagesQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->leftJoin('pl.locale', 'l')->addSelect('l');
        };
        return $this;
    }

    public function withUrls(): PagesQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->leftJoin('p.url', 'u')->addSelect('u');
        };
        return $this;
    }

    public function withContents(): PagesQuery
    {
        $this->select[] = function (QueryBuilder $qb): void {
            $qb->leftJoin('pl.content', 'c')->addSelect('c');
        };
        return $this;
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder */
    protected function doCreateQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository);
        foreach ($this->select as $modifier) {
            $modifier($qb);
        }
        return $qb;
    }

    /** @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder|QueryBuilder */
    protected function doCreateCountQuery(Queryable $repository)
    {
        $qb = $this->createBasicDql($repository)
            ->select('COUNT(p.id) AS total_count');
        return $qb;
    }

    private function createBasicDql(Queryable $repository): QueryBuilder
    {
        $qb = $repository->createQueryBuilder('p', 'p.id');
        foreach ($this->filter as $modifier) {
            $modifier($qb);
        }
        return $qb;
    }

}
