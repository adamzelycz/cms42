<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\DI;

use App\Components\Css\Providers\ICssProvider;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;

class PagesExtension extends CompilerExtension implements IEntityProvider, ICssProvider, IJsAdminProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function getEntityMappings(): array
    {
        return ['Page' => __DIR__ . '/..'];
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping($this->getContainerBuilder(), ['Pages' => 'Pages\\*Module\\Presenters\\*Presenter']);
    }

    /** @return string[] */
    public function getCssFiles(): array
    {
        return [__DIR__ . '/../assets/css/pages.css'];
    }

    /** @return string[] */
    public function getJsAdminFiles(): array
    {
        return [\dirname(__DIR__) . '/assets/js/page-editor.js'];
    }

}
