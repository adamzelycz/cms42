function pageInit() {
    const select = $('.page-editor-locale-select select');
    const defaultValue = select.val();
    select.on('change', function () {
        const currentValue = $(this).val();
        const action = $(this).parent().parent().parent().find('.page-editor-locale-action');
        action.css('display', currentValue !== defaultValue ? 'block' : 'none');
        const warning = $(this).parent().parent().parent().find('.page-editor-locale-warning').first();
        const text = $(this).find('option:selected').first().text();
        warning.html('WARNING! You can overwrite eventually existing page in <strong>' + text + '</strong>');
    });

    // const actions = $('.page-editor-locale-action input[type=radio]');
    // actions.on('change', function () {
    //     const warning = $(this).parent().parent().parent().find('.page-editor-locale-warning').first();
    //     if (this.value === 'change') {
    //         const selected = $(this).parent().parent().parent().parent().find('.page-editor-locale-select select').first();
    //         const text = selected.find('option:selected').first().text();
    //         warning.html('WARNING! You can overwrite eventually existing page in <strong>' + text + '</strong>');
    //     } else {
    //         warning.text('');
    //     }
    // });
}

window._stack.push([function (di, DOM) {
    let setup = false;
    let router = di.getService('router');
    let page = di.getService('page');

    router.getDOMRoute('.page-editor')
        .on('match', function (evt) {
            if (setup) return;
            setup = true;

            page.getSnippet('snippet--pageContent')
                .setup(function () {
                    pageInit();
                })
                .teardown(function () {
                    setup = false;
                });
        });
}, {
    DOM: 'Utils.DOM'
}]);
