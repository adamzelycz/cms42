<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="page_content")
 * @method string getContent()
 * @method \DatetimeImmutable getCreatedAt()
 * @method setCreatedAt(\DatetimeImmutable $version)
 * @method setContent(string $content)
 * @method setPageLocale(PageLocale $pageLocale)
 */
class PageContent
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\ManyToOne(targetEntity="Pages\PageLocale", inversedBy="content")
     * @ORM\JoinColumn(name="page_locale_id", referencedColumnName="id", onDelete="CASCADE")
     * @var PageLocale
     */
    protected $pageLocale;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $content = '';

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    protected $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

}
