<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\FrontModule\Presenters;

use App\Components\Flashes\Flashes;
use App\FrontModule\Presenters\BasePresenter;
use Kdyby\Doctrine\EntityManager;
use Locale\LocaleFacade;
use Monolog\Logger;
use Pages\Components\AuthenticationForm\AuthenticationForm;
use Pages\Components\AuthenticationForm\IAuthenticationFormFactory;
use Pages\Components\PageEditBar\IPageEditBarFactory;
use Pages\Components\PageEditBar\PageEditBar;
use Pages\Helpers\RenderControls;
use Pages\Page;
use Pages\PageContent;
use Pages\PageLocale;
use Pages\PagesAuthenticator;
use Pages\Query\PagesQuery;
use Security\Authorizator;
use function array_shift;

//TODO testy (Unit)
class PagesPresenter extends BasePresenter
{

    /** @var Logger @inject */
    public $monolog;

    /** @var EntityManager */
    private $em;

    /** @var Page|null */
    private $page;

    /** @var PagesAuthenticator */
    private $pageAuthenticator;

    /** @var RenderControls */
    private $renderControls;

    /** @var PageLocale */
    private $pageLocale;

    /** @var LocaleFacade */
    private $localeFacade;

    public function __construct(
        EntityManager $em,
        PagesAuthenticator $pageAuthenticator,
        RenderControls $renderControls,
        LocaleFacade $localeFacade
    )
    {
        parent::__construct();
        $this->em = $em;
        $this->pageAuthenticator = $pageAuthenticator;
        $this->renderControls = $renderControls;
        $this->localeFacade = $localeFacade;
    }

    public function startup(): void
    {
        parent::startup();
        $this->renderControls->setPresenter($this);
    }

    //TODO cache
    public function actionDefault(int $id): void
    {
        $this->loadPage($id);
        /** @var Page $page */
        $page = $this->page;
        $pageLocale = $this->pageLocale;
        if (!$page->isPublished() && !$this->user->isAllowed('Pages:Admin:Pages', Authorizator::READ)) {
            $this->error();
        }

        if ($page->isProtected() && !$this->pageAuthenticator->isAuthenticated($page)) {
            $this->redirect('protected', $id);
        }

        $this->setTitle($pageLocale->getHtmlTitle() ?? $pageLocale->getTitle());
        if ($pageLocale->getDescription()) {
            $this->setDescription($pageLocale->getDescription());
        }
        $this->template->contentWithControls = $this->getContentWithControls();
        $this->template->page = $page;
        $this->template->pageLocale = $pageLocale;
    }

    public function actionPreview(int $id, ?int $contentId): void
    {
        $pageLocale = $this->em->getRepository(PageLocale::class)->find($id);
        if ($pageLocale === null) {
            $this->flashMessage('Page with this ID does not exists. Redirecting to Pages list.', Flashes::ERROR);
            $this->redirect(':Pages:Admin:Pages:');
            return;
        }
        if ($contentId !== null) {
            /** @var PageContent $content */
            $content = $this->em->getRepository(PageContent::class)->find($contentId);
            if (!$pageLocale->getAllContent()->contains($content)) {
                $this->flashMessage('Page with this id does not contain selected content version. Redirecting to Pages list.', Flashes::ERROR);
                $this->redirect(':Pages:Admin:Pages:');
                return;
            }
        } else {
            /** @var PageContent $content */
            $content = $pageLocale->getLatestContent();
        }

        $this->template->page = $pageLocale->getPage();
        $this->template->pageLocale = $pageLocale;
        $this->template->content = $this->renderControls->render($content->getContent() ?? '');
        $this->setTitle('Pages – Preview');
    }

    public function actionProtected(int $id): void
    {
        $this->loadPage($id);
        /** @var Page $page */
        $page = $this->page;
        if (!$page->isProtected() || $this->pageAuthenticator->isAuthenticated($page)) {
            $this->redirect('default', $id);
        }

        $this->template->pageLocale = $this->pageLocale;
        $this->setNoIndex();
        $this->setTitle('pages.title', true);
    }

    protected function createComponentPageEditBar(IPageEditBarFactory $factory): PageEditBar
    {
        /** @var Page $page */
        $page = $this->page;
        $control = $factory->create($page);
        $control->onDelete[] = function (PageEditBar $control): void {
            $this->flashMessage('Page was deleted', Flashes::SUCCESS);
            $this->redirect(':Pages:Admin:Pages:default');
        };
        return $control;
    }

    protected function createComponentAuthenticationForm(IAuthenticationFormFactory $factory): AuthenticationForm
    {
        /** @var Page $page */
        $page = $this->page;
        $control = $factory->create($page);
        $control->onBadPassword[] = function (AuthenticationForm $control, Page $page): void {
            $this->flashMessage($this->translator->translate('forms.authenticationForm.password.bad'), Flashes::ERROR);
            $this->postGet('this');
        };
        $control->onSuccess[] = function (AuthenticationForm $control, Page $page): void {
            $this->redirect('default', $page->getId());
        };
        return $control;
    }

    private function loadPage(int $id): void
    {
        if ($this->page === null) {
            $qb = (new PagesQuery())->withPageLocales()->whereId($id)->withContents();
            $pages = $this->em->getRepository(Page::class)->fetch($qb)->toArray();
            if (!$pages) {
                $this->error();
            }
            $this->page = array_shift($pages);
        }
        if ($this->pageLocale === null) {
            $locale = $this->localeFacade->getOneByCode($this->locale) ?? $this->localeFacade->getDefault();
            if ($locale === null) {
                $this->monolog->addInfo('No locales found, please create at least one');
                $this->error();
            }
            $pageLocale = $this->page->getPageLocale($locale);
            if ($pageLocale === null) {
                $pageLocales = $this->page->getPageLocales();
                if (!$pageLocales) {
                    $message = 'No locale found for this page (ID): ' . $this->page->getId();
                    $this->monolog->addError($message, [$this->page]);
                    $this->error($message);
                }
                $pageLocale = array_shift($pageLocales); //any possible language
            }
            $this->pageLocale = $pageLocale;
        }
    }

    private function getContentWithControls(): string
    {
        /** @var PageLocale $pageLocale */
        $pageLocale = $this->pageLocale;
        $latestContent = $pageLocale->getLatestContent();
        return $latestContent === null ? '' : $this->renderControls->render($latestContent->getContent());
    }

}
