<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\AuthenticationForm;

use Pages\Page;

interface IAuthenticationFormFactory
{

    public function create(Page $page): AuthenticationForm;

}
