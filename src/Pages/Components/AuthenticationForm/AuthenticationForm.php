<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\AuthenticationForm;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Pages\Exceptions\BadPassword;
use Pages\Page;
use Pages\PagesAuthenticator;

/**
 * @method onSuccess(AuthenticationForm $self, Page $page)
 * @method onBadPassword(AuthenticationForm $self, Page $page)
 */
class AuthenticationForm extends BaseControl
{

    use TFormFactory;

    /** @var callable[] */
    public $onBadPassword = [];

    private const TEMPLATE = __DIR__ . '/AuthenticationForm.latte';

    /** @var Translator */
    private $translator;

    /** @var Page */
    private $page;

    /** @var PagesAuthenticator */
    private $pagesAuthenticator;

    public function __construct(Page $page, Translator $translator, PagesAuthenticator $pagesAuthenticator)
    {
        parent::__construct();
        $this->page = $page;
        $this->translator = $translator;
        $this->pagesAuthenticator = $pagesAuthenticator;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = $this->getForm();
        $form->setTranslator($this->translator->domain('forms.authenticationForm'));

        $form->addPassword('password', 'password.label')->setRequired('password.required');

        $form->addSubmit('submit', 'submit.label');
        $form->onSuccess[] = [$this, 'formSucceeded'];

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        if (!Passwords::verify($values->password, $this->page->getPassword())) {
            $this->onBadPassword($this, $this->page);
            $form->addError('password.bad');
            $this->presenter->sendPayload();
            return;
        }

        try {
            $this->pagesAuthenticator->authenticate($this->page, $values->password);
            $this->onSuccess($this, $this->page);
        } catch (BadPassword $exception) {
            $this->onBadPassword($this, $this->page);
            $form->addError('password.bad');
            $this->presenter->sendPayload();
        }
    }

}
