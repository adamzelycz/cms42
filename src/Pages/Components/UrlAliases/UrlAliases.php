<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\UrlAliases;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Security\Authorizator;
use Url\Forms\UrlPathControl;
use Url\Url;
use Url\UrlFacade;

/**
 * @method onDelete(UrlAliases $self, array $urls)
 * @method onAdd(UrlAliases $self, array $urls)
 */
class UrlAliases extends BaseControl
{

    use TFormFactory;

    /** @var callable[] */
    public $onAdd = [];

    /** @var callable[] */
    public $onDelete = [];

    /** @var Url[] */
    private $urls;

    /** @var EntityManager */
    private $em;

    /** @var UrlFacade */
    private $urlFacade;

    /** @var Url */
    private $mainUrl;

    /** @param Url[] $urls */
    public function __construct(Url $mainUrl, array $urls, EntityManager $em, UrlFacade $urlFacade)
    {
        parent::__construct();
        $this->urls = $urls;
        $this->em = $em;
        $this->urlFacade = $urlFacade;
        $this->mainUrl = $mainUrl;
    }

    private const TEMPLATE = __DIR__ . '/UrlAliases.latte';

    public function render(): void
    {
        $this->template->urls = $this->urls;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleDelete(int $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        /** @var Url $url */
        $url = $this->em->getPartialReference(Url::class, $id);
        if ($url !== null) {
            $index = $this->urlArraySearch($url);
            if ($index !== null) {
                $this->urlFacade->delete($url);
                unset($this->urls[$index]);
                $this->redrawControl('aliases');
                $this->onDelete($this, $this->urls);
            }
        }
        $this->postGet('this');
    }

    protected function createComponentAddForm(): Form
    {
        $form = new Form();
        $form['urlAlias'] = new UrlPathControl();
        $form->addSubmit('add', 'Add');
        $form->onSuccess[] = [$this, 'addFormSucceeded'];
        return $form;
    }

    /** @param array|mixed[] $values*/
    public function addFormSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::CREATE);
        $url = new Url();
        $url->setUrlPath($values['urlAlias']);
        $url->setRedirect($this->mainUrl);
        try {
            $this->urlFacade->save($url);
            $this->urls[] = $url;
            $this->onAdd($this, $this->urls);
        } catch (UniqueConstraintViolationException $exception) {
            $form->addError('This URL is not available');
        }
        $this->redrawControl('aliases-form');
        $this->redrawControl('aliases'); //TODO dynamic add
    }

    private function urlArraySearch(Url $searchedUrl): ?int
    {
        $id = $searchedUrl->getId();
        /** @var Url $url */
        foreach ($this->urls as $key => $url) {
            if ($url->getId() === $id) {
                return $key;
            }
        }
        return null;
    }

}
