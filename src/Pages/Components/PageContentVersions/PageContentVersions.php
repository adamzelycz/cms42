<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PageContentVersions;

use App\Components\BaseControl;
use Kdyby\Doctrine\EntityManager;
use Pages\PageContent;
use Pages\PageLocale;

class PageContentVersions extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/PageContentVersions.latte';

    /** @var EntityManager */
    private $em;

    /** @var PageLocale */
    private $pageLocale;

    public function __construct(PageLocale $pageLocale, EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->pageLocale = $pageLocale;
    }

    public function render(): void
    {
        $this->template->pageLocale = $this->pageLocale;
        $this->template->contents = array_reverse($this->pageLocale->getAllContent()->toArray());
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    /** @secured */
    public function handleSelect(int $contentId): void
    {
        /** @var PageContent $content */
        $content = $this->em->getRepository(PageContent::class)->find($contentId);
        if ($content !== null && $this->pageLocale->getAllContent()->contains($content)) {
            $newContent = new PageContent();
            $newContent->setContent($content->getContent());
//            $newContent->setCreatedAt(new \DateTimeImmutable());
            $this->em->transactional(function () use ($newContent, $content): void {
                $this->pageLocale->addContent($newContent);
                $this->em->remove($content);
                $this->em->persist($newContent);
                $this->em->flush();
            });
        }
        $this->onSuccess($this);
//        $this->postGet('this');
    }

}
