<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PageContentVersions;

use Pages\PageLocale;

interface IPageContentVersionsFactory
{

    public function create(PageLocale $pageLocale): PageContentVersions;

}
