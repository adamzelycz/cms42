<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PageEditBar;

use Pages\Page;

interface IPageEditBarFactory
{

    public function create(Page $page): PageEditBar;

}
