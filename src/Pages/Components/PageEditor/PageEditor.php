<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages\Components\PageEditor;

use App\Components\BaseControl;
use App\Components\Flashes\Flashes;
use App\Components\TFormFactory;
use Kdyby\Doctrine\EntityManager;
use Locale\Locale;
use Locale\LocaleFacade;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\SubmitButton;
use Nette\Utils\ArrayHash;
use Pages\Components\DynamicControls\DynamicControls;
use Pages\Components\DynamicControls\IDynamicControlsFactory;
use Pages\Components\PageContentVersions\IPageContentVersionsFactory;
use Pages\Components\PageContentVersions\PageContentVersions;
use Pages\Components\UrlAliases\IUrlAliasesFactory;
use Pages\Components\UrlAliases\UrlAliases;
use Pages\Page;
use Pages\PageLocale;
use Pages\PagesFacade;
use Security\Authorizator;
use Url\Url;
use Url\UrlFacade;
use Users\User;

/**
 * @method onSaveAndRedirect(PageEditor $control, Page $page)
 * @method onSaveAndStay(PageEditor $control, Page $page)
 * @method onDiscard(PageEditor $control, Page $page)
 * @method onException(PageEditor $control, \Throwable $exception)
 * @method onDelete(PageEditor $control)
 * @method onNoLocales(PageEditor $control)
 */
class PageEditor extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/PageEditor.latte';

    //*****<EVENTS>****/

    /** @var callable[] */
    public $onSaveAndRedirect = [];

    /** @var callable[] */
    public $onNoLocales = [];

    /** @var callable[] */
    public $onSaveAndStay = [];

    /** @var callable[] */
    public $onDiscard = [];

    /** @var callable[] */
    public $onDelete = [];

    /** @var callable[] */
    public $onException = [];

    //****</EVENTS>****

//    /** @var string */
//    private $localeCode;

    /** @var Locale */
    private $locale;

    /** @var string[] */
    private $availableLocalesArray = [];

    /** @var PagesFacade */
    private $pagesFacade;

    /** @var Page */
    private $page;

    /** @var PageLocale */
    private $pageLocale;

    /** @var bool */
    private $edit = false;

    /** @var Url[] */
    private $urlAliases;

    /** @var array|mixed[]|array[] */
    private $usersArray;

    public function __construct(
        PagesFacade $pagesFacade,
        UrlFacade $urlFacade,
        EntityManager $em,
        LocaleFacade $localeFacade,
        ?Page $page = null,
        ?string $localeCode = null
    )
    {
        parent::__construct();
        $this->pagesFacade = $pagesFacade;
        if ($page === null) {
            $page = new Page();
        } else {
            $this->edit = true;
        }
        $url = $page->getUrl();
        $this->urlAliases = $url ? $urlFacade->getAliases($url) : [];
        $this->usersArray = $em->getRepository(User::class)->createQueryBuilder('a', 'a.id')
            ->select('a.id, a.username')
            ->getQuery()->getResult();
        $this->usersArray = array_values($this->usersArray); //remove indexes
        $locale = $em->getRepository(Locale::class)->findOneBy(['code' => $localeCode]) ?? $localeFacade->getDefault();
        if ($locale === null) {
            $this->onNoLocales($this);
            $this->error('No locales');
            return;
        }
        $this->locale = $locale;
        $pageLocale = $page->getPageLocale($locale);
        if ($pageLocale === null) {
            $pageLocale = new PageLocale($page, $locale);
            $page->addPageLocale($pageLocale);
        }
        $this->page = $page;

        $this->pageLocale = $pageLocale;
        foreach ($localeFacade->getAll() as $availableLocale) {
            $this->availableLocalesArray[$availableLocale->getCode()] = $availableLocale->getName();
        }
    }

    public function render(): void
    {
        $this->template->page = $this->page;
        $this->template->pageLocale = $this->pageLocale;
        $this->template->edit = $this->edit;
        $this->template->urlAliases = $this->urlAliases;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    //TODO rozdělit na kontejnery
    protected function createComponentForm(): Form
    {
        $form = $this->getForm();
        $form->addProtection();

        $form->addText('title', 'Title')->setRequired();
        $form->addCheckbox('hideTitle', 'Hide title')->setDefaultValue(false);

        $form->addText('htmlTitle', 'HTML title')
            ->setPlaceholder('Same as title')
            ->setAttribute('class', 'character-counter')
            ->addRule(Form::MAX_LENGTH, 'Maximum number of characters is %d', 70);

        $form->addTextArea('description', 'Description')
            ->setAttribute('placeholder', 'Leave empty for default value from settings')
            ->setAttribute('class', 'character-counter')
            ->addRule(Form::MAX_LENGTH, 'Maximum number of characters is %d', 160);

        $form->addSelect('locale', 'Language', $this->availableLocalesArray)
            ->setRequired()
            ->setHtmlId('page-editor-locale-select');
        $form->addRadioList('localeAction',
            'Would you like to change language of current page or add current values as a new one?', [
                'add' => 'Add',
                'change' => 'Change',
            ])->setRequired()->setDefaultValue('change');

        $form->addCheckbox('passwordNone', 'None')->setDefaultValue(true);
        $form->addText('password', 'Password');

        $form->addUrlPath('url', 'URL');

        $form->addSelectize('authors', 'Authors', $this->usersArray, [
            'valueField' => 'id',
            'labelField' => 'username',
            'searchField' => 'username',
        ]);

        $form->addTextArea('customCss', 'Custom CSS');
        $form->addTextArea('customJs', 'Custom JS');

        //TODO znovupoužitelný container zvlášť
        //https://forum.nette.org/cs/27119-jak-vykreslit-kontejner-nette-forms-container
        //https://pla.nette.org/cs/dedicnost-vskompozice

        $form->addSubmit('saveAndStay', 'Save and stay')->onClick[] = [$this, 'saveAndStay'];
        $form->addSubmit('saveAndRedirect', 'Save')->onClick[] = [$this, 'saveAndRedirect'];
        $form->addSubmit('discard', 'Discard')
            ->setValidationScope(false)
            ->onClick[] = [$this, 'discard'];

        $form->addQuill('content');

        $this->setDefaults($form);

        return $form;
    }

    public function discard(): void
    {
        $this->onDiscard($this, $this->page);
        $this->presenter->redirect(':Pages:Admin:Pages:');
    }

    public function saveAndStay(SubmitButton $button): void
    {
        $this->savePage($button);
        $this->onSaveAndStay($this, $this->page);
        $this->presenter->redirect('this', $this->page->getId());
    }

    public function saveAndRedirect(SubmitButton $button): void
    {
        $this->savePage($button);
        $this->onSaveAndRedirect($this, $this->page);
        $this->presenter->redirect(':Pages:Front:Pages:', ['id' => $this->page->getId()]);
    }

    private function savePage(SubmitButton $button): void
    {
        $this->checkPermissions($this->edit ? Authorizator::UPDATE : Authorizator::CREATE);
        try {
            $form = $button->getForm();
            if ($form === null) {
                throw new \UnexpectedValueException('Internal error');
            }
            /** @var ArrayHash $values */
            $values = $form->getValues();

            $this->pagesFacade->save($this->page, $this->pageLocale, $values);
        } catch (\Throwable $exception) {
            //TODO catch TitleCannotBeEmpty
            $this->onException($this, $exception);
            $this->presenter->redirect('this');
        }
    }

    private function setDefaults(Form $form): void
    {
        if (!$this->edit) {
            return; //only for existing pages
        }
        $pageContent = $this->pageLocale->getLatestContent();
        $url = $this->page->getUrl();
        $form->setDefaults([
            'title' => $this->pageLocale->getTitle(),
            'hideTitle' => $this->page->getHideTitle(),
//            'url' => $url ? $url->getUrlPath() : Strings::webalize($this->pageLocale->getTitle()),
            'url' => $url ? $url->getUrlPath() : '',
            'locale' => $this->locale->getCode(),
            'content' => $pageContent ? $pageContent->getContent() : '',
            'htmlTitle' => $this->pageLocale->getHtmlTitle(),
            'description' => $this->pageLocale->getDescription(),
            'passwordNone' => !(bool)$this->page->getPassword(),
            'customCss' => $this->page->getCustomCss(),
            'customJs' => $this->page->getCustomJs(),
        ]);
        $form['authors']->setDefaultValues($this->getAuthorsIds());
    }

    /** @secured */
    public function handlePublish(): void
    {
        $this->checkPermissions(Authorizator::UPDATE, 'Pages:Admin:Pages');
        if ($this->page === null) {
            $this->presenter->sendPayload();
            return;
        }
        $this->pagesFacade->publish($this->page, !$this->page->isPublished());
        if ($this->presenter->isAjax()) {
            $this->redrawControl('publishButton');
            $this->redrawControl('publishedAt');
            $this->postGet('this');
        } else {
            $this->redirect('this');
        }
    }

    /** @secured */
    public function handleDeletePage(): void
    {
        $this->checkPermissions(Authorizator::DELETE, 'Pages:Admin:Pages');
        if ($this->page === null) {
            $this->presenter->sendPayload();
            return;
        }
        $this->pagesFacade->deletePage($this->page);
        $this->onDelete($this);
    }

    /** @secured */
    public function handleDeletePageLocale(): void
    {
        $this->checkPermissions(Authorizator::DELETE, 'Pages:Admin:Pages');
//        if ($this->pageLocale === null) {
//            $this->presenter->sendPayload();
//            return;
//        }
        $this->pagesFacade->deletePageLocale($this->pageLocale);
        $this->onDelete($this);
    }

    protected function createComponentPageContentVersions(IPageContentVersionsFactory $factory): PageContentVersions
    {
        $control = $factory->create($this->pageLocale);
        $control->onSuccess[] = function (PageContentVersions $control): void {
            $this->presenter->flashMessage('Version changed', Flashes::SUCCESS);
            $this->redrawControl('pageEditorContent');
            $this->presenter->redirect('this');
        };
        return $control;
    }

    protected function createComponentUrlAliases(IUrlAliasesFactory $factory): UrlAliases
    {
        $control = $factory->create($this->page->getUrl() ?? new Url(), $this->urlAliases);
        $refreshAliases = function (UrlAliases $control, array $urls): void {
            $this->urlAliases = $urls;
            $this->redrawControl('aliasesCount');
        };
        $control->onAdd[] = $refreshAliases;
        $control->onDelete[] = $refreshAliases;
        return $control;
    }

    protected function createComponentDynamicControls(IDynamicControlsFactory $factory): DynamicControls
    {
        return $factory->create();
    }

    /** @return int[] */
    private function getAuthorsIds(): array
    {
        $authors = $this->page->getAuthors()->toArray();
        $authors = array_map(function (User $author): int {
            return $author->getId();
        }, $authors);
        return $authors;
    }

}
