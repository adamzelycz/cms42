<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Locale\Locale;
use Pages\Exceptions\TitleCannotBeEmpty;

/**
 * @ORM\Entity
 * @ORM\Table(name="pages_locale")
 * @method Page getPage()
 * @method string getTitle()
 * @method string|null getHtmlTitle()
 * @method string getDescription()
 */
class PageLocale
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="pageLocales", cascade={"persist"})
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Page
     */
    protected $page;

    /**
     * @var Locale
     * @ORM\ManyToOne(targetEntity="Locale\Locale", cascade={"persist"})
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $locale;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title = '';

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Custom HTML title tag, same as title if NULL"})
     * @var null|string
     */
    protected $htmlTitle;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Custom HTML description"})
     * @var string|null
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="Pages\PageContent", mappedBy="pageLocale", cascade={"persist"})
     * @var ArrayCollection|PageContent[]
     */
    protected $content;

    public function __construct(Page $page, Locale $locale)
    {
        $this->content = new ArrayCollection();
        $this->setLocale($locale);
        $this->setPage($page);
    }

    //PAGES
    public function setPage(Page $page): void
    {
        $this->page = $page;
        if ($page->getPageLocale($this->locale) === null) {
            $page->addPageLocale($this);
        }
    }

    //LOCALES
    public function getLocale(): Locale
    {
        return $this->locale;
    }

    public function setLocale(Locale $locale): void
    {
        $this->locale = $locale;
    }

    //TITLE + HTML TAGS
    public function setTitle(string $title): void
    {
        if ($title === '') {
            throw new TitleCannotBeEmpty('Title cannot be empty');
        }
        $this->title = $title;
    }

    public function setHtmlTitle(?string $htmlTitle): void
    {
        $this->htmlTitle = $htmlTitle;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    //CONTENT
    /** @return ArrayCollection|PageContent[]|PersistentCollection */
    public function getAllContent()
    {
        return $this->content;
    }

    public function addContent(PageContent $content): void
    {
        $this->content[] = $content;
        $content->setPageLocale($this);
    }

    public function getLatestContent(): ?PageContent
    {
        return $this->content->last() ?: null;
    }

}
