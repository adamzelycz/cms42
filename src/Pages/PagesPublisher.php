<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Pages;

use Kdyby\Doctrine\EntityManager;
use Url\UrlFacade;

class PagesPublisher
{

    /** @var EntityManager */
    private $em;

    /** @var UrlFacade */
    private $urlFacade;

    public function __construct(EntityManager $em, UrlFacade $urlFacade)
    {
        $this->em = $em;
        $this->urlFacade = $urlFacade;
    }

    public function publish(Page $page, bool $publish = true): void
    {
        $page->publish($publish);
        $this->em->persist($page);
        $this->em->flush();
        if ($page->getUrl() !== null) {
            $this->urlFacade->cleanCache($page->getUrl());
        }
    }

}
