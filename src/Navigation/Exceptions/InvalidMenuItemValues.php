<?php declare(strict_types=1);

/** Copyright (c) 2018 Adam Zelycz (https://www.adamzelycz.cz) */

namespace Navigation\Exceptions;

class InvalidMenuItemValues extends \Exception
{

}
