<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="menu")
 * @method string getName()
 * @method MenuItem[]|ArrayCollection getItems()
 * @method setName(string $name)
 */
class Menu
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\Column(type="string", unique=true, options={"comment":"Unique identifier of menu"})
     * @var string
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="MenuItem", mappedBy="menu", cascade={"persist"})
     * @var MenuItem[]|ArrayCollection
     */
    protected $items;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $selected = false;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function addItem(MenuItem $item): void
    {
        $item->setMenu($this);
        $this->items[] = $item;
    }

    /** @param MenuItem[] $items */
    public function setItems(array $items): void
    {
        $this->items->clear();
        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    public function select(bool $selected = true): void
    {
        $this->selected = $selected;
    }

    public function isSelected(): bool
    {
        return $this->selected;
    }

}
