<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Kdyby\Doctrine\EntityManager;
use Navigation\Components\MenuForm\IMenuFormFactory;
use Navigation\Components\MenuForm\MenuForm;
use Navigation\Exceptions\InvalidMenuItemValues;
use Navigation\Menu;
use Navigation\MenuFacade;
use Navigation\Query\MenuQuery;
use Security\Authorizator;

class MenuPresenter extends BasePresenter
{

    /** @var EntityManager */
    private $em;

    /** @var null|Menu */
    private $menu;

    /** @var MenuFacade */
    private $menuFacade;

    public function __construct(EntityManager $em, MenuFacade $menuFacade)
    {
        parent::__construct();
        $this->em = $em;
        $this->menuFacade = $menuFacade;
    }

    public function actionList(): void
    {
        $this->setTitle('Menu list');
        $this->addHint('List', 'You can select from list of menus and open menu editor');
        $this->addHint('Create and delete', 'You can also create and delete menus');
    }

    public function renderList(): void
    {
        $query = (new MenuQuery())->withItems()->withItemsUrls();
        $this->template->menus = $this->em->getRepository(Menu::class)->fetch($query)->toArray();
    }

    public function actionEdit(int $id): void
    {
        $this->setTitle('Menu edit');
        $this->addHint('Text', 'Text that will be displayed. You can use translatable titles, if you type underscore
        before translation key (e.g. "_homepage.title")');
        $this->addHint('URL', 'You can use fully path of external URL or just URL path after domain name (internal) – 
        Internal URLs are preferred');
        $query = (new MenuQuery())->whereId($id)->withItems()->withItemsUrls();
        $menu = $this->em->getRepository(Menu::class)->fetch($query)->toArray();
        if (!$menu) {
            $this->flashMessage('Menu with this ID does not exists, you were redirected to Create menu page');
            $this->redirect('new');
        }
        $this->menu = array_shift($menu);
    }

    public function actionNew(): void
    {
        $this->setTitle('Menu create');
    }

    protected function createComponentMenuForm(IMenuFormFactory $factory): MenuForm
    {
        $control = $factory->create($this->menu);
        $control->onSuccess[] = function (MenuForm $form, Menu $menu): void {
            $this->flashMessage(sprintf('Menu with name %s was successfully saved', $menu->getName()), Flashes::SUCCESS);
            $this->redirect('list');
        };
        $control->onDuplicateName[] = function (MenuForm $form, string $name): void {
            $this->flashMessage(sprintf('Menu with this name (%s) already exists', $name), Flashes::ERROR);
            $this->monolog->addInfo('UniqueConstraint Exception while trying to save menu', [$name]);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onInvalidMenuItemValues[] = function (array $values, InvalidMenuItemValues $exception): void {
            $this->flashMessage($exception->getMessage(), Flashes::ERROR);
            $this->monolog->addInfo($exception->getMessage(), [$values]);
            $this->postGet('this');
            $this->sendPayload();
        };
//        $control->onException[] = function (MenuForm $form, \Throwable $exception, ArrayHash $values) {
//            $this->flashMessage('Error occurred, please try again later', Flashes::ERROR);
//            $this->monolog->addError('Error occurred while trying to save menu', [$exception, $values]);
//            $this->redirect('this');
//        };
        return $control;
    }

    /** @secured */
    public function handleDelete(int $id): void
    {
        $resource = $this->getName() . ':' . $this->getAction();
        if (!$this->user->isAllowed($resource, Authorizator::DELETE)) {
            return;
        }
        /** @var Menu|null $menu */
        $menu = $this->em->getRepository(Menu::class)->find($id);
        if ($menu !== null) {
            if ($menu->isSelected()) {
                $this->flashMessage('This menu cannot be deleted because it is selected. Select or create
                another menu', Flashes::ERROR);
            } else {
                $this->menuFacade->delete($menu);
                $this->flashMessage(sprintf('Menu with id %s was deleted', $id), Flashes::SUCCESS);
            }
        }
        $this->redirect('this');
    }

    /** @secured */
    public function handleSelect(int $id): void
    {
        $resource = $this->getName() . ':' . $this->getAction();
        if (!$this->user->isAllowed($resource, Authorizator::UPDATE)) {
            return;
        }
        /** @var Menu $menu */
        $menu = $this->em->getRepository(Menu::class)->find($id);
        if ($menu !== null) {
            $this->menuFacade->select($menu);
            $this->em->flush();
            $this->flashMessage(sprintf('Menu with id %s was selected', $id), Flashes::SUCCESS);
        }
        $this->redirect('this');
    }

}
