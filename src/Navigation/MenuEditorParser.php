<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation;

use Kdyby\Doctrine\EntityManager;
use Navigation\Exceptions\BadUrlFormat;
use Nette\Utils\Json;
use Nette\Utils\Validators;
use Url\Url;

class MenuEditorParser
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /** @param MenuItem[] $menuItems */
    public function encode(array $menuItems): string
    {
        Validators::everyIs($menuItems, MenuItem::class);
        $itemsArray = [];
        foreach ($menuItems as $item) {
            $itemsArray[] = [
                'href' => $item->getUrlString(),
                'text' => $item->getTitle(),
                'target' => $item->getTarget()->getTarget(),
            ];
        }
        return Json::encode($itemsArray);
    }

    /** @return MenuItem[] */
    public function decode(string $json): array
    {
        $items = [];
        foreach (Json::decode($json, Json::FORCE_ARRAY) as $item) {
            $menuItem = new MenuItem();
            try {
                $externalUrl = new ExternalUrl($item['href']);
                $menuItem->setExternalUrl($externalUrl);
            } catch (\InvalidArgumentException $_) {
//                TODO podle ID, pouze reference (?)
                $url = $this->em->getRepository(Url::class)->findOneBy(['urlPath' => $item['href']]);
                if ($url === null) {
                    throw new BadUrlFormat('Invalid format for external URL and none URL found with given URL Path');
                }
                $menuItem->setInternalUrl($url);
            }
            $menuItem->setTitle($item['text']);
            $menuItem->setTarget(new MenuItemTarget($item['target']));
            $items[] = $menuItem;
        }
        return $items;
    }

}
