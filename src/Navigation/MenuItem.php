<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Url\Url;

/**
 * @ORM\Entity
 * @ORM\Table(name="menu_item")
 * @method setMenu(Menu $menu)
 * @method setTitle(string $menu)
 * @method setInternalUrl(Url $menu)
 * @method setSequence(int $sequence)
 * @method string getTitle()
 * @method Url|null getInternalUrl()
 * @method int getSequence()
 */
class MenuItem
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="items")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Menu
     */
    protected $menu;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $target;

    /**
     * @ORM\ManyToOne(targetEntity="\Url\Url")
     * @ORM\JoinColumn(name="url_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @var null|Url
     */
    protected $internalUrl;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"External url in case of internal url is null"})
     * @var string|null
     */
    protected $externalUrl;

    public function __construct()
    {
        $this->setTarget(new MenuItemTarget());
    }

    public function getUrlString(): string
    {
        $url = $this->getInternalUrl();
        if ($url === null) {
            $url = $this->getExternalUrl();
            if ($url !== null) {
                $url = $url->getUrl();
            }
        } else {
            $url = $url->getUrlPath();
        }
        return $url ?? '#';
    }

    public function setExternalUrl(?ExternalUrl $externalUrl): void
    {
        $this->externalUrl = (string)$externalUrl;
    }

    public function getExternalUrl(): ?ExternalUrl
    {
        return $this->externalUrl === null ? null : new ExternalUrl($this->externalUrl);
    }

    public function setTarget(MenuItemTarget $target): void
    {
        $this->target = $target->getTarget();
    }

    public function getTarget(): MenuItemTarget
    {
        return new MenuItemTarget($this->target);
    }

}
