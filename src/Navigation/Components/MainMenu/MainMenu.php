<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Components\MainMenu;

use App\Components\BaseControl;
use Kdyby\Translation\Translator;
use Navigation\MenuFacade;
use Nette\Http\Url;
use Nette\Utils\Strings;
use Nette\Utils\Validators;

class MainMenu extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/MainMenu.latte';

    /** @var MenuFacade */
    private $menuFacade;

    /** @var Translator */
    private $translator;

    public function __construct(Translator $translator, MenuFacade $menuFacade)
    {
        parent::__construct();
        $this->menuFacade = $menuFacade;
        $this->translator = $translator;
    }

    public function render(): void
    {
        $this->template->setFile($this->getChangedTemplate() ?? self::TEMPLATE);
        $menu = $this->menuFacade->getSelectedMenu();
        $items = $menu !== null ? $menu->getItems() : [];
        $itemsArray = [];
        foreach ($items as $item) {
            //TITLE
            $title = $item->getTitle();
            if (Strings::startsWith($title, '_')) {
                $title = $this->translator->translate(Strings::substring($title, 1));
            }

            //URL
            $internalUrl = $item->getInternalUrl();
            if ($internalUrl !== null) {
                $url = $this->presenter->link($internalUrl->getAbsoluteDestination(), $internalUrl->getInternalId() ?? []); //TODO zbytečný dotaz
            } elseif ($item->getExternalUrl() !== null) {
                $url = $item->getExternalUrl();
            } else {
                $url = '#';
            }

            $currentUrl = $this->presenter->link('this');
            if (Validators::isUrl($currentUrl)) { //may be absolute URL if empty URL path
                $absoluteUrl = new Url($currentUrl);
                $currentUrl = $absoluteUrl->getPath();
            }
            $currentUrl = trim($currentUrl, '/');
            $itemsArray[] = [
                'url' => $url ?: '/', //may be empty string (homepage)
                'title' => $title,
                'target' => $item->getTarget(), //MenuItemTarge
                'active' => $currentUrl === $url,
            ];
        }
        $this->template->items = $itemsArray;
        $this->template->render();
    }

}
