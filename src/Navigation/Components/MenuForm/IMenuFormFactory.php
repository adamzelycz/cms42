<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Navigation\Components\MenuForm;

use Navigation\Menu;

interface IMenuFormFactory
{

    public function create(?Menu $menu): MenuForm;

}
