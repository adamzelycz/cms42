<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Security\Components\Permissions\IPermissionsFactory;
use Security\Components\Permissions\Permissions;
use Security\Components\Resources\IResourcesFactory;
use Security\Components\Resources\Resources;
use Security\Components\Roles\IRolesFactory;
use Security\Components\Roles\Roles;
use Settings\Components\SettingsBar\ISettingsBarFactory;
use Settings\Components\SettingsBar\SettingsBar;
use Settings\Components\SettingsForm\ISettingsFormFactory;
use Settings\Components\SettingsForm\SettingsForm;
use Settings\Option;
use Settings\SettingsQuery;

//TODO locale nastavení
//TODO datetime format
//TODO registration permission

class SettingsPresenter extends BasePresenter
{

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    public function actionDefault(): void
    {
        $this->setTitle('Settings – Basics');
    }

    public function actionPermissions(): void
    {
        $this->setTitle('Settings – Permission');
    }

    public function actionRolesResources(): void
    {
        $this->setTitle('Settings – Roles &amp; Resources');
    }

    public function actionLocalization(): void
    {
        $this->redirect(':Locale:Admin:Translations:list');
    }

    protected function createComponentSettingsBar(ISettingsBarFactory $factory): SettingsBar
    {
        return $factory->create([
            'default' => 'Basic',
            'permissions' => 'Permissions',
            'rolesResources' => 'Roles and Resources',
            'localization' => 'Localization',
        ]);
    }

    protected function createComponentSettingsForm(ISettingsFormFactory $factory): SettingsForm
    {
        /** @var \Kdyby\Doctrine\EntityRepository $repository */
        $repository = $this->em->getRepository(Option::class);
        /** @var ResultSet $resultSet */
        $resultSet = $repository->fetch((new SettingsQuery())->isNotPlugin());
        $control = $factory->create($resultSet->toArray());
        $control->onSuccess[] = function (SettingsForm $control): void {
            $this->monolog->addInfo('Settings were changed');
            $this->flashMessage('Settings were successfully saved', Flashes::SUCCESS);
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

    protected function createComponentPermissions(IPermissionsFactory $factory): Permissions
    {
        $control = $factory->create();
        $control->onSuccess[] = function (Permissions $control): void {
            $this->monolog->addInfo('Permissions settings were changed');
            $this->flashMessage('Permissions settings were successfully saves', Flashes::SUCCESS);
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

    protected function createComponentRoles(IRolesFactory $factory): Roles
    {
        $control = $factory->create();
        $control->onEdit[] = function ($name): void {
            $message = sprintf('Role %s was edited', $name);
            $this->monolog->addInfo($message);
            $this->flashMessage($message, Flashes::SUCCESS);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onCreate[] = function ($name): void {
            $message = sprintf('Role %s was created', $name);
            $this->monolog->addInfo($message);
            $this->flashMessage($message, Flashes::SUCCESS);
            $this->redirect('this');
            $this->sendPayload();
        };
        $control->onExistingName[] = function ($name): void {
            $message = sprintf('Role with name "%s" already exists', $name);
            $this->monolog->addError($message);
            $this->flashMessage($message, Flashes::ERROR);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onDelete[] = function ($name): void {
            $message = sprintf('Role with name "%s" was deleted', $name);
            $this->monolog->addError($message);
            $this->flashMessage($message);
            $this->redirect('this');
            $this->sendPayload();
        };
        return $control;
    }

    protected function createComponentResources(IResourcesFactory $factory): Resources
    {
        $control = $factory->create();
        $control->onEdit[] = function ($name): void {
            $message = sprintf('Resource %s was edited', $name);
            $this->monolog->addInfo($message);
            $this->flashMessage($message, Flashes::SUCCESS);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onCreate[] = function ($name): void {
            $message = sprintf('Resource %s was created', $name);
            $this->monolog->addInfo($message);
            $this->flashMessage($message, Flashes::SUCCESS);
            $this->redirect('this');
            $this->sendPayload();
        };
        $control->onExistingName[] = function ($name): void {
            $message = sprintf('Resource with name "%s" already exists', $name);
            $this->monolog->addError($message);
            $this->flashMessage($message, Flashes::ERROR);
            $this->postGet('this');
            $this->sendPayload();
        };
        $control->onDelete[] = function ($name): void {
            $message = sprintf('Resource with name "%s" was deleted', $name);
            $this->monolog->addError($message);
            $this->flashMessage($message);
            $this->redirect('this');
            $this->sendPayload();
        };
        return $control;
    }

}
