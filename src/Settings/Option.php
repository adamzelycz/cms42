<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\Strings;
use Plugins\Plugin;
use Settings\Exceptions\InvalidOptionType;
use Settings\Rules\Rule;
use Settings\Types\IType;
use Settings\Types\ITypeSelectable;
use Settings\Types\Text;
use function class_exists;

/**
 * @ORM\Entity
 * @ORM\Table(name="settings", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="key_plugin", columns={"option_key", "plugin_id"}),
 *     @ORM\UniqueConstraint(name="key_plugin_null", columns={"option_key"}, options={"where": "(plugin_id IS NULL)"})
 *    })
 * @method string|null getValue()
 * @method string|null getName()
 * @method string getKey()
 * @method string getHint()
 * @method Parameter[] getParameters()
 * @method Rule[] getRules()
 * @method Plugin|null getPlugin()
 * @method array|mixed[]|null getRenderOptions()
 * @method setValue(?string $value)
 * @method setName(string | null $name)
 * @method setPlugin(Plugin $plugin)
 * @method setHint(string | null $hint)
 * @method setRenderOptions(array|mixed[]|null $renderOptions)
 */
class Option
{

    use MagicAccessors;
    use Identifier;

    /**
     * @ORM\Column(type="string", name="option_key", options={"comment":"Key of the option"})
     * @var string
     */
    protected $key;

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Value of selected key"})
     * @var string
     */
    protected $value = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    protected $name;

    /**
     * @ORM\Column(type="string", options={"comment":"Type of form input"})
     * @var string
     */
    protected $type = 'text';

    /**
     * @ORM\Column(type="string", nullable=true, options={"comment":"Small text, that will be displayed under input field"})
     * @var string|null
     */
    protected $hint;

    /**
     * @ORM\ManyToOne(targetEntity="Plugins\Plugin", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, name="plugin_id", referencedColumnName="id")
     * @var Plugin|null
     */
    protected $plugin;

    /**
     * TODO simply as array (?|.) \Settings\Prameter entity is currently useless
     * @var ArrayCollection|Parameter[]|array
     * @ORM\OneToMany(targetEntity="Parameter", mappedBy="option", cascade={"persist"})
     */
    protected $parameters;

    /**
     * @var ArrayCollection|Rule[]
     * @ORM\OneToMany(targetEntity="\Settings\Rules\Rule", mappedBy="option", cascade={"persist"})
     */
    protected $rules;

    /**
     * @ORM\Column(type="array", nullable=true, options={"comment":"Custom Render options for SettingsForm"})
     * @var array|mixed[]|null
     */
    protected $renderOptions;

    public function __construct(string $key)
    {
        $this->key = $key;
        $this->parameters = new ArrayCollection();
        $this->rules = new ArrayCollection();
    }

    public function setType(IType $type): void
    {
        $this->type = $type->getName();
    }

    public function getType(): IType
    {
        $words = explode('_', $this->type);
        $words = array_map(function (string $word) {
            return Strings::firstUpper($word);
        }, $words);
        $className = 'Settings\\Types\\' . implode('', $words);
        if (class_exists($className)) {
            return new $className();
        }
        return new Text(); //default
    }

    /** @param Parameter[] $parameters */
    public function setParameters(array $parameters): void
    {
        if (!$this->isSelectable()) {
            throw new InvalidOptionType('Cannot set parameters to Option with type that does not implement ' .
                ITypeSelectable::class);
        }
        $this->parameters = $parameters;
        foreach ($parameters as $parameter) {
            $parameter->setOption($this);
        }
    }

    public function addParameter(Parameter $parameter): void
    {
        if (!$this->isSelectable()) {
            throw new InvalidOptionType('Cannot set parameters to Option with type that does not implement ' .
                ITypeSelectable::class);
        }
        $this->parameters[] = $parameter;
        $parameter->setOption($this);
    }

    public function addRule(Rule $rule): void
    {
        $this->rules[] = $rule;
        $rule->setOption($this);
    }

    private function isSelectable(): bool
    {
        return $this->getType() instanceof ITypeSelectable;
    }

}
