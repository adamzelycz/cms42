<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Types;

use Nette\Forms\Controls\BaseControl;
use Settings\Option;

interface IType
{

    /** Get simple name of the type. e.g.: "text" */
    public function getName(): string;

    /** Render form input */
    public function getInput(Option $option): BaseControl;

}
