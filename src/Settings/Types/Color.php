<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Types;

use Czubehead\BootstrapForms\Inputs\TextInput;
use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Form;
use Settings\Option;

class Color implements IType
{

    public function getName(): string
    {
        return 'color';
    }

    public function getInput(Option $option): BaseControl
    {
        /** @var TextInput $text */
        $text = (new Text())->getInput($option);
        return $text->setHtmlType('color')
            ->addRule(Form::PATTERN, 'This is not valid color format. Use "#ffffff" format', '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$')
            ->setAttribute('class', ['h-50p'])
            ->setDefaultValue($option->getValue());
    }

}
