<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Components\SettingsForm;

interface ISettingsFormFactory
{

    /** @param \Settings\Option[] $options */
    public function create(array $options): SettingsForm;

}
