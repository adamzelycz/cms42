<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Settings\Rules;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Settings\Option;
use function getType;

/**
 * @ORM\Entity
 * @ORM\Table(name="option_rules")
 * @method string getValidator()
 * @method string|null getErrorMessage()
 * @method mixed|null getArgument()
 * @method setValidator(string $validator)
 * @method setErrorMessage(string | null $errorMessage)
 * @method setOption(Option $option)
 */
class Rule
{

//    private const TYPE_SCALAR = 'scalar',
//        TYPE_ARRAY = 'array';

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\ManyToOne(targetEntity="\Settings\Option", inversedBy="rules")
     * @var Option
     */
    protected $option;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $validator;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    protected $errorMessage;

    /**
     * @ORM\Column(type="array", nullable=true)
     * @var mixed|null
     */
    protected $argument;

//    /**
//     * @ORM\Column(type="string", nullable=true)
//     * @var string
//     */
//    private $argumentType;

    /** @param mixed|null $argument */
    public function setArgument($argument): void
    {
//        if (\is_array($argument)) {
//            $this->setArgumentType(self::TYPE_ARRAY);
//        } elseif (\is_scalar($argument)) {
//            $this->setArgumentType(self::TYPE_SCALAR);
//        } else {
//            throw new \InvalidArgumentException(sprintf('Argument must be scalar or array. %s given',
//                getType($argument)));
//        }
//        $this->argument = $argument;
        if (!\is_scalar($argument) && !\is_array($argument)) {
            throw new \InvalidArgumentException(sprintf('Argument must be scalar or array. %s given',
                getType($argument)));
        }
        $this->argument = $argument;
    }

//    private function setArgumentType(string $type): void
//    {
//        $this->argumentType = $type;
//    }

}
