<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Locale\Exceptions\LocaleWithCodeAlreadyExists;
use Monolog\Logger;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Utils\ArrayHash;

class LocaleFacade
{

    private const CACHE_NAMESPACE = 'CMS.Locale';

    /** @var EntityManager */
    private $em;

    /** @var Cache */
    private $cache;

    /** @var Logger */
    private $monolog;

    public function __construct(EntityManager $em, IStorage $storage, Logger $monolog)
    {
        $this->em = $em;
        $this->cache = new Cache($storage, self::CACHE_NAMESPACE);
        $this->monolog = $monolog;
    }

    /** @return Locale[] */
    public function getAll(): array
    {
        return $this->cache->load('locales', function (& $dependencies) {
            $dependencies = [Cache::TAGS => ['locales']];
            return $this->em->getRepository(Locale::class)->findAll();
        });
    }

    public function getOneByCode(string $code): ?Locale
    {
        return $this->cache->load('locale_' . $code, function (& $dependencies) use ($code) {
            $dependencies = [Cache::TAGS => ['locale_' . $code]];
            return $this->em->getRepository(Locale::class)->findOneBy(['code' => $code]);
        });
    }

    public function save(Locale $locale): void
    {
        try {
            $this->em->persist($locale);
            $this->em->flush();
            $this->monolog->addDebug(sprintf('Saving locale %s', $locale->getName()));
        } catch (UniqueConstraintViolationException $exception) {
            $this->monolog->addInfo(sprintf('Cannot save locale %s. Locale with this code already exists',
                $locale->getName()));
            throw new LocaleWithCodeAlreadyExists();
        }
    }

    public function fillValues(ArrayHash $values, ?Locale $locale = null): Locale
    {
        if ($locale === null) {
            $locale = new Locale($values->code, $values->name);
        } else {
            $locale->setCode($values->code);
            $locale->setName($values->name);
        }
        $this->setDefault($locale, $values->default);

        return $locale;
    }

    public function setDefault(Locale $locale, bool $default = true): void
    {
        $locale->setDefault($default);
        if (!$default) {
            return;
        }
        /** @var Locale[] $locales */
        $locales = $this->em->getRepository(Locale::class)->findBy(['default' => true]);
        if (!$locales) {
            return;
        }
        foreach ($locales as $otherLocale) {
            $otherLocale->setDefault(false);
            $this->em->persist($otherLocale);
        }
    }

    public function getDefault(): ?Locale
    {
        return $this->em->getRepository(Locale::class)->findOneBy(['default' => true]);
    }

}
