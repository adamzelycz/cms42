<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Dictionaries;

use Nette\Neon\Neon;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class NeonDictionaryParser
{

    /** @return string[] */
    public function decode(string $string): array
    {
        $translations = Neon::decode($string);
        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($translations));
        $fullKeys = [];
        foreach ($iterator as $leafValue) {
            $keys = [];
            foreach (range(0, $iterator->getDepth()) as $depth) {
                $keys[] = $iterator->getSubIterator($depth)->key();
            }
            $fullKeys[ implode('.', $keys) ] = $leafValue;
        }
        return $fullKeys;
    }

    /** @param string[] $array */
    public function encode(array $array): string
    {
        $r = [];
        foreach ($array as $dotted => $value) {
            $keys = explode('.', $dotted);
            $c = &$r[array_shift($keys)];
            foreach ($keys as $key) {
                if (isset($c[$key]) && $c[$key] === true) {
                    $c[$key] = [];
                }
                $c = &$c[$key];
            }
            if ($c === null) {
                $c = $value;
            }
        }
        return Neon::encode($r, Neon::BLOCK);
    }

}
