<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Dictionaries;

use Locale\Exceptions\DictionaryLoadException;
use Locale\Locale;

interface IDictionaryLoader
{

    /**
     * @return string[]
     * @throws DictionaryLoadException
     */
    public function load(Locale $locale, string $resource): array;

}
