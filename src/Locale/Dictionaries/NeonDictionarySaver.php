<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Dictionaries;

use Locale\Exceptions\DictionarySaveException;
use Locale\Locale;
use function file_exists;
use function file_put_contents;
use function is_dir;

class NeonDictionarySaver implements IDictionarySaver
{

    /** @var string */
    private $langsDir;

    /** @var NeonDictionaryParser  */
    private $parser;

    public function __construct(string $langsDir)
    {
        $this->langsDir = $langsDir;
        $this->parser = new NeonDictionaryParser();
    }

    /**
     * @param string[] $values
     * throws DictionarySaveException
     */
    public function save(Locale $locale, string $resource, array $values): void
    {
        $dir = $this->langsDir . '/' . $resource;
        if (!file_exists($dir) || !is_dir($dir)) {
            throw new DictionarySaveException($dir . ' not exists or it is not a directory');
        }
        $file = $dir . '/' . $resource . '.' . $locale->getCode() . '.neon';
        $values = array_filter($values); //filter empty values
        $neon = $this->parser->encode($values);
        file_put_contents($file, $neon);
    }

}
