<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Exceptions;

class LocaleWithCodeAlreadyExists extends \Exception
{

    public function __construct(string $message = '', int $code = 0, ?\Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'Locale with this code already exists. Please use another one';
        }
        parent::__construct($message, $code, $previous);
    }

}
