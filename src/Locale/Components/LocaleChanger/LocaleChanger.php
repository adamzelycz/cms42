<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\LocaleChanger;

use App\Components\BaseControl;
use Locale\Locale;
use Locale\LocaleFacade;

/**
 * Class LocaleChanger
 * @package Locale\Components\LocaleChanger
 */
class LocaleChanger extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/LocaleChanger.latte';

    /** @var Locale[] */
    private $locales;

    public function __construct(LocaleFacade $localepageLocalepageLocalepageFacade)
    {
        parent::__construct();
        $locales = $localepageLocalepageLocalepageFacade->getAll();
        if (\count($locales) <= 1) {
            $locales = [];
        }
        $this->locales = $locales;
    }

    public function render(): void
    {
        if ($this->getPresenter()->getName() !== 'Error:Error') {
            $this->template->locales = $this->locales;
            $this->template->setFile($this->getChangedTemplate() ?? self::TEMPLATE);
            $this->template->render();
        }
    }

}
