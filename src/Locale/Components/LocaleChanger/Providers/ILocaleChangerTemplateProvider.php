<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\LocaleChanger\Providers;

interface ILocaleChangerTemplateProvider
{

    public function getLocaleChangerTemplate(): string;

}
