<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\TranslationsEditor;

use Locale\Locale;

interface ITranslationsEditorFactory
{

    /** @param Locale[] $locales */
    public function create(string $resource, array $locales): TranslationsEditor;

}
