<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\TranslationsEditor;

use App\Components\BaseControl;
use App\Components\TFormFactory;
use Kdyby\Translation\Translator;
use Locale\Dictionaries\IDictionaryLoader;
use Locale\Dictionaries\IDictionarySaver;
use Locale\Exceptions\DictionaryLoadException;
use Locale\Exceptions\DictionarySaveException;
use Locale\Locale;
use Nette\Application\UI\Form;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Forms\Controls\TextInput;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use Security\Authorizator;
use function array_key_exists;
use function array_merge;
use function array_unique;
use function explode;
use function in_array;
use function ksort;
use function preg_match;

/** @method onSave(array $success, array $errors) */
class TranslationsEditor extends BaseControl
{

    use TFormFactory;

    private const TEMPLATE = __DIR__ . '/TranslationsEditor.latte';

    /** @var callable[] */
    public $onSave = [];

    /** @var Locale[] */
    private $locales;

    /** @var string[] */
    private $keys;

    /** @var array|array[] */
    private $translations;

    /** @var IDictionarySaver */
    private $saver;

    /** @var string */
    private $resource;

    /** @var Cache */
    private $cache;

    /** @param Locale[] $locales */
    public function __construct(
        string $resource,
        array $locales,
        IDictionaryLoader $loader,
        IDictionarySaver $saver,
        IStorage $storage
    )
    {
        parent::__construct();
        $this->locales = $locales;

        $allTranslations = [];
        $allKeys = [];
        foreach ($locales as $locale) {
            try {
                /** @var string[] $translations */
                $translations = $loader->load($locale, $resource);
            } catch (DictionaryLoadException $e) {
                $saver->save($locale, $resource, []);
                $translations = [];
            }
            $allTranslations[$locale->getCode()] = $translations;
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $allKeys = array_merge($allKeys, array_keys($translations));
        }
        $allKeys = array_unique($allKeys);

        $this->translations = $allTranslations;
        $this->keys = $allKeys;
        $this->saver = $saver;
        $this->resource = $resource;
        $this->cache = new Cache($storage, Translator::class);
    }

//    private function setLang(string $lang): void
//    {
//        if (!$this->resourcesService->isValidResource($lang)) {
//            throw new \InvalidArgumentException('Lang not found');
//        }
//        $this->resource = $lang;
//    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->keys = $this->keys;
        $this->template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = $this->getAdminForm();

        $form->addHidden('delete')->setHtmlId('translations-delete');
        $form->addHidden('add')->setHtmlId('translations-add');
        foreach ($this->locales as $locale) {
            $form->addGroup($locale->getName() . ' [' . $locale->getCode() . ']');
            if ($form->getCurrentGroup() !== null) {
                $form->getCurrentGroup()->setOption('locale', $locale);
            }

            foreach ($this->keys as $key) {
                $underScoredKey = str_replace('.', '_', $key);
                $textBox = $form->addText($locale->getCode() . '_' . $underScoredKey, $key)
                    ->setAttribute('title', $key);
                $localeTranslations = $this->translations[$locale->getCode()];
                if (array_key_exists($key, $localeTranslations)) {
                    $textBox->setDefaultValue($localeTranslations[$key]);
                }
            }
        }

        $form->setCurrentGroup(null);
        $form->addSubmit('save', 'Save');
        $form->onSuccess[] = [$this, 'formSucceeded'];

        return $form;
    }

    /** @param mixed[] $values */
    public function formSucceeded(Form $form, array $values): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        $groups = $form->getGroups();
        $success = [];
        $errors = [];
        //Delete translations
        $deletion = explode(';', $values['delete']);
        //Add translations
        try {
            $addition = Json::decode($values['add'], Json::FORCE_ARRAY) ?? [];
        } catch (JsonException $_) {
            $addition = [];
        }
        foreach ($groups as $name => $group) {
            $translations = $group->getControls();
            if (!$translations) {
                continue;
            }
            $reversed = array_reverse($translations);
            /** @var TextInput $first */
            $first = array_pop($reversed);
            $code = Strings::before($first->name, '_');
            if (!$code) {
                continue;
            }
            $locale = new Locale($code, $name);

            $newValues = [];
            /** @var TextInput $translation */
            foreach ($translations as $translation) {
                /** @var string $caption */
                $caption = $translation->caption;
                if (!in_array($caption, $deletion, true)) {
                    $newValues[$caption] = $translation->getValue();
                }
            }

            foreach ($addition as $fullKey => $value) {
                if (Strings::startsWith($fullKey, $code . '_')) {
                    $key = Strings::after($fullKey, '_');
                    if (!$key || preg_match('/[^a-zA-Z0-9\-_.]/', $key)) {
                        continue;
                    }
                    $newValues[$key] = $value;
                }
            }

            ksort($newValues);
            try {
                $this->saver->save($locale, $this->resource, $newValues);
                $success[$code] = $locale;
            } catch (DictionarySaveException $exception) {
                $errors[$code] = [$locale, $exception];
            }
        }
        $this->cache->clean([Cache::ALL => true]);
        $this->onSave($success, $errors);
    }

}
