<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\Components\LocaleGrid;

interface ILocaleGridFactory
{

    public function create(): LocaleGrid;

}
