<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Locale\DI;

use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;
use function dirname;

class LocaleExtension extends CompilerExtension implements IEntityProvider, IJsAdminProvider
{

    public function loadConfiguration(): void
    {
        $this->parseConfig($this->getContainerBuilder(), __DIR__ . '/services.neon');
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping(
            $this->getContainerBuilder(), ['Locale' => 'Locale\\*Module\\Presenters\\*Presenter']
        );
    }

    /**
     * Returns associative array of Namespace => mapping definition
     * @return array
     */
    public function getEntityMappings(): array
    {
        return ['Locale' => __DIR__ . '/..'];
    }

    /** @return string[] */
    public function getJsAdminFiles(): array
    {
        return [dirname(__DIR__) . '/assets/js/translations.js'];
    }

}
