<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Download;

use Nette\SmartObject;
use Plugins\AllPluginsContainer;
use Plugins\Api\ApiClient;
use Plugins\Api\Exceptions\ApiException;
use Plugins\Exceptions\PluginDownloadException;
use Plugins\Plugin;
use ZipArchive;

class Downloader
{

    use SmartObject;

    /** @var ApiClient */
    private $client;

    /** @var string */
    private $pluginsDir;

    /** @var Plugin[] */
    private $allPlugins;

    /** @var string */
    private $pluginsTempDir;

    public function __construct(
        string $pluginsDir,
        ApiClient $client,
        AllPluginsContainer $allPluginsContainer,
        ?string $pluginsTempDir = null
    )
    {
        $this->client = $client;
        $this->pluginsDir = $pluginsDir;
        $this->allPlugins = $allPluginsContainer->getAllPlugins();
        $this->pluginsTempDir = $pluginsTempDir ?? __DIR__ . '/temp';
    }

    /**
     * @param Plugin $plugin Plugin must contain ID and Identifier
     * @throws PluginDownloadException
     */
    public function download(Plugin $plugin): void
    {
        if (array_key_exists($plugin->getIdentifier(), $this->allPlugins)) {
            throw new PluginDownloadException('This plugin is already downloaded');
        }

        $zipFile = $this->pluginsTempDir . '/' . $plugin->getId() . '.zip';
        try {
            $this->client->get('plugins/download/' . $plugin->getId(), ['sink' => $zipFile]);
        } catch (ApiException $exception) {
            throw new PluginDownloadException($exception->getMessage(), 0, $exception);
        }

        $zip = new ZipArchive();
        $resource = $zip->open($zipFile);
        if ($resource === true) {
            $pluginDir = $this->pluginsDir . '/' . $plugin->getVendor() . '/' . $plugin->getPackage();
            $zip->extractTo($pluginDir);
            $zip->close();
        }
        unlink($zipFile);
    }

}
