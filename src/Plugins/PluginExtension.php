<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins;

use App\Extensions\CompilerExtension;

abstract class PluginExtension extends CompilerExtension implements IPlugin
{

}
