<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Install;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Kdyby\Doctrine\EntityManager;
use Nette\DI\Container;
use Nette\SmartObject;
use Nette\Utils\Json;
use Plugins\AllPluginsContainer;
use Plugins\Exceptions\PluginInstallException;
use Plugins\Exceptions\PluginIsNotInstalled;
use Plugins\Plugin;

class Installer
{

    use SmartObject;

    public const FILE = __DIR__ . '/plugins_installed.json';

    /** @var Container */
    private $container;

    /** @var EntityManager */
    private $em;

    /** @var Plugin[] */
    private $allPlugins;

    /** @var string[] */
    private $installClasses;

    public function __construct(Container $container, EntityManager $em, AllPluginsContainer $allPluginsContainer)
    {
        $this->container = $container;
        $this->em = $em;
        $this->allPlugins = $allPluginsContainer->getAllPlugins();
        $this->installClasses = $allPluginsContainer->getInstallClasses();
    }

    public function install(string $identifier): void
    {
        $installedPlugins = $this->getInstalledPluginNames();

        /** @var IInstallPlugin|null $install */
        $install = array_key_exists($identifier, $this->installClasses) ?
            $this->container->getByType($this->installClasses[$identifier]) : null;

        if (!array_key_exists($identifier, $this->allPlugins)) {
            throw new PluginInstallException('This plugin is not available, try remove and download again');
        }
        $plugin = $this->em->getRepository(Plugin::class)->findOneBy(['identifier eq' => $identifier]);
        if ($plugin !== null) {
            throw new PluginInstallException('This plugin is already installed');
        }
        $plugin = $this->allPlugins[$identifier];
        if ($install !== null) {
            $install->install();
        }
        $pluginFromDatabase = $this->em->getRepository(Plugin::class)->findOneBy(['identifier eq' => $identifier]);

        if ($pluginFromDatabase === null) {
            try {
                $this->em->persist($plugin);
                $this->em->flush();
            } catch (UniqueConstraintViolationException $e) {
                throw new PluginInstallException('Plugin with this identifier is already installed');
            }
        }
        $installedPlugins[$plugin->getIdentifier()] = $plugin->getVersion();

        $this->save($installedPlugins);
    }

    public function uninstall(string $identifier): void
    {
        $installedPlugins = $this->getInstalledPluginNames();

        /** @var IInstallPlugin|null $install */
        $install = array_key_exists($identifier, $this->installClasses) ?
            $this->container->getByType($this->installClasses[$identifier]) : null;

        $plugin = $this->em->getRepository(Plugin::class)->findOneBy(['identifier eq' => $identifier]);
        if ($plugin === null) {
            throw new PluginIsNotInstalled('Plugin with this identifier is not installed');
        }

        if ($install !== null) {
            $install->uninstall();
        }

        $this->em->remove($plugin);
        $this->em->flush();
        unset($installedPlugins[$plugin->getIdentifier()]);

        $this->save($installedPlugins);
    }

    /** @param array|string[] $installedPlugins */
    private function save(array $installedPlugins): void
    {
        \file_put_contents('nette.safe://' . self::FILE, Json::encode($installedPlugins, Json::PRETTY));
    }

    /**
     * @return string[]
     * @throws \Nette\Utils\JsonException
     */
    private function getInstalledPluginNames(): array
    {
        $installedPlugins = file_get_contents('nette.safe://' . self::FILE);
        return (array)Json::decode($installedPlugins);
    }

}
