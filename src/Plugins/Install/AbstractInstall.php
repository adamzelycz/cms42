<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Install;

use Plugins\Plugin;

abstract class AbstractInstall implements IInstallPlugin
{

    /** @var Plugin */
    protected $plugin;

    /** @var InstallHelper */
    protected $install;

    public function __construct(Plugin $plugin, InstallHelper $install)
    {
        $this->install = $install;
        $this->plugin = $plugin;
    }

}
