<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Install;

use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Settings\Option;

class InstallHelper
{

    /** @var EntityManager */
    private $em;

    /** @var Logger */
    private $monolog;

    public function __construct(EntityManager $em, Logger $monolog)
    {
        $this->em = $em;
        $this->monolog = $monolog;
    }

    /**
     * @param Option[] $options
     * @param bool $uninstall
     * @throws \Exception
     */
    public function options(array $options, bool $uninstall = false): void
    {
        if (!$uninstall) {
            foreach ($options as $option) {
                $this->monolog->addDebug('Installing option', [$option]);
                $this->em->persist($option);
            }
        } else {
            /** @var Option $option */
            foreach ($options as $option) {
                $plugin = $option->getPlugin();
                if ($plugin !== null) {
                    $option = $this->em->getRepository(Option::class)->findOneBy(
                        ['key' => $option->getKey(), 'plugin.identifier' => $plugin->getIdentifier()]
                    );
                    if ($option) {
                        $this->monolog->addDebug('Uninstalling option', [$option]);
                        $this->em->remove($option);
                    }
                }
            }
        }
        $this->em->flush();
    }

}
