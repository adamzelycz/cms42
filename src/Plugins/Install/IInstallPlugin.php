<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Install;

interface IInstallPlugin
{

    public function install(): void;

    public function uninstall(): void;

}
