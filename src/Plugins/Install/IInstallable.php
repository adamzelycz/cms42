<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\Install;

/** Interface IInstallable provides way to pass custom install class of plugin */
interface IInstallable
{

    /** @return string Full className (Not path!) */
    public function getInstallClass(): string;

}
