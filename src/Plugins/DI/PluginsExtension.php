<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\DI;

use App\Extensions\CompilerExtension;
use Kdyby\Doctrine\DI\IEntityProvider;
use Plugins\Api\ApiClient;

class PluginsExtension extends CompilerExtension implements IEntityProvider
{

    /** @var array|mixed[] */
    private $defaults = [
        'clientId' => null,
        'secret' => null,
    ];

    public function loadConfiguration(): void
    {
        $cb = $this->getContainerBuilder();
        $this->parseConfig($cb, __DIR__ . '/services.neon');

        $config = $this->getConfig($this->defaults);
        $cb->addDefinition($this->prefix('apiClient'))
            ->setType(ApiClient::class)
            ->setArguments([
                $config['clientId'],
                $config['secret'],
            ]);
    }

    public function beforeCompile(): void
    {
        $this->setPresenterMapping(
            $this->getContainerBuilder(), ['Plugins' => 'Plugins\\*Module\\Presenters\\*Presenter']
        );
    }

    public function getEntityMappings(): array
    {
        return ['Plugins' => __DIR__ . '/..'];
    }

}
