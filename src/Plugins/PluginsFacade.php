<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins;

use App\Helpers\RestartService;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;
use Plugins\Download\Downloader;
use Plugins\Exceptions\PluginRemoveException;
use Plugins\Install\Installer;
use Plugins\Remove\Remover;

class PluginsFacade
{

    use SmartObject;

    /** @var Installer */
    private $installer;

    /** @var Remover */
    private $remover;

    /** @var Downloader */
    private $downloader;

    /** @var EntityManager */
    private $em;

    /** @var Plugin[] */
    private $allPlugins;

    /** @var RestartService */
    private $restartService;

    public function __construct(
        EntityManager $em,
        AllPluginsContainer $allPluginsContainer,
        Installer $installer,
        Remover $remover,
        Downloader $downloader,
        RestartService $restartService
    )
    {
        $this->em = $em;
        $this->restartService = $restartService;
        $this->allPlugins = $allPluginsContainer->getAllPlugins();
        $this->installer = $installer;
        $this->remover = $remover;
        $this->downloader = $downloader;
    }

    /** @return Plugin[] */
    public function getAvailable(): array
    {
        return array_udiff($this->allPlugins, $this->getInstalled(), function ($plugin1, $plugin2) {
            /** @var Plugin $plugin1 */
            /** @var Plugin $plugin2 */
            if ($plugin1->getIdentifier() === $plugin2->getIdentifier()) {
                return 0;
            }
            return $plugin1->getIdentifier() > $plugin2->getIdentifier() ? 1 : -1;
        });
    }

    /** @return Plugin[] */
    public function getInstalled(): array
    {
        return $this->em->getRepository(Plugin::class)->findAll();
    }

    public function install(string $identifier, bool $uninstall = false): void
    {
        $uninstall ? $this->installer->uninstall($identifier) : $this->installer->install($identifier);
        $this->restartService->cleanCache();
    }

    public function download(Plugin $plugin): void
    {
        $this->downloader->download($plugin);
        $this->restartService->refreshClassmap();
        $this->restartService->cleanCache();
    }

    public function remove(string $identifier, bool $quiet = false): void
    {
        try {
            $this->remover->remove($identifier, $quiet);
        } catch (PluginRemoveException $exception) {
            throw $exception;
        } finally {
            $this->restartService->refreshClassmap();
            $this->restartService->cleanCache();
        }
    }

}
