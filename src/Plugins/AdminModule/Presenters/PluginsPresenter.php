<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use App\Components\Flashes\Flashes;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Plugins\Exceptions\PluginInstallException;
use Plugins\Exceptions\PluginRemoveException;
use Plugins\PluginsFacade;
use Security\Authorizator;
use Settings\Components\SettingsForm\ISettingsFormFactory;
use Settings\Components\SettingsForm\SettingsForm;
use Settings\Option;
use Settings\SettingsQuery;

class PluginsPresenter extends BasePresenter
{

    /** @var EntityManager */
    private $em;

    /** @var PluginsFacade */
    private $pluginsFacade;

    public function __construct(EntityManager $em, PluginsFacade $pluginsFacade)
    {
        parent::__construct();
        $this->em = $em;
        $this->pluginsFacade = $pluginsFacade;
    }

    public function actionDefault(): void
    {
        $this->setTitle('Plugins – Overview');
        $this->addHint('Downloaded plugins',
            'On this page you can all your downloaded plugins from Plugins Center. This plugins can be activated or
            deactivated. If you remove any plugin, you can easily download it again.');
    }

    public function renderDefault(): void
    {
        $this->template->availablePlugins = $this->pluginsFacade->getAvailable();
        $this->template->installedPlugins = $this->pluginsFacade->getInstalled();
    }

    /** @secured */
    public function handleInstall(string $identifier, bool $uninstall = false): void
    {
        $this->checkPermissions(Authorizator::UPDATE);
        try {
            $this->pluginsFacade->install($identifier, $uninstall);
            $this->flashMessage(
                sprintf('Plugin %s was successfully %s', $identifier, $uninstall ? 'uninstalled' : 'activated'),
                Flashes::SUCCESS
            );
        } catch (PluginInstallException $exception) {
            $this->monolog->addError($exception->getMessage(), [$identifier]);
            $this->flashMessage($exception->getMessage(), Flashes::ERROR);
        }
        $this->fullRedirect('this');
    }

    /** @secured */
    public function handleRemove(string $identifier): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        try {
            $this->pluginsFacade->remove($identifier);
            $this->flashMessage('Plugin was successfully removed', Flashes::SUCCESS);
            $this->monolog->addDebug('Plugin %s was successfully removed', [$identifier]);
            $this->fullRedirect('this');
        } catch (PluginRemoveException $exception) {
            $this->flashMessage($exception->getMessage(), Flashes::ERROR);
            $this->monolog->addError($exception->getMessage(), [$identifier]);
            $this->redirect('this');
        }
    }

    public function actionSettings(): void
    {
        $this->setTitle('Plugins - Settings');
        $this->addHint('Plugins Settings',
            'There you can control all options of plugins. If you want to set up CMS, please go to the classic Settings');
    }

    protected function createComponentSettingsForm(ISettingsFormFactory $factory): SettingsForm
    {
        /** @var \Kdyby\Doctrine\EntityRepository $repository */
        $repository = $this->em->getRepository(Option::class);
        /** @var ResultSet $resultSet */
        $resultSet = $repository->fetch((new SettingsQuery())->isPlugin());
        $control = $factory->create($resultSet->toArray());
        $control->onSuccess[] = function (SettingsForm $control): void {
            $this->monolog->addInfo('Plugin settings were changed');
            $this->flashMessage('Settings were successfully saved', Flashes::SUCCESS);
            $this->postGet('this');
            $this->sendPayload();
        };
        return $control;
    }

}
