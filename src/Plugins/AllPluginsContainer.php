<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Plugins;

use Nette\SmartObject;

class AllPluginsContainer
{

    use SmartObject;

    /** @var Plugin[] */
    private $allPlugins = [];

    /** @var string[] */
    private $installClasses = [];

    /** @param array|null|Plugin[] $allPlugins */
    public function setAllPlugins(?array $allPlugins): void
    {
        $this->allPlugins = $allPlugins ?? [];
    }

    /** @param array|null|string[] $installClasses */
    public function setInstallClasses(?array $installClasses): void
    {
        $this->installClasses = $installClasses ?? [];
    }

    /** @return Plugin[] */
    public function getAllPlugins(): array
    {
        return $this->allPlugins;
    }

    /** @return string[] */
    public function getInstallClasses(): array
    {
        return $this->installClasses;
    }

}
