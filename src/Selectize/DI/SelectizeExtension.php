<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Selectize\DI;

use App\Components\Css\Providers\ICssAdminProvider;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use Nette\PhpGenerator as Code;
use Selectize\Selectize;
use function array_key_exists;

class SelectizeExtension extends CompilerExtension implements ICssAdminProvider, IJsAdminProvider
{

    /** @var array|mixed[] */
    private $defaults = [
        'mode' => 'full',
        'create' => true,
        'maxItems' => null,
        'delimiter' => '#/',
        'plugins' => ['remove_button'],
        'valueField' => 'id',
        'labelField' => 'name',
        'searchField' => 'name',
    ];

    public function loadConfiguration(): void
    {
        $this->config = $this->getConfig($this->defaults);
    }

    public function getCssAdminFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/css';
        return [
            $dir . '/selectize.bootstrap3.css',
        ];
    }

    public function getJsAdminFiles(): array
    {
        $dir = \dirname(__DIR__) . '/assets/js';
        return [
            $dir . '/selectize.standalone.js',
            $dir . '/selectize.js',
            array_key_exists('aidam/nittro', $this->compiler->getExtensions())
                ? $dir . '/selectize.nittro.init.js' : $dir . '/selectize.init.js',
        ];
    }

    public function afterCompile(Code\ClassType $class): void
    {
        parent::afterCompile($class);

        $init = $class->methods['initialize'];
        $config = $this->getConfig($this->defaults);
        $init->addBody(Selectize::class . '::register(?, ?);', ['addSelectize', $config]);
    }

}
