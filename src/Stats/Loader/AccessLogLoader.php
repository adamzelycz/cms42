<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Loader;

use DateTimeImmutable;
use DateTimeZone;
use Nette\Utils\FileSystem;
use Stats\Stat;
use function file_exists;
use function file_put_contents;
use function mb_substr;
use function preg_split;

class AccessLogLoader implements IStatsLoader
{

    private const PARTS = [
        'dateTime',
        'httpMethod',
        'urlPath',
        'protocol',
        'httpCode',
        'referer',
        'userAgent',
        'ipAddress',
    ];

    /** @var string */
    private $fileName;

    /** @var string[] */
    private $separators;

    /** @var string[] */
    private $parts;

    /** @var string */
    private $newFileName;

    /** @var string */
    private $tempFileName;

    public function __construct(string $fileName, string $format = '%_% - %_% [%dateTime%] "%httpMethod% %urlPath% ' .
    '%protocol%" %httpCode% %_% "%referer%" "%userAgent%" "%ipAddress%"')
    {
        $this->fileName = realpath($fileName) ?: $fileName; //could be false
        $this->separators = preg_split('/\%([a-zA-Z0-9\_]+)\%/', $format) ?: [];
        array_shift($this->separators);
        preg_match_all('/\%([a-zA-Z0-9\_]+)\%/', $format, $parts);
        $this->parts = $parts[1];
        $this->newFileName = __DIR__ . '/loaded_logs/access_' . \time() . '.log';
        $this->tempFileName = __DIR__ . '/loaded_logs/temp/access_' . \time() . '.log';
    }

    /** @return Stat[] */
    public function load(): array
    {
        if (!file_exists($this->fileName)) {
            return [];
        }
        FileSystem::copy($this->fileName, $this->tempFileName);
        $file = new \SplFileObject($this->tempFileName);
        $stats = [];
        while ($file->valid()) {
            $line = $file->fgets();
            if ($line) { //last line is empty
                $stats[] = $this->parse($line);
            }
        }
        $file = null;
        return $stats;
    }

    public function clean(): void
    {
        file_put_contents($this->fileName, '');
        if (file_exists($this->tempFileName)) {
            FileSystem::rename($this->tempFileName, $this->newFileName);
        }
    }

    private function parse(string $line): Stat
    {
        $parts = [];
        foreach ($this->separators as $key => $separator) {
            $pos = mb_strpos($line, $separator);
            if (\in_array($this->parts[$key], self::PARTS, true)) { //only allowed part
                $parts[$this->parts[$key]] = mb_substr($line, 0, $pos === false ? null : $pos); //could be false
            }
            $line = mb_substr($line, $pos + mb_strlen($separator));
        }
        $stat = new Stat();
        $dateTimeUtc = new DateTimeImmutable($parts['dateTime'], new DateTimeZone('UTC'));
//        $dateTimeCurrent = $dateTimeUtc->setTimezone(new DateTimeZone('Europe/Prague'));
        $stat->setDateTime($dateTimeUtc);
        $stat->setUrlPath($parts['urlPath']);
        $stat->setHttpMethod($parts['httpMethod']);
        $stat->setProtocol($parts['protocol']);
        $stat->setHttpCode((int)$parts['httpCode']);
        $stat->setReferer($parts['referer']);
        $stat->setUserAgent($parts['userAgent']);
        $stat->setIpAddress($parts['ipAddress']);
        return $stat;
    }

}
