<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Loader;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="load_stamps")
 * @method DateTimeImmutable getDateTime()
 * @method int getCount()
 * @method setDateTime(DateTimeImmutable $dateTime)
 * @method setCount(int $count)
 */
class LoadStamp
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var DateTimeImmutable
     */
    protected $dateTime;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $count;

    public function __construct(int $count, ?DateTimeImmutable $dateTime = null)
    {
        $this->dateTime = $dateTime ?? new \DateTimeImmutable();
        $this->count = $count;
    }

}
