<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\AdminModule\Presenters;

use App\AdminModule\Presenters\BasePresenter;
use Stats\Components\LastRefresh\ILastRefreshFactory;
use Stats\Components\LastRefresh\LastRefresh;
use Stats\Components\StatsGrid\IStatsGridFactory;
use Stats\Components\StatsGrid\StatsGrid;
use Stats\Components\TotalStats\ITotalStatsFactory;
use Stats\Components\TotalStats\TotalStats;

class StatsPresenter extends BasePresenter
{

    public function actionAll(): void
    {
        $this->setTitle('Statistics – All logs');
    }

    public function actionSummary(): void
    {
        $this->setTitle('Statistics – Summary');
    }

    protected function createComponentStatsGrid(IStatsGridFactory $factory): StatsGrid
    {
        return $factory->create();
    }

    protected function createComponentLastRefresh(ILastRefreshFactory $factory): LastRefresh
    {
        return $factory->create();
    }

    protected function createComponentTotalStats(ITotalStatsFactory $factory): TotalStats
    {
        return $factory->create([
            'httpCode' => 'HTTP Code',
            'ipAddress' => 'IP Address',
            'protocol' => 'Protocol',
            'httpMethod' => 'HTTP Method',
            'urlPath' => 'URL Path',
            'userAgent' => 'User Agent',
            'referer' => 'Referer',
        ]);
    }

}
