<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 * @ORM\Table(name="stats")
 *
 * GETTERS
 * @method DateTimeImmutable getDateTime()
 * @method string getIpAddress()
 * @method string getHttpMethod()
 * @method string getProtocol()
 * @method string getUrlPath()
 * @method string getHttpCode()
 * @method string getUserAgent()
 * @method string getReferer()
 *
 * SETTERS
 * @method setDateTime(DateTimeImmutable $dateTime)
 * @method setIpAddress(string $ipAddress)
 * @method setHttpMethod(string $httpMethod)
 * @method setProtocol(string $protocol)
 * @method setUrlPath(string $url)
 * @method setHttpCode(int $httpCode)
 * @method setUserAgent(string $userAgent)
 * @method setReferer(string $referer)
 */
class Stat
{

    use Identifier;
    use MagicAccessors;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     * @var DateTimeImmutable
     */
    protected $dateTime;

    /**
     * @ORM\Column(type="string", length=50)
     * @var string
     */
    protected $ipAddress;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $protocol;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $httpMethod;

    /**
     * @ORM\Column(type="string", length=1023)
     * @var string
     */
    protected $urlPath;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    protected $httpCode;

    /**
     * @ORM\Column(type="string", length=1023)
     * @var string
     */
    protected $userAgent;

    /**
     * @ORM\Column(type="string", length=1023)
     * @var string
     */
    protected $referer;

}
