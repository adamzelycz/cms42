<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\PDOStatement;
use Monolog\Logger;
use Nette\SmartObject;
use Stats\Exceptions\LoadException;
use Stats\Loader\IStatsLoader;
use Stats\Loader\LoadStamp;
use function get_class;

class StatsFacade
{

    use SmartObject;

    public const
        COUNT_BY_DAY = 'day',
        COUNT_BY_WEEK = 'week',
        COUNT_BY_MONTH = 'month',
        COUNT_BY_YEAR = 'year';

    /** @var IStatsLoader */
    private $loader;

    /** @var Logger */
    private $monolog;

    /** @var EntityManager */
    private $em;

    public function __construct(IStatsLoader $loader, Logger $monolog, EntityManager $em)
    {
        $this->loader = $loader;
        $this->monolog = $monolog;
        $this->em = $em;
    }

    public function load(bool $clean = true): void
    {
        try {
            $stats = $this->loader->load();
            $this->em->transactional(function () use ($stats): void {
                $this->em->persist($stats);
                $stamp = new LoadStamp(\count($stats));
                $this->em->persist($stamp);
                $this->em->flush();
            });
            if ($clean) {
                $this->loader->clean();
            }
        } catch (\Throwable $exception) {
            $this->monolog->addError(sprintf('Error occurred during loading data using %s. Error: %s',
                get_class($this->loader), $exception->getMessage()));
            throw new LoadException($exception->getMessage(), 0, $exception);
        }
        $this->monolog->addDebug('Successfully loaded data using ' . get_class($this->loader));
    }

    public function countTotal(?string $dateModify = null, bool $unique = false): int
    {
        $query = $this->em->getRepository(Stat::class)->createQueryBuilder('s', 's.id')
            ->select($unique ? 'COUNT(DISTINCT(s.ipAddress))' : 'COUNT(s)');
        if ($dateModify !== null) {
            $date = (new \DateTimeImmutable())->modify($dateModify);
            $query->andWhere('s.dateTime > :date')->setParameter('date', $date);
        }
        return (int)$query->getQuery()->getSingleScalarResult();
    }

    public function getAvg(?int $limit = null, bool $unique = false): float
    {
        $query = $this->em->getConnection()->prepare('SELECT AVG(sclr_1) FROM (' . $this->countBySql(self::COUNT_BY_DAY, $limit, $unique) . ') sub');
        $query->execute();
        return (float)$query->fetchColumn();
    }

    /** @return array|array[] */
    public function countBy(string $countBy = self::COUNT_BY_DAY, ?int $limit = null, bool $unique = false): array
    {
        $sql = $this->countBySql($countBy, $limit, $unique);
        return $this->execute($sql)->fetchAll();
    }

    /** @param mixed[]|null $params */
    private function execute(string $sql, ?array $params = null): PDOStatement
    {
        $query = $this->em->getConnection()->prepare($sql);
        $query->execute($params);
        return $query;
    }

    /** @return mixed (SQL string) */
    private function countBySql (string $by, ?int $limit = null, bool $unique = false)
    {
        $query = '
SELECT date_trunc(\'' . $by . '\', s.dateTime) as by_day,
' . ($unique ? 'COUNT(DISTINCT(s.ipAddress))' : 'COUNT(s)') . '
FROM ' . Stat::class . ' s
GROUP BY by_day
ORDER BY by_day DESC
';
        $query = $this->em->getRepository(Stat::class)->createQuery($query);
        if ($limit !== null) {
            $query->setMaxResults($limit);
        }
        return $query->getSQL();
    }

}
