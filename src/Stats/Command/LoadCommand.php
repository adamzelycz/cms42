<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Command;

use Kdyby\Doctrine\EntityManager;
use Monolog\Logger;
use Stats\Exceptions\LoadException;
use Stats\StatsFacade;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LoadCommand extends Command
{

    /** @var EntityManager @inject */
    public $em;

    /** @var Logger @inject */
    public $monolog;

    /** @var StatsFacade @inject */
    public $statsFacade;

    protected function configure(): void
    {
        $this
            ->setName('stats:load')
            ->setDescription('Loads data from Nginx access.log into the database.')
            ->addOption('no-clear', null, InputOption::VALUE_NONE, 'Disable clear of the original access.log')
            ->setHelp(<<<EOT
The <info>stats:load</info> command data from Nginx access.log into the database.

  <info>bin/console stats:load</info>

If you do not want to clear data from original access.log after load, you can use the <info>--no-clear</info> option:

  <info>bin/console stats:load --no-clear</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->statsFacade->load(!$input->getOption('no-clear'));
        } catch (LoadException $exception) {
            $output->writeln($exception->getMessage());
            return 1;//error
        }
        $this->monolog->addDebug('Successfully executed Load command');
        return 0;//no error
    }

}
