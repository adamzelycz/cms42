<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Components\TotalStats;

interface ITotalStatsFactory
{

    /** @param string[] $groupableColumns */
    public function create(array $groupableColumns): TotalStats;

}
