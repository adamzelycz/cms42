<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Stats\Components\StatsGrid;

use App\Components\BaseControl;
use App\Components\Datagrid\Datagrid;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\ResultSet;
use Nette\Forms\Container;
use Nette\Utils\Paginator;
use Nette\Utils\Validators;
use Security\Authorizator;
use Stats\Stat;
use function array_key_exists;

class StatsGrid extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/StatsGrid.latte';

    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    protected function createComponentGrid(): Datagrid
    {
        $grid = $this->getDatagrid();
        $grid->addColumn('dateTime', 'Date-Time')->enableSort();
        $grid->addColumn('ipAddress', 'IP Address')->enableSort();
        $grid->addColumn('protocol', 'Protocol')->enableSort();
        $grid->addColumn('httpMethod', 'HTTP Method')->enableSort();
        $grid->addColumn('urlPath', 'URL Path')->enableSort();
        $grid->addColumn('httpCode', 'HTTP Code')->enableSort();
        $grid->addColumn('userAgent', 'User Agent')->enableSort();
        $grid->addColumn('referer', 'Referer')->enableSort();
        $grid->setRowPrimaryKey('id');

        $grid->setDeleteCallback([$this, 'deleteStat']);
        $grid->setDataSourceCallback([$this, 'getData']);

        $grid->addCellsTemplate(__DIR__ . '/templates/@cells.datagrid.latte');
        $grid->setFilterFormFactory(function () {
            $form = new Container();
//            $form->addText('dateTime');
            $form->addText('ipAddress');
            $form->addText('protocol');
            $form->addText('httpMethod');
            $form->addText('urlPath');
            $form->addInteger('httpCode');
            $form->addText('userAgent');
            $form->addText('referer');
            $form->addSubmit('filter', 'Filter data');
//                ->getControlPrototype()->class = 'd-none btn btn-secondary';
            $form->addSubmit('cancel', 'Cancel filter');
//                ->getControlPrototype()->class = 'btn';

            return $form;
        });

        $grid->addGlobalAction('delete', 'Delete', function (array $ids, Datagrid $grid): void {
            $this->checkPermissions(Authorizator::DELETE);
            $stats = $this->em->getRepository(Stat::class)->findBy(['id' => $ids]);
//            $this->cleanCache($stats);
            $this->em->remove($stats);
            $this->em->flush();
            $grid->redrawControl('rows');
        });

        $grid->setPagination(10, [$this, 'getDataCount']);

        return $grid;
    }

    /**
     * @param array|mixed[] $filter
     * @param array|mixed[] $order
     * @return Stat[]
     */
    public function getData(?array $filter, ?array $order, ?Paginator $paginator = null): ?array
    {
        $orderBy = [];
        if (\is_array($order) && \count($order) === 2) {
            $orderBy[$order[0]] = $order[1];
        }
        if (!array_key_exists('dateTime', $orderBy)) {
            $orderBy['dateTime'] = 'DESC';
        }
        $qb = $this->em->getRepository(Stat::class)->createQueryBuilder('s');
        if ($filter) {
            foreach ($filter as $column => $value) {
                if (Validators::isNumeric($value)) {
                    $qb->andWhere('s.' . $column . ' = :' . $column)->setParameter($column, $value);
                } else {
                    $qb->andWhere('LOWER(s.' . $column . ') LIKE :' . $column)
                        ->setParameter($column, '%' . mb_strtolower($value) . '%');
                }
            }
        }
        foreach ($orderBy as $key => $value) {
            $qb->addOrderBy('s.' .$key, $value);
        }
        $resultSet = new ResultSet($qb->getQuery());
        if ($paginator) {
            $resultSet->applyPaginator($paginator);
        }
        return $resultSet->toArray();
    }

    public function getDataCount(): int
    {
        return $this->em->getRepository(Stat::class)->count([]);
    }

    public function deleteStat(string $id): void
    {
        $this->checkPermissions(Authorizator::DELETE);
        /** @var Stat $stat */
        $stat = $this->em->getRepository(Stat::class)->find($id);
        if ($stat !== null) {
//            $this->cleanCache([$stat]);
            $this->em->remove($stat);
            $this->em->flush();
        }
        $this->postGet('this');
    }

}
