<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace MenuEditor\DI;

use App\Components\Css\Providers\ICssAdminProvider;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Extensions\CompilerExtension;
use MenuEditor\MenuEditorControl;
use Nette\PhpGenerator\ClassType;

class MenuEditorExtension extends CompilerExtension implements IJsAdminProvider, ICssAdminProvider
{

    /** @var array|mixed[] */
    private $defaults = [
        'iconPicker' => [],
        'listOptions' => [
            'placeholderCss' => [
                'background-color' => 'gray',
            ],
        ],
        'labelEdit' => 'Edit',
    ];

//    public function loadConfiguration(): void
//    {
//        $cb = $this->getContainerBuilder();
//        $config = $this->validateConfig($this->defaults);
//    }

    public function afterCompile(ClassType $class): void
    {
        $config = $this->getConfig($this->defaults);
        $init = $class->methods['initialize'];
        $init->addBody(MenuEditorControl::class . '::register(?, ?);', [$config, 'addMenuEditor']);
    }

    /** @return string[] */
    public function getJsAdminFiles(): array
    {
        return [
//            \dirname(__DIR__) . '/assets/js/bootstrap-iconpicker-font.js',
            \dirname(__DIR__) . '/assets/js/bootstrap-iconpicker.js',
            \dirname(__DIR__) . '/assets/js/jquery-menu-editor.min.js',
            \dirname(__DIR__) . '/assets/js/menu-editor.nittro.js',
        ];
    }

//<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-iconpicker/1.9.0/css/bootstrap-iconpicker.min.css">

    /** @return string[] */
    public function getCssAdminFiles(): array
    {
        return [

        ];
    }

}
