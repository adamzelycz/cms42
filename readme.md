CMS42 [EN] [CS](cs.md)
=========================
![alt text](www/static/cms_logo.png "CMS42 Logo")

----> [LIVE DEMO](https://cms42.adamzelycz.cz "CMS42 - Adam Zelycz") <---- 
--------
**To enter the administration use following login credentials: username: _user_, password: _user_.
For obvious reasons, you can not do all operations and enter some part of administration with this account.
If you want administration right, feel free to [contact me](mailto:contact@adamzelycz.cz)**

About project
--------
This CMS (Content Management System) is developed just for fun and it is possible example of my work.
Entire application runs on PHP 7.2 and [Nette framework](https://nette.org/cs/ "Nette Framework")

CMS is not complete and probably will never be, I work on it only occasionally. 
It provides a variety of features. On the publicly accessible site you can find only contact and login forms
but the most features are hidden in the administration. This includes general settings but also advanced work
with permissions

You can also find datagrids useful for managing user accounts, internal and external URL address or languages
and translations. For editing pages there is advanced WYSIWYG editor that includes ability to include dynamic
renderable components.

CMS is based on plugins and it is possible to extend system this way.
It ensures my external app (something like Plugins Store) that communicates with client side app via REST Api and
provides listing of plugins catalog and downloading them.

For Front-end I used Bootstrap 4. Design can be edited by downloading templates or with custom CSS and JS.

An unavoidable part is also fully Ajax browsing. For this purpose, I used
[Nittro framework](https://www.nittro.org/ "Nittro Framework"). It can be of course disabled
in the administration.

Used technologies
--------
- Doctrine 2 (Kdyby/Doctrine)
- Monolog
- Guzzle + OAuth2
- Doctrine migrations, fixtures
- PHPStan
- Mockery
- Nette Tester
- Codeception
- Slevomat/CodingStandards
- Gitlab CI
- Gulp
- PhpStorm
- Docker
- PostgreSQL
- Blackfire 
- Nginx
- ....

About me
--------
My name is Adam Zelycz. My biggest hobby, right after walking
[my dog](https://www.adamzelycz.cz/images/aileenka.jpg "AIleenka") is programming.

I started programming at [high school](http://www.vda.cz/ "VDA.cz") in PHP and C# and now I am fully focused
on programming web applications using [Nette framework](https://nette.org/cs/ "Nette Framework").
I am currently looking for a part-time job while studying [FIT VUT](http://www.fit.vutbr.cz/ "FIT VUT").

I try to write clean code and still improve myself.

More info on my Website: [https://www.adamzelycz.cz](https://www.adamzelycz.cz "Adam Zelycz")

Contact: [contact@adamzelycz.cz](mailto:contact@adamzelycz.cz) or [contact form](https://www.adamzelycz.cz#contact)

Requirements
--------
- PHP >= 7.2
- PostgreSQL (9.6 recommended)
- Redis
- Nginx
- _!!!! Or just use Docker !!! :)_

Installation
--------
**First make sure that you meet the requirements**
1. Clone this repository - `git clone https://gitlab.com/adamzelycz/cms42.git`
2. Find configuration file in `app/config/config.local.template.neon` and rename it to
`app/config/config.local.neon`. After that Fill this file with correct data (like database connection).
If you use Docker, also fill file `docker/production.yml`.
3. If you use docker, run containers using
`docker-compose -f docker-compose.yml -f docker/production.yml up` or simply `make start-prod`.
4. Run `make install-prod` or `make install-prod-docker`.
5. It is recommended to also edit configuration file `app/config/config.neon` to your needs.

Now you should be ready. If some problems occurred, feel free to [contact me](mailto:contact@adamzelycz.cz).
