<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180805111114 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE menu_item ADD url_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE menu_item ADD title VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE menu_item ADD external_url VARCHAR(255) DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN menu_item.external_url IS \'External url in case of internal url is null\'');
        $this->addSql('ALTER TABLE menu_item ADD CONSTRAINT FK_D754D55081CFDAE7 FOREIGN KEY (url_id) REFERENCES urls (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D754D55081CFDAE7 ON menu_item (url_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE menu_item DROP CONSTRAINT FK_D754D55081CFDAE7');
        $this->addSql('DROP INDEX IDX_D754D55081CFDAE7');
        $this->addSql('ALTER TABLE menu_item DROP url_id');
        $this->addSql('ALTER TABLE menu_item DROP title');
        $this->addSql('ALTER TABLE menu_item DROP external_url');
    }
}
