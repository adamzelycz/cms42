<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180703145055 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE rule_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE option_rules_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE option_rules (id INT NOT NULL, option_id INT DEFAULT NULL, validator VARCHAR(255) NOT NULL, message VARCHAR(255) DEFAULT NULL, argument TEXT DEFAULT NULL, argument_type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B0712454A7C41D6F ON option_rules (option_id)');
        $this->addSql('COMMENT ON COLUMN option_rules.argument IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE option_rules ADD CONSTRAINT FK_B0712454A7C41D6F FOREIGN KEY (option_id) REFERENCES settings (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE rule');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE option_rules_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE rule_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE rule (id INT NOT NULL, option_id INT DEFAULT NULL, validator VARCHAR(255) NOT NULL, message VARCHAR(255) DEFAULT NULL, argument TEXT DEFAULT NULL, argument_type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_46d8accca7c41d6f ON rule (option_id)');
        $this->addSql('COMMENT ON COLUMN rule.argument IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE rule ADD CONSTRAINT fk_46d8accca7c41d6f FOREIGN KEY (option_id) REFERENCES settings (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE option_rules');
    }
}
