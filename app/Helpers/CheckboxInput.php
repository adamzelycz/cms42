<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Helpers;

use Nette\Utils\Html;

class CheckboxInput extends \Czubehead\BootstrapForms\Inputs\CheckboxInput
{

    /** @inheritdoc */
    public function getControl()
    {
        parent::getControl();

        return self::makeCheckbox($this->name, $this->getHtmlId(), $this->caption, $this->value, FALSE, $this->required,
            $this->disabled);
    }

    /** @inheritdoc */
    public static function makeCheckbox(
        $name, $htmlId, $caption = NULL, $checked = FALSE, $value = FALSE, $required = FALSE,
        $disabled = FALSE)
    {
        $label = Html::el('label', ['class' => ['custom-control', 'custom-checkbox']]);
        $input = Html::el('input', [
            'type'     => 'checkbox',
            'class'    => ['custom-control-input'],
            'name'     => $name,
            'disabled' => $disabled,
            'required' => $required,
            'checked'  => $checked,
            'id'       => $htmlId,
        ]);
        if ($value !== FALSE) {
            $input->attrs += [
                'value' => $value,
            ];
        }

        $label->addHtml($input);
        //<edited>
        $label->addHtml(
            Html::el('span', ['class' => ['custom-control-indicator']])
        );
        //</edited>
        $label->addHtml(
            Html::el('label', [
                'class' => ['custom-control-label', 'custom-control-description'], //edited
                'for'   => $htmlId,
            ])->setText($caption)
        );

        $line = Html::el('div');
        $line->setHtml($label);

        return $line;
    }

}
