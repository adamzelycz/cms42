<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Helpers;


class FileSystem
{

    use \Nette\StaticClass;

    public static function copy(string $source, string $destination): void
    {
        if ($source === $destination) {
            throw new \RuntimeException(
                sprintf('Source path %s cannot be equal to Destination path %s', $source, $destination)
            );
        }
        if (!file_exists($source)) {
            throw new \RuntimeException(sprintf('Source path %s not exists', $source));
        }

        if (is_dir($source)) {
            static::createDir($destination); //only if not exists
            $items = static::scanDir($source);
            foreach ($items as $item) {
                if (is_file($source . '/' . $item)) {
                    copy($source . '/' . $item, $destination . '/' . $item);
                } else {
                    if (is_dir($source . '/' . $item)) {
                        if (!is_dir($destination . '/' . $item)) {
                            static::createDir($destination . '/' . $item);
                        }
                        static::copy($source . '/' . $item, $destination . '/' . $item);
                    }
                }
            }
        } else {
            static::createDir(\dirname($destination));
            copy($source, $destination . '/' . basename($source));
        }
    }

    public static function purge(string $dir, bool $deleteBaseDir = false, bool $createIfNotExists = false, bool $safe = false): void
    {
        if (!file_exists($dir) || !is_dir($dir)) {
            if (!$createIfNotExists) {
                throw new \RuntimeException(sprintf('Directory "%s" does not exists', $dir));
            }
            static::createDir($dir);
        }

        $items = static::scanDir($dir);

        if (!$items) {
            if ($deleteBaseDir) {
                rmdir($dir);
            }
            return;
        }
        foreach ($items as $item) {
            if (is_dir($dir . '/' . $item)) {
                static::purge($dir . '/' . $item, true);
            } else {
                if (is_file($dir . '/' . $item)) {
                    unlink(($safe ? 'nette.safe://' : '') . $dir . '/' . $item);
                }
            }
        }
        if ($deleteBaseDir && static::isDirEmpty($dir)) {
            rmdir($dir);
        }
    }

    public static function createDir(string $path, int $mode = 0777, bool $recursive = true): void
    {
        if (!file_exists($path) && !@mkdir($path, $mode, $recursive) && !is_dir($path)) {
            throw new \RuntimeException(sprintf('Directory %s was not created', $path));
        }
    }

    public static function isDirEmpty(string $dir): bool
    {
        return empty(static::scanDir($dir));
    }

    /**
     * @param string $dir
     * @return string[]
     */
    public static function scanDir(string $dir): array
    {
        $items = scandir($dir, \SCANDIR_SORT_NONE);
        if (!$items) {
            return [];
        }
        return array_diff($items, ['.', '..']);
    }

}
