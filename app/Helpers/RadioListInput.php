<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Helpers;

use Nette\Utils\Html;

class RadioListInput extends \Czubehead\BootstrapForms\Inputs\RadioInput
{

    /** @var Html */
    private $container;

    /** @inheritdoc */
    public function __construct($label = null, ?array $items = null)
    {
        parent::__construct($label, $items);
        $this->container = Html::el('fieldset');
    }

    /** @inheritdoc */
    public function getControl()
    {
        $items = $this->getItems();
        $container = $this->container;

        $c = 0;
        $htmlId = $this->getHtmlId();
        foreach ($items as $value => $caption) {
            $disabledOption = $this->isValueDisabled($value);
            $itemHtmlId = $htmlId . $c;

            $wrapper = Html::el('div', [
                'class' => ['custom-control', 'custom-radio'],
            ]);

            $wrapper->addHtml(
                Html::el('input', [
                    'class' => ['custom-control-input'],
                    'type' => 'radio',
                    'value' => $value,
                    'name' => $this->getHtmlName(),
                    'checked' => $this->isValueSelected($value),
                    'disabled' => $disabledOption,
                    'id' => $itemHtmlId,
                ])
            );

            $wrapper->addHtml(
                Html::el('span', [
                    'class' => ['custom-control-indicator'],
                ])
            );

            $wrapper->addHtml(
                Html::el('label', [
                    'class' => ['custom-control-label'],
                    'for' => $itemHtmlId,
                ])->setText($caption)
            );

//            $input = Html::el('input', [
//                'class' => ['custom-control-input'],
//                'type' => 'radio',
//                'value' => $value,
//                'name' => $this->getHtmlName(),
//                'checked' => $this->isValueSelected($value),
//                'disabled' => $disabledOption,
//                'id' => $itemHtmlId,
//            ]);
//
//            $label = Html::el('label', [
//                'class' => ['custom-control-label'],
//                'for' => $itemHtmlId,
//            ])->setText($caption);
//
//            $label->addHtml(
//                $input
//            );
//
//            $label->addHtml(
//                Html::el('span', [
//                    'class' => ['custom-control-indicator'],
//                ])
//            );

                $container->addHtml($wrapper);
            $c++;
        }

        return $container;
    }

}
