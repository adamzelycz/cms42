<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Helpers;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\SmartObject;

class RestartService
{

    use SmartObject;

    /** @var string */
    private $console;

    /** @var string */
    private $tempDir;

    /** @var Cache */
    private $cache;

    /** @var string */
    private $rootDir;

    public function __construct(string $console, string $rootDir, string $tempDir, IStorage $cache)
    {
        $this->tempDir = $tempDir;
        $this->cache = new Cache($cache);
        $this->console = $console;
        $this->rootDir = $rootDir;
    }

    public function cleanCache(): void
    {
        $this->cache->clean([\Nette\Caching\Cache::ALL => true]);
        try {
            FileSystem::purge($this->tempDir . '/cache/Nette.Configurator', false, false, true);
        } catch (\RuntimeException $exception) {
        }
    }

    public function refreshClassmap(): void
    {
        exec('cd ' . $this->rootDir . ' && composer dump-autoload -o --no-dev 2>&1');
    }

    public function dbSchema(): void
    {
        exec(
            $this->console . ' orm:schema-tool:drop --force --no-interaction &&' .
            $this->console . ' migrations:reset --no-interaction &&' .
            $this->console . ' migrations:migrate --no-interaction'
        );
    }

    public function fixtures(): void
    {
        exec($this->console . ' fixtures:load -q');
    }

}
