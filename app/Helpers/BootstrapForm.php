<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Helpers;

use Url\Forms\UrlPathControl;

class BootstrapForm extends \Czubehead\BootstrapForms\BootstrapForm
{

    /** @inheritdoc */
    public function addCheckbox($name, $caption = null)
    {
        $comp = new CheckboxInput($caption);
        $this->addComponent($comp, $name);

        return $comp;
    }

    /** @inheritdoc */
    public function addCheckboxList($name, $label = null, ?array $items = null, string $render = CheckboxListInput::RENDER_VERTICAL)
    {
        $comp = new CheckboxListInput($label, $items, $render);
        $this->addComponent($comp, $name);

        return $comp;
    }

    /** @inheritdoc */
    public function addRadioList($name, $label = null, ?array $items = null)
    {
        $comp = new RadioListInput($label, $items);
        $this->addComponent($comp, $name);

        return $comp;
    }

    public function addUrlPath(string $name, ?string $label = null, ?int $cols = null, int $maxLength = 254): UrlPathControl
    {
        $comp = new UrlPathControl($label, $maxLength);
        if ($cols !== null) {
            $comp->setHtmlAttribute('cols', $cols);
        }
        $this->addComponent($comp, $name);

        return $comp;
    }

}
