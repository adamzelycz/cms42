<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use Kdyby;
use Nette;
use Nette\Application\IPresenterFactory;
use Nette\DI\Compiler;
use Nette\DI\ContainerBuilder;
use Pages\Helpers\RenderableControl;
use Pages\Helpers\RenderControls;

class CompilerExtension extends Nette\DI\CompilerExtension
{

    protected function parseConfig(ContainerBuilder $builder, string $fileName): void
    {
        $config = $this->loadFromFile($fileName);
        Compiler::loadDefinitions($builder, $config['services'] ?? [], $this->name);
        if (isset($config['commands'])) {
            foreach ($config['commands'] as $i => $command) {
                $builder->addDefinition($this->prefix('cli.' . $i))
                    ->addTag(Kdyby\Console\DI\ConsoleExtension::TAG_COMMAND)
                    ->setInject(false)
                    ->setClass($command);
            }
        }
    }

    /** @param array|string[] $mapping */
    protected function setPresenterMapping(ContainerBuilder $builder, array $mapping): void
    {
        $builder
            ->getDefinition($builder->getByType(IPresenterFactory::class) ?: 'nette.presenterFactory')
            ->addSetup('setMapping', [$mapping]);
    }

    /** @param RenderableControl[] $controls ClassNames of control factories that can be renderable on the page TODO extension?*/
    protected function setRenderableControls(array $controls): void
    {
        $definition = $this->getContainerBuilder()->getDefinitionByType(RenderControls::class);
        $definition->addSetup('addRenderableControl', $controls);
    }

}
