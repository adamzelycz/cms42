<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use App\Components\Footer\IFooterFactory;
use App\Components\Footer\Providers\IFooterTemplateProvider;
use App\Components\Header\IHeaderFactory;
use App\Components\Header\Providers\IHeaderTemplateProvider;
use App\Components\Noscript\INoscriptFactory;
use App\Components\Noscript\Providers\INoscriptTemplateProvider;
use Locale\Components\LocaleChanger\ILocaleChangerFactory;
use Locale\Components\LocaleChanger\Providers\ILocaleChangerTemplateProvider;
use Navigation\Components\MainMenu\IMainMenuFactory;
use Navigation\Components\MainMenu\Providers\IMainMenuTemplateProvider;

class ControlTemplateExtension extends CompilerExtension
{

    public function beforeCompile(): void
    {
        $builder = $this->getContainerBuilder();
        /** @var IControl $factory */

        //HEADER
        $providers = $this->compiler->getExtensions(IHeaderTemplateProvider::class);
        if ($providers) {
            /** @var IHeaderTemplateProvider $provider */
            $provider = array_shift($providers);
            $definition = $builder->getDefinitionByType(IHeaderFactory::class);
            $template = $provider->getHeaderTemplateProvider();
            $definition->addSetup('changeTemplate', [$template]);
        }

        //FOOTER
        $providers = $this->compiler->getExtensions(IFooterTemplateProvider::class);
        if ($providers) {
            /** @var IFooterTemplateProvider $provider */
            $provider = array_shift($providers);
            $definition = $builder->getDefinitionByType(IFooterFactory::class);
            $template = $provider->getFooterTemplate();
            $definition->addSetup('changeTemplate', [$template]);
        }

        //NOSCRIPT
        $providers = $this->compiler->getExtensions(INoscriptTemplateProvider::class);
        if ($providers) {
            /** @var INoscriptTemplateProvider $provider */
            $provider = array_shift($providers);
            $definition = $builder->getDefinitionByType(INoscriptFactory::class);
            $template = $provider->getNoscriptTemplate();
            $definition->addSetup('changeTemplate', [$template]);
        }

        //MAIN MENU
        $providers = $this->compiler->getExtensions(IMainMenuTemplateProvider::class);
        if ($providers) {
            /** @var IMainMenuTemplateProvider $provider */
            $provider = array_shift($providers);
            $definition = $builder->getDefinitionByType(IMainMenuFactory::class);
            $template = $provider->getMainMenuTemplate();
            $definition->addSetup('changeTemplate', [$template]);
        }

        //LOCALE CHANGER
        $providers = $this->compiler->getExtensions(ILocaleChangerTemplateProvider::class);
        if ($providers) {
            /** @var ILocaleChangerTemplateProvider $provider */
            $provider = array_shift($providers);
            $definition = $builder->getDefinitionByType(ILocaleChangerFactory::class);
            $template = $provider->getLocaleChangerTemplate();
            $definition->addSetup('changeTemplate', [$template]);
        }
    }

}
