<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use App\Components\Js\IJsFactory;
use App\Components\Js\Providers\IJsAdminProvider;
use App\Components\Js\Providers\IJsAsyncProvider;
use App\Components\Js\Providers\IJsProvider;
use App\Components\Js\Providers\IJsVendorProvider;
use Nette\Utils\Validators;

class JsExtension extends AssetExtension
{

    public function beforeCompile(): void
    {
        $builder = $this->getContainerBuilder();
        $definition = $builder->getDefinitionByType(IJsFactory::class);

        if (!$this->config['minify']) {
            $definition->addSetup('setMinify', [false]);
        }
        if (!$this->config['concatenate']) {
            $definition->addSetup('setConcatenate', [false]);
        }

        /** @var IJsVendorProvider $extension */
        foreach ($this->compiler->getExtensions(IJsVendorProvider::class) as $extension) {
            $vendorScripts = $extension->getJsVendorFiles();
            foreach ($vendorScripts as $vendorScript) {
                if (Validators::isUrl($vendorScript)) {
                    $definition->addSetup('addExternalScript', [$vendorScript]);
                } else {
                    $definition->addSetup('addVendorScript', [$vendorScript]);
                }
            }
        }
//        $definition->addSetup('addAdminScript', [$this->wwwDir . '/assets/nextras.datagrid.js']);

        /** @var IJsProvider $extension */
        foreach ($this->compiler->getExtensions(IJsProvider::class) as $extension) {
            $scripts = $extension->getJsFiles();
            foreach ($scripts as $script) {
                $definition->addSetup('addScript', [$script]);
            }
        }

        /** @var IJsAsyncProvider $extension */
        foreach ($this->compiler->getExtensions(IJsAsyncProvider::class) as $extension) {
            $scripts = $extension->getJsAsyncFiles();
            foreach ($scripts as $script) {
                if (Validators::isUrl($script)) {
                    $definition->addSetup('addExternalAsyncScript', [$script]);
                } else {
                    $definition->addSetup('addAsyncScript', [$script]);
                }
            }
        }

        /** @var IJsAdminProvider $extension */
        foreach ($this->compiler->getExtensions(IJsAdminProvider::class) as $extension) {
            $scripts = $extension->getJsAdminFiles();
            foreach ($scripts as $script) {
                if (Validators::isUrl($script)) {
                    $definition->addSetup('addExternalAdminScript', [$script]);
                } else {
                    $definition->addSetup('addAdminScript', [$script]);
                }
            }
        }

        $definition->addSetup('addScript', [$this->wwwDir . '/assets/front.js']); //default front.js && admin.js
        $definition->addSetup('addAdminScript', [$this->wwwDir . '/assets/admin.js']); //default front.js && admin.js
    }

}
