<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions;

use App\Exceptions\FileNotFound;
use App\Extensions\Providers\IPluginIconProvider;
use Nette\Reflection\ClassType;
use Nette\Utils\Image;
use Plugins\IPlugin;

class PluginIconExtension extends CompilerExtension
{

    /** @var string */
    private $wwwDir;

    public function __construct(string $wwwDir)
    {
        $this->wwwDir = $wwwDir;
    }

    public function beforeCompile(): void
    {
        $extensions = $this->compiler->getExtensions(IPluginIconProvider::class);
        if (!$extensions) {
            return;
        }
        /** @var IPluginIconProvider $extension */
        foreach ($extensions as $extension) {
            $icon = $extension->getPluginIconPath();
            $reflection = ClassType::from(\get_class($extension));
            /** @var IPlugin $extension */
            if ($reflection->implementsInterface(IPlugin::class)) {
                $plugin = $extension->getPlugin();
                $destination = $this->wwwDir . '/plugins/' . $plugin->getIdentifier();
            } else {
                $destination = $this->wwwDir . '/images';
            }
            if (!file_exists($icon)) {
                throw new FileNotFound(sprintf('Icon with path %s not found', $icon));
            }
            $image = Image::fromFile($icon);
            $image = $image->resize(100, 100, Image::EXACT);
            $image->save($destination . '/icon.png', null, Image::PNG);
        }
    }

}
