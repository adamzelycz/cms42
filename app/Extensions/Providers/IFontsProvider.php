<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions\Providers;

interface IFontsProvider
{

    /**
     * TODO zvlášť IFontsDirectoryProvider a IFontsProvider
     * @return array|string[]|string
     * Array of files or path to directory with files (It will copy entire directory)
     */
    public function getFonts();

}
