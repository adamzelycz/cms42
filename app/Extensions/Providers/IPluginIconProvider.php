<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Extensions\Providers;

interface IPluginIconProvider
{

    /** @return string full filePath to icon.png */
    public function getPluginIconPath(): string;

}
