<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Favicon;

use App\Components\BaseControl;

class Favicon extends BaseControl
{

    private const TEMPLATE = __DIR__ . '/templates/Favicon.latte';

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

}
