<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Favicon;

interface IFaviconFactory
{

    public function create(): Favicon;

}
