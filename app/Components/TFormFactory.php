<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components;

use App\Helpers\BootstrapForm;
use Czubehead\BootstrapForms\Enums\RenderMode;

trait TFormFactory
{

    protected function getForm(int $renderMode = RenderMode::SideBySideMode): BootstrapForm
    {
        $form = new BootstrapForm();
        $form->renderMode = $renderMode;

        return $form;
    }

    protected function getAdminForm(int $renderMode = RenderMode::SideBySideMode): BootstrapForm
    {
        $form = new BootstrapForm();
        $form->renderMode = $renderMode;
        $form->addProtection();

        return $form;
    }

}
