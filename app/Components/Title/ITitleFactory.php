<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */


namespace App\Components\Title;


interface ITitleFactory
{

    public function create(): Title;

}
