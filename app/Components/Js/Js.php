<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Js;

use App\Components\Asset;
use MatthiasMullie\Minify;
use WebLoader;

//TODO ValueObject
class Js extends Asset
{

    private const TEMPLATE = __DIR__ . '/templates/Js.latte',
        ADMIN_TEMPLATE = __DIR__ . '/templates/JsAdmin.latte',
        ASYNC_TEMPLATE = __DIR__ . '/templates/JsAsync.latte';

    /** @var string[] */
    private $scripts = [];
    /** @var string[] */
    private $externalScripts = [];

    /** @var string[] */
    private $vendorScripts = [];

    /** @var string[] */
    private $asyncScripts = [];
    /** @var string[] */
    private $externalAsyncScripts = [];

    /** @var string[] */
    private $adminScripts = [];
    /** @var string[] */
    private $externalAdminScripts = [];

    public function render(): void
    {
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    public function renderAdmin(): void
    {
        $this->template->setFile(self::ADMIN_TEMPLATE);
        $this->template->render();
    }

    public function renderAsync(): void
    {
        $this->template->setFile(self::ASYNC_TEMPLATE);
        $this->template->render();
    }

    protected function createComponentJsLoader(): JavascriptLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addFiles($this->scripts);

        return $this->createJsLoader($files);
    }

    protected function createComponentJsAsyncLoader(): JavascriptLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addFiles($this->asyncScripts);
        $files->addRemoteFiles($this->externalAsyncScripts);

        return $this->createJsLoader($files, true, true, true);
    }

    protected function createComponentJsVendorLoader(): JavascriptLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addRemoteFiles($this->externalScripts);
        $files->addFiles($this->vendorScripts);

        return $this->createJsLoader($files);
    }

    protected function createComponentJsAdminLoader(): JavascriptLoader
    {
        $files = new WebLoader\FileCollection();
        $files->addRemoteFiles($this->externalAdminScripts);
        $files->addFiles($this->adminScripts);

        return $this->createJsLoader($files);
    }

    private function createJsLoader(WebLoader\FileCollection $files, bool $nonce = true, bool $async = false, bool $defer = false): JavascriptLoader
    {
        $compiler = WebLoader\Compiler::createJsCompiler($files, $this->wwwDir . '/temp');
        if ($this->minify) {
            $compiler->addFilter(function ($code) {
                $minifier = new Minify\JS();
                $minifier->add($code);
                return $minifier->minify();
            });
        }
        $control = new JavascriptLoader($compiler, $this->template->basePath . '/temp');
        $control->setNonce($nonce)
            ->setAsync($async)
            ->setDefer($defer);
        return $control;
    }

    public function addScript(string $style): void
    {
        $this->scripts[] = $style;
    }

    public function addExternalScript(string $externalScript): void
    {
        $this->externalScripts[] = $externalScript;
    }

    public function addVendorScript(string $vendorScript): void
    {
        $this->vendorScripts[] = $vendorScript;
    }

    public function addAdminScript(string $script): void
    {
        $this->adminScripts[] = $script;
    }

    public function addExternalAdminScript(string $externalScript): void
    {
        $this->externalAdminScripts[] = $externalScript;
    }

    public function addAsyncScript(string $asyncScript): void
    {
        $this->asyncScripts[] = $asyncScript;
    }

    public function addExternalAsyncScript(string $externalAsyncScript): void
    {
        $this->externalAsyncScripts[] = $externalAsyncScript;
    }

}
