<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Flashes;

use App\Components\BaseControl;

class Flashes extends BaseControl
{

    public const
        SUCCESS = 'success',
        ERROR = 'danger',
        INFO = 'info';

    private const TEMPLATE = __DIR__ . '/templates/Flashes.latte';

    public function render(): void
    {
        $this->template->flashes = $this->getPresenter()->getTemplate()->flashes;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

}
