<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Flashes\Providers;

interface IFlashesProvider
{

    public function getFlashesPath(): string;

}
