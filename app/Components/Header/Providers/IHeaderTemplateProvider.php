<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */


namespace App\Components\Header\Providers;


interface IHeaderTemplateProvider
{

    public function getHeaderTemplateProvider(): string;

}
