<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Css;

use Nette\Utils\Html;

class CssLoader extends \WebLoader\Nette\CssLoader
{

    /** @var bool */
    private $nonce = true;

    public function setNonce(bool $nonce): CssLoader
    {
        $this->nonce = $nonce;
        return $this;
    }

    /**
     * Get html element including generated content
     * @param string $source
     * @return \Nette\Utils\Html
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration
     */
    public function getElement($source): Html
    {
        $nonce = null;
        if ($this->nonce) {
            $csp = $this->presenter->getHttpResponse()->getHeader('Content-Security-Policy');
            if ($csp && preg_match('#\s\'nonce-([\w+/]+=*)\'#', $csp, $m)) {
                $nonce = $m[1];
            }
        }
        return
            Html::el('link', [
                'rel' => 'stylesheet',
                'type' => 'text/css',
                'href' => $source,
                'nonce' => $nonce,
            ]);
    }

}
