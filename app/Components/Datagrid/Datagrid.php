<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Datagrid;

use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\Button;
use Nette\Utils\Callback;

/**
 * Class Datagrid
 * @package App\Components\Datagrid
 * @phpcsSuppress
 */
class Datagrid extends \Nextras\Datagrid\Datagrid
{

//    use SecuredLinksControlTrait;

    /** @var callable|null */
    private $deleteCallback;

    /** @var callable|null */
    private $addCallback;

    /** @var callable|null */
    private $addFormFactory;

    /** @var bool */
    private $addRow;

    public function handleDelete(string $primary): void
    {
        ($this->deleteCallback)($primary);
        if ($this->presenter->isAjax()) {
            $this->redrawControl('rows');
        }
    }

    public function setDeleteCallback(?callable $deleteCallback = null): void
    {
        Callback::check($deleteCallback);
        $this->deleteCallback = $deleteCallback;
    }

    public function getDeleteCallback(): ?callable
    {
        return $this->deleteCallback;
    }

    /** @return string[] */
    public function getCellsTemplates(): array
    {
        $templates = $this->cellsTemplates;
        $templates[] = __DIR__ . '/Datagrid.blocks.latte';
        return $templates;
    }

    public function render(): void
    {
        if ($this->filterFormFactory) {
            $this['form']['filter']->setDefaults($this->filter);
        }

        $this->template->form = $this['form'];
        $this->template->data = $this->getData();
        $this->template->columns = $this->columns;
        $this->template->editRowKey = $this->editRowKey;
        $this->template->addRow = $this->addRow;
        $this->template->rowPrimaryKey = $this->rowPrimaryKey;
        $this->template->paginator = $this->paginator;
        $this->template->sendOnlyRowParentSnippet = $this->sendOnlyRowParentSnippet;
        $this->template->cellsTemplates = $this->getCellsTemplates();
        $this->template->showFilterCancel = $this->filterDataSource != $this->filterDefaults; // @ intentionaly
        $this->template->setFile(__DIR__ . '/Datagrid.latte');

        $this->onRender($this);
        $this->template->render();
    }

    public function createComponentForm(): Form
    {
        $form = new Form();

        if ($this->filterFormFactory) {
            $form['filter'] = call_user_func($this->filterFormFactory);
            if (!isset($form['filter']['filter'])) {
                $form['filter']->addSubmit('filter', $this->translate('Filter'));
            }
            if (!isset($form['filter']['cancel'])) {
                $form['filter']->addSubmit('cancel', $this->translate('Cancel'));
            }

            $this->prepareFilterDefaults($form['filter']);
            if (!$this->filterDataSource) {
                $this->filterDataSource = $this->filterDefaults;
            }
        }

        if ($this->editFormFactory && ($this->editRowKey !== null || !empty($_POST['edit']))) {
            $data = $this->editRowKey !== null && empty($_POST) ? $this->getData($this->editRowKey) : null;
            $form['edit'] = call_user_func($this->editFormFactory, $data);

            if (!isset($form['edit']['save']))
                $form['edit']->addSubmit('save', 'Save');
            if (!isset($form['edit']['cancel']))
                $form['edit']->addSubmit('cancel', 'Cancel')->setValidationScope(false);
            if (!isset($form['edit'][$this->rowPrimaryKey]))
                $form['edit']->addHidden($this->rowPrimaryKey);

            $form['edit'][$this->rowPrimaryKey]
                ->setDefaultValue($this->editRowKey)
                ->setOption('rendered', true);
        }

        if ($this->globalActions) {
            $actions = array_map(function($row) { return $row[0]; }, $this->globalActions);
            $form['actions'] = new Container();
            $form['actions']->addSelect('action', 'Action', $actions)
                ->setPrompt('- select action -');
            $form['actions']->addCheckboxList('items', '', []);
            $form['actions']->addSubmit('process', 'Do');
        }

        if ($this->translator) {
            $form->setTranslator($this->translator);
        }

        $form->onSuccess[] = function(): void {}; // fix for Nette Framework 2.0.x
        $form->onSubmit[] = [$this, 'processForm'];
        if ($this->addFormFactory) {
            $form['add'] = ($this->addFormFactory)();

            if (!isset($form['add']['save'])) {
                $form['add']->addSubmit('add', 'Add');
            }
            if (!isset($form['add']['cancel'])) {
                $form['add']->addSubmit('cancel', 'Cancel')->setValidationScope(false);
            }
        }
        return $form;
    }

    public function processForm(Form $form): void
    {
        if (isset($form['add'])) {
            if ($form['add']['add']->isSubmittedBy() && $form['add']->isValid()) {
                ($this->addCallback)($form['add']);
            }
            if ($form['add']['cancel']->isSubmittedBy() || ($form['add']['add']->isSubmittedBy() && $form['add']->isValid())) {
                $this->redirect('this');
            }
        }
        parent::processForm($form);
    }

    public function handleAdd(?string $cancelEditPrimaryValue = null): void
    {
        $this->addRow = true;
        if ($this->presenter->isAjax()) {
            $this->redrawControl('rows');
            if ($cancelEditPrimaryValue) {
                foreach (explode(',', $cancelEditPrimaryValue) as $pv) {
                    $this->redrawRow($pv);
                }
            }
        }
    }

    public function setAddCallback(callable $callback): void
    {
        Callback::check($callback);
        $this->addCallback = $callback;
    }

    public function getAddFormFactory(): ?callable
    {
        return $this->addFormFactory;
    }

    public function setAddFormFactory(callable $addFormFactory): void
    {
        $this->addFormFactory = $addFormFactory;
    }


    private function prepareFilterDefaults(Container $container): void
    {
        $this->filterDefaults = [];
        foreach ($container->controls as $name => $control) {
            if ($control instanceof Button) {
                continue;
            }

            $value = $control->getValue();
            if (!self::isEmptyValue($value)) {
                $this->filterDefaults[$name] = $value;
            }
        }
    }

    /** @param mixed $value */
    private static function isEmptyValue($value): bool
    {
        return $value === NULL || $value === '' || $value === [] || $value === false;
    }

}
