<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Meta;

use App\Components\BaseControl;
use Nette\Http\IRequest;
use Settings\SettingsFacade;

/** TODO config.neon + plugin */
class Meta extends BaseControl
{

    public const
        ROBOTS_NOINDEX = 'noindex',
        ROBOTS_ALL = 'all';

    private const TEMPLATE = __DIR__ . '/Meta.latte';

    /** @var string[] */
    private $metas = [
        'robots' => 'all',
    ];

    /** @var string[] */
    private $ogs = [];

    /** @var IRequest */
    private $request;

    /** @var SettingsFacade */
    private $settingsFacade;

    public function __construct(IRequest $request, SettingsFacade $settingsFacade)
    {
        parent::__construct();
        $this->request = $request;
        $this->settingsFacade = $settingsFacade;
    }

    public function render(): void
    {
        $this->setDefaults();
        $this->template->metas = $this->metas;
        $this->template->ogs = $this->ogs;
        $this->template->setFile(self::TEMPLATE);
        $this->template->render();
    }

    public function setMeta(string $name, string $content): void
    {
        $this->metas[$name] = $content;
    }

    public function setOg(string $property, string $content): void
    {
        $this->ogs[$property] = $content;
    }

    private function setDefaults(): void
    {
        $siteName = $this->settingsFacade->getOptionValue('site_name');
        if ($siteName) {
            $this->setMeta('apple-mobile-web-app-title', $siteName);
            $this->setMeta('application-name', $siteName);
            $this->setOg('og:site_name', $siteName);
        }

        $googleVerification = $this->settingsFacade->getOptionValue('google_verification');
        if ($googleVerification) {
            $this->setMeta('google-site-verification', $googleVerification);
        }

        $description = $this->settingsFacade->getOptionValue('site_description');
        if ($description) {
            $this->setMeta('description', $description);
        }

        $color = $this->settingsFacade->getOptionValue('color');
        if ($color) {
            $this->setMeta('theme-color', $color);
            $this->setMeta('msapplication-navbutton-color', $color);
            $this->setMeta('apple-mobile-web-app-status-bar-style', $color);
        }

        $this->setOg('og:url', (string)$this->request->getUrl());
        $this->ogs['og:image'] = $this->request->getUrl()->getBaseUrl() . 'android-chrome-384x384.png';
    }

}
