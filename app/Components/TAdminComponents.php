<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components;

trait TAdminComponents
{

    use TPublicComponents;

    protected function createComponentSidebar(\App\Components\Admin\Sidebar\ISidebarFactory $factory): \App\Components\Admin\Sidebar\Sidebar
    {
        return $factory->create();
    }

    protected function createComponentAdminHeader(\App\Components\Admin\Header\IHeaderFactory $factory): \App\Components\Admin\Header\Header
    {
        return $factory->create();
    }

    protected function createComponentTutorial(\App\Components\Admin\Tutorial\ITutorialFactory $factory): \App\Components\Admin\Tutorial\Tutorial
    {
        return $factory->create();
    }

}
