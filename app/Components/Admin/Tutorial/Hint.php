<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\Tutorial;

class Hint
{

    /** @var string */
    private $title;

    /** @var string */
    private $hint;

    public function __construct(string $title, string $hint)
    {
        $this->title = $title;
        $this->hint = $hint;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getHint(): string
    {
        return $this->hint;
    }

}
