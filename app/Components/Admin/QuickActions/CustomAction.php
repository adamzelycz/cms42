<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions;

use Nette\Utils\Html;

interface CustomAction extends Action
{

    /**
     * Standard render example:
     *
     *  use Nette\Utils\Html;
     *
     *  ---------------------------------------------------------------------------
     *
     *  $el = Html::el('li', ['class' => 'dropdown-item']);
     *  $a = Html::el('a', [
     *      'href' => htmlspecialchars_decode((string)$link),
     *      'title' => 'Something',
     *      'class' => 'd-b td-n pY-5 bgcH-grey-100 c-grey-700',
     *  ]);
     *
     *  $img = Html::el('img', [
     *      'class' => 'w-2r',
     *      'src' => $basePath . '/images/something.svg',
     *      'alt' => 'Something',
     *  ]);
     *
     *  $span = Html::el('span', [
     *      'class' => 'ml-2',
     *  ])->addText('Something');
     *
     *  $a->addHtml($img);
     *  $a->addHtml($span);
     *  $el->addHtml($a);
     *  return $el;
     */
    public function getElement(string $link, string $basePath): Html;

}
