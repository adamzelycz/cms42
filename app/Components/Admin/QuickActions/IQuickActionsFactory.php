<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions;

interface IQuickActionsFactory
{

    public function create(): QuickActions;

}
