<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions;

interface SimpleAction extends Action
{

    public function getTitle(): string;

//    public function getHref(): string;
//
    /** Source of image without basePath (eg. images/image.svg */
    public function getImageSrc(): string;

}
