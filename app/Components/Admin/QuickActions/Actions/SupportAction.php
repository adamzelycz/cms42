<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions\Actions;

use App\Components\Admin\QuickActions\QuickActions;
use App\Components\Admin\QuickActions\SimpleAction;

class SupportAction implements SimpleAction
{

    public function process(QuickActions $control): void
    {
        // beforeRelease TODO: Implement process() method.
    }

    public function getTitle(): string
    {
        return 'Support';
    }

    public function getImageSrc(): string
    {
        return 'images/lifebuoy.svg';
    }

}
