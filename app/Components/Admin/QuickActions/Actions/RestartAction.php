<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Components\Admin\QuickActions\Actions;

use App\Components\Admin\QuickActions\QuickActions;
use App\Components\Admin\QuickActions\SimpleAction;
use App\Components\Flashes\Flashes;
use App\Helpers\RestartService;

class RestartAction implements SimpleAction
{

    /** @var RestartService */
    private $restartService;

    public function __construct(RestartService $restartService)
    {
        $this->restartService = $restartService;
    }

    public function process(QuickActions $control): void
    {
        $this->restartService->refreshClassmap();
        $this->restartService->cleanCache();
        $control->flashMessage('CMS was restarted, it may be slower after first requests', Flashes::SUCCESS);
    }

    public function getTitle(): string
    {
        return 'Restart';
    }

    public function getImageSrc(): string
    {
        return 'images/restart.svg';
    }

}
