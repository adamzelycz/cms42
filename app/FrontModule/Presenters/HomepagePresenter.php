<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\FrontModule\Presenters;

/**
 * Class HomepagePresenter
 * @package App\FrontModule\Presenters
 * This presenter is not used by default. There is redirect to the Homepage in administration
 */
class HomepagePresenter extends BasePresenter
{

    protected function beforeRender(): void
    {
        parent::beforeRender();
        $this->setTitle('homepage.title', true);
    }

}
