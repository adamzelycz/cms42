<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\FrontModule\Presenters;

use App\Components\Meta\Meta;
use App\Components\TPublicComponents;
use Kdyby\Translation\Translator;
use Nextras\Application\UI\SecuredLinksPresenterTrait;
use Nittro\Bridges\NittroUI\Presenter;

/**
 * Class BasePresenter
 * @package App\FrontModule\Presenters
 */
abstract class BasePresenter extends Presenter
{

    use TPublicComponents;
    use SecuredLinksPresenterTrait;

    /** @var string @persistent */
    public $locale;

    /** @var Translator @inject */
    public $translator;

    public function findLayoutTemplateFile(): ?string
    {
        if ($this->layout === false) {
            return null;
        }
        return __DIR__ . '/templates/@layout.latte';
    }

    protected function startup(): void
    {
        parent::startup();
        $this->setDefaultSnippets(['pageContent', 'title', 'header']);
    }

    protected function beforeRender(): void
    {
        $this->template->locale = $this->locale;
    }

    /**
     * Performs full HTTP redirect without AJAX
     * @param array|mixed[] $args
     */
    protected function fullRedirect(string $destination, array $args = []): void
    {
        $this->disallowAjax();
        $this->redirect($destination, $args);
    }

    protected function setTitle(string $title, bool $translate = false): void
    {
        if ($translate) {
            $title = (string)$this->translator->translate($title);
        }
        $this['title']->setTitle($title);
        $this->setOg('og:title', $title);
    }

    protected function setDescription(string $description): void
    {
        $this->setMeta('description', $description);
        $this->setOg('og:description', $description);
    }

    protected function setMeta(string $name, string $content): void
    {
        $this['meta']->setMeta($name, $content);
    }

    protected function setOg(string $property, string $content): void
    {
        $this['meta']->setOg($property, $content);
    }

    protected function setNoIndex(): void
    {
        $this['meta']->setMeta('robots', Meta::ROBOTS_NOINDEX);
    }

}
