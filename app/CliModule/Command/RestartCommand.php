<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\CliModule\Command;

use App\Helpers\RestartService;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RestartCommand extends Command
{

    /** @var RestartService @inject */
    public $restartService;

    /** @var Logger @inject */
    public $monolog;

    protected function configure(): void
    {
        $this
            ->setName('cms:restart')
            ->setDescription('Restarts application. (Clean cache, rebuild classmaps, etc.) It is absolutely safe and no data will be deleted')
            ->setHelp(<<<EOT
The <info>cms:restart</info> command restarts entire CMS application.

  <info>bin/console cms:restart</info>
Command will clear application cache and rebuild classmaps. May takes few seconds
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {

        try {
            $output->writeln('Refreshing classmap');
            $this->restartService->refreshClassmap();
            $output->writeln('Deleting cache files');
            $this->restartService->cleanCache();
            $output->writeln('Done');
            $this->monolog->addDebug('Restart command successfully executed');
            return 0;//no error
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
            $this->monolog->addError(sprintf('Restart Command failerd: %s', $exception->getMessage()));
            return 1;//error(s)
        }
    }

}
