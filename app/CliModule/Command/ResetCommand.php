<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\CliModule\Command;

use App\Helpers\RestartService;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ResetCommand extends Command
{

    /** @var RestartService @inject */
    public $restartService;

    /** @var Logger @inject */
    public $monolog;

    protected function configure(): void
    {
        $this
            ->setName('cms:reset')
            ->setDescription('Resets application. WARNING! This command will delete entire database.')
            ->setHelp(<<<EOT
The <info>cms:reset</info> command reset entire CMS application and brings it to factory defaults

  <info>bin/console cms:reset</info>
Command will clear application cache, rebuild classmaps, drop database schema and load default data.

May takes few seconds
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        if ($input->isInteractive()) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('This operation will purge the database. You should never run in production environment. Do you wanna continue? [y/n]', false);

            if (!$helper->ask($input, $output, $question)) {
                return 0;//no error
            }
        }
        try {
            $output->writeln('Refreshing classmap');
            $this->restartService->refreshClassmap();
            $output->writeln('Deleting cache files');
            $this->restartService->cleanCache();
            $output->writeln('Deleting DB schema');
            $this->restartService->dbSchema();
            $output->writeln('Loading default DB data');
            $this->restartService->fixtures();
            $output->writeln('Done');
            $this->monolog->addDebug('Reset command successfully executed');
            return 0;//no error
        } catch (\Throwable $exception) {
            $output->writeln($exception->getMessage());
            $this->monolog->addError(sprintf('Reset Command failed: %s', $exception->getMessage()));
            return 1;//error(s)
        }
    }

}
