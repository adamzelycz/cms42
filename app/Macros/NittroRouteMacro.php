<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace App\Macros;

use Latte\CompileException;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use function addslashes;

//TODO Use Nittro DI container only once
class NittroRouteMacro extends MacroSet
{

    public static function install(Compiler $compiler): NittroRouteMacro
    {
        $macroset = new static($compiler);
        $macroset->addMacro('nittroDom', [$macroset, 'nittroDomBegin'], [$macroset, 'nittroRouteEnd']);
        $macroset->addMacro('nittroUrl', [$macroset, 'nittroUrlBegin'], [$macroset, 'nittroRouteEnd']);
        return $macroset;
    }

    public function nittroDomBegin(MacroNode $node, PhpWriter $writer): string
    {
        if (!$node->args) {
            throw new CompileException('Nittro DOM Route macro must get DOM selector in the parameter. None given');
        }
        $selector = $node->args;
        $string = addslashes($this->universalBegin('getDOMRoute', $selector));
        return $writer->write("echo '$string';");
    }

    public function nittroUrlBegin(MacroNode $node, PhpWriter $writer): string
    {
        if (!$node->args) {
            throw new CompileException('Nittro URL Route macro must get URL selector in the parameter. None given');
        }
        $selector = $node->args;
        $string = addslashes($this->universalBegin('getURLRoute', $selector));
        return $writer->write("echo '$string';");
    }

    public function nittroRouteEnd(MacroNode $node, PhpWriter $writer): string
    {
        return $writer->write("echo '})
                    .teardown(function () {
                        setup = false;
                    });
            });
    }, {
        DOM: \'Utils.DOM\'
    }]);';");
    }

    private function universalBegin(string $method, string $selector): string
    {
        return 'window._stack.push([function (di, DOM) {
        let setup = false;
        let router = di.getService(\'router\');
        let page = di.getService(\'page\');

        router.' . $method . '(' . $selector . ')
            .on(\'match\', function (evt) {
                if (setup) return;
                setup = true;

                page.getSnippet(\'snippet--pageContent\')
                    .setup(function () {';
    }

}
