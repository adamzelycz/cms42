<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

$loader = require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator();

$configurator->setDebugMode(false);//beforeCommit
$configurator->enableTracy(__DIR__ . '/../log', 'log@adamzelycz.cz');
\Tracy\Debugger::$logSeverity = E_NOTICE | E_WARNING;

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->addParameters([
    'appDir' => __DIR__,
    'wwwDir' => __DIR__ . '/../www',
    'pluginsDir' => __DIR__ . '/../plugins',
    'console' => __DIR__ . '/../bin/console',
    'rootDir' => __DIR__ . '/..',
]);

$configurator->onCompile[] = function ($_, \Nette\DI\Compiler $compiler) use ($loader): void {
    $compiler->addExtension('plugin', new \Plugins\DI\PluginsRegistrarExtension(__DIR__ . '/../www', $loader));
};

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

return $configurator->createContainer();
