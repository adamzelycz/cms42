<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace TestPlugin\ICanDoAlmostEverything\DI;

use Plugins\Install\IInstallable;
use Plugins\Plugin;
use Plugins\PluginExtension;
use TestPlugin\ICanDoAlmostEverything\Install\Install;

class ICanDoAlmostEverythingExtension extends PluginExtension implements IInstallable
{

    public function getPlugin(): Plugin
    {
        $plugin = new Plugin();
        $plugin->setName('Plugin for developing');
        $plugin->setDescription('This plugin is used only for developing CMS');
        $plugin->setVersion('42.42');
        $plugin->setAuthor('John');
        $plugin->setIdentifier('test-plugin', 'i-can-do-almost-everything');
        return $plugin;
    }

    public function getInstallClass(): string
    {
        return Install::class;
    }

}
