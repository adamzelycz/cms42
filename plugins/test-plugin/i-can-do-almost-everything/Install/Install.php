<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace TestPlugin\ICanDoAlmostEverything\Install;

use Plugins\Install\AbstractInstall;
use Plugins\Install\InstallHelper;
use Plugins\Plugin;
use Settings\Option;
use Settings\Types\Text;

class Install extends AbstractInstall
{

    private const OPTIONS = [
        ['google_verification', 'testValue1', 'testName1', Text::class],
        ['testKey2', 'testValue2', 'testName2', Text::class],
    ];
    /** @var Option[] */
    private $options = [];

    public function __construct(Plugin $plugin, InstallHelper $install)
    {
        parent::__construct($plugin, $install);
        //OPTIONS
        foreach (self::OPTIONS as $optionArray) {
            $option = new Option($optionArray[0]);
            $option->setValue($optionArray[1]);
            $option->setName($optionArray[2]);
            $option->setType(new $optionArray[3]());
            $option->setPlugin($this->plugin);
            $this->options[] = $option;
        }
    }

    public function install(): void
    {
        $this->install->options($this->options);
    }

    public function uninstall(): void
    {
        $this->install->options($this->options, true);
    }

}
