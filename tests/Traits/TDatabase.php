<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Traits;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\Configuration\Configuration;
use Doctrine\DBAL\Migrations\Migration;
use Kdyby\Doctrine\EntityManager;
use Nette\DI\Container;
use Test\Helpers\ContainerFactory;
use Test\Mocks\ConnectionMock;
use Tester\Assert;

/** @deprecated */
trait TDatabase
{

    /** @var string|null */
    private $databaseName;

    protected function getContainer(bool $new = false): Container
    {
        /** @var Connection $connection */
        $container = ContainerFactory::getContainer($new);
        /** @var ConnectionMock $connection */
        $connection = $container->getByType(Connection::class);

        $connection->onConnect[] = function (Connection $connection): void {
            if ($this->databaseName !== null) {
//                $this->setConnectionParams($connection);
                return;
            }
            try {
                $this->setupDatabase($connection);
            } catch (\Throwable $exception) {
                Assert::fail($exception->getMessage());
            }
        };

        return $container;
    }

    protected function getEm(bool $newConnection = false): EntityManager
    {
        if ($newConnection) {
            $this->databaseName = null;
        }
        /** @var EntityManager $em */
        $em = $this->getContainer($newConnection)->getByType(EntityManager::class);
        $connection = $em->getConnection();
        $this->setConnectionParams($connection);
        return $em;
    }

    private function setupDatabase(Connection $connection): void
    {
        $this->databaseName = 'db_' . getmypid();
        $this->dropDatabase($connection);
        $this->createDatabase($connection);
        $connection->transactional(function () use ($connection): void {
            $this->runMigrations($connection);
            $this->runFixtures();
        });

        register_shutdown_function(function () use ($connection): void {
            $this->dropDatabase($connection);
        });
    }

    private function runMigrations(Connection $connection): void
    {
        $container = $this->getContainer();
        /** @var Configuration $migrationConf */
        $migrationConf = $container->getByType(Configuration::class);
        $migrationConf->__construct($connection);
//        $migrationConf = new Configuration($connection);
        $migrationConf->registerMigrationsFromDirectory($migrationConf->getMigrationsDirectory());
        $migration = new Migration($migrationConf);
        $migration->migrate($migrationConf->getLatestVersion());
    }

    private function runFixtures(): void
    {
        $loader = new Loader();
        $loader->loadFromDirectory(__DIR__ . '/../../src/Fixtures/Basic');
        $fixtures = $loader->getFixtures();

        $executor = new ORMExecutor($this->getEm());

        $executor->execute($fixtures, true);
    }

    private function createDatabase(Connection $connection): void
    {
        $connection->exec('CREATE DATABASE ' . $this->databaseName);
        $this->connectToDb($connection);
    }

    private function dropDatabase(Connection $connection): void
    {
        $connection->exec('DROP DATABASE IF EXISTS ' . $this->databaseName);
    }

    private function connectToDb(Connection $connection): void
    {
        $connection->close();
        $this->setConnectionParams($connection);
        $connection->connect();
    }

    private function setConnectionParams(Connection $connection): void
    {
        $connection->__construct(
            ['dbname' => $this->databaseName] + $connection->getParams(),
            $connection->getDriver(),
            $connection->getConfiguration(),
            $connection->getEventManager()
        );
    }

}
