<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Traits;

use Nette\Application\IPresenter;
use Nette\Application\IPresenterFactory;
use Nette\Application\IResponse;
use Nette\Application\Request;
use Nette\Application\Responses\RedirectResponse;
use Nette\Application\Responses\TextResponse;
use Nette\Application\UI\ITemplate;
use Nette\Application\UI\Presenter;
use Nette\Security\Identity;
use Nette\Security\User;
use Nette\Utils\Strings;
use Test\Helpers\ContainerFactory;
use Tester\Assert;
use Tester\DomQuery;

/**
 * Class PresenterTester
 * @package Test
 */
trait TPresenter
{

    /** @var IPresenter */
    private $presenter;

    /**
     * @param array|mixed[] $params
     * @param array|mixed[] $post
     * @throws \Exception
     */
    protected function checkAction(string $destination, string $method = 'GET', array $params = [], array $post = []): IResponse
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->check($destination, $method, $params, $post);

        Assert::type(TextResponse::class, $response);
        Assert::type(ITemplate::class, $response->getSource());

        $html = (string)$response->getSource();
        $html = Strings::replace($html, '#<svg[^>]*+>.+?</svg>#s', ''); //https://github.com/nette/tester/issues/354
        $dom = DomQuery::fromHtml($html);
        Assert::true($dom->has('title'));

        return $response;
    }

    /**
     * @param array|mixed[] $post
     * @throws \Exception
     */
    protected function checkForm(string $destination, string $method = 'POST', array $post = []): IResponse
    {
        $response = $this->check($destination, $method, [], $post);

        Assert::type(RedirectResponse::class, $response);

        return $response;
    }

    /**
     * @param array|mixed[] $params
     * @param array|mixed[] $post
     */
    private function check(string $destination, string $method = 'GET', array $params = [], array $post = []): IResponse
    {
        $destination = ltrim($destination, ':');
        $pos = strrpos($destination, ':');
        $presenterName = substr($destination, 0, $pos);
        $action = substr($destination, $pos + 1) ?: 'default';

        $this->presenter = $this->createPresenter($presenterName);

        $params['action'] = $action;
        $request = new Request($presenterName, $method, $params, $post);
        return $this->presenter->run($request);
    }

    /**
     * @param array|null|mixed[] $data
     * @throws \Nette\Security\AuthenticationException
     */
    protected function fakeLogin(int $id, string $role, ?array $data = null): void
    {
        $identity = new Identity($id, $role, $data);
        $container = ContainerFactory::getContainer();
        $user = $container->getByType(User::class);
        $user->login($identity);
    }

    protected function createPresenter(string $name): Presenter
    {
        $presenterFactory = ContainerFactory::getContainer()->getByType(IPresenterFactory::class);
        /** @var Presenter $presenter */
        $presenter = $presenterFactory->createPresenter($name);
        $presenter->autoCanonicalize = false;
        return $presenter;
    }

}
