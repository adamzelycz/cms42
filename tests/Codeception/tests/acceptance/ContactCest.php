<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class ContactCest
{

    public function testContactFormSuccess(AcceptanceTester $I): void
    {
        $I->amOnPage('/contact');
        $I->fillField('name', 'Jméno');
        $I->fillField('email', 'email@email.email');
        $I->fillField('message', 'Zpráva');
        $I->click('Odeslat');
//        $I->see('Zpráva byla odeslána, brzy se Vám ozveme'); //Gitlab CI cannot send Emails
    }
// NEEDS JAVASCRIPT (TODO Selenium)
//    public function testContactFormEmailError(AcceptanceTester $I): void
//    {
//        $I->amOnPage('/en/contact');
////        $I->fillField('name', '');
//        $I->fillField('email', 'email@email');
////        $I->fillField('message', 'Message');
//        $I->click('Send');
//        $I->see('Please enter your email address in the correct format.');
//        $I->see('Please fill out name.');
//        $I->see('Please fill out a message area.');
//    }
}
