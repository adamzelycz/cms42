<?php declare(strict_types=1);

use Codeception\Util\Locator;
use Nette\Utils\Random;

class PageEditorCest
{

    public function editorTest(AcceptanceTester $I): void
    {
        $I->loginAsRoot();
        $url = 'test-url' . Random::generate();
        $title = 'test-title' . Random::generate();

        $I->amOnPage('/admin/pages/edit');
        $I->fillField('title', $title);
        $I->fillField('url', $url);
        $I->fillField('content', 'Řčš');
        //TODO Selenium + js + authorsTest + dynamicControls
        $I->fillField('htmlTitle', 'abc');
        $I->fillField('description', 'cde');
//        $I->fillField('customCss', '#you-caant-see-me {display: none;}');
        $I->click('input[name="saveAndRedirect"]');
        $I->seeInCurrentUrl('/' . $url);
        $I->see($title);
        $I->seeInTitle('abc | CMS42');
        $I->click('Edit');

        $I->checkOption('hideTitle');
        $I->click('input[name="saveAndRedirect"]');
        $I->seeInCurrentUrl('/' . $url);
        $I->dontSee($title);

        $I->click('Edit');
        $I->seeInCurrentUrl('/admin/pages/edit?id=');
        $I->fillField('url', strrev($url));
        $I->click('Save and stay');

        $I->seeInCurrentUrl('/admin/pages/edit?id=');
        $I->canSee($url); //alias

        //DELETE alias
        $I->click(Locator::find('a', ['title' => 'Delete alias ' . $url]));
        $I->dontSee($url);

        //CREATE alias
        $I->fillField('urlAlias', 'new-alias' . $url);
        $I->click('Add');
        $I->see('new-alias' . $url);

        $I->amOnPage('/new-alias' . $url);
        $I->seeInCurrentUrl(strrev($url));

        $I->click('Delete');
        $I->seeInCurrentUrl('admin/pages');
        $I->dontSeeInCurrentUrl('edit');
        $I->see('Page was deleted');
        $I->dontSee($title);
    }

//    public function selectVersionTest(AcceptanceTester $I): void
//    {
//        $I->loginAsRoot();
//        $url = 'test-url' . Random::generate();
//        $title = 'test-title' . Random::generate();
//
//        $I->amOnPage('/admin/pages/edit');
//        $I->dontSeeElement('#previous-versions-modal');
//
//        $I->fillField('title', $title);
//        $I->fillField('url', $url);
//        $I->fillField('content', 'Řčš');
//        $I->click('Save and stay');
//
//        $I->seeInCurrentUrl('/admin/pages/edit?id=');
//        $I->seeElement('#previous-versions-modal');
//        $I->fillField('content', 'Řčýu');
//        $I->click('Save and stay');
//        $I->seeElement('input[value="Řčýu"]');
//        $I->dontSeeElement('input[value="Řčš"]');
//
//        $I->see('3 characters');
//        $I->see('4 characters');
//        $I->click('Select'); //only one button with 3 characters
//
//        $I->seeElement('input[value="Řčš"]');
//        $I->dontSeeElement('input[value="Řčýu"]');
//        $I->see('3 characters');
//        $I->see('4 characters');
//
//        $I->click('Delete');
//        $I->seeInCurrentUrl('admin/pages');
//        $I->dontSee($title);
//    }

//    public function discardTest(AcceptanceTester $I): void
//    {
//        $I->loginAsRoot();
//        $I->amOnPage('/admin/pages/edit');
//        $I->click('Discard');
//        $I->seeCurrentUrlEquals('/admin/pages');
//    }
}
