<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

class LocaleChangerCest
{

    public function testChange(AcceptanceTester $I): void
    {
        $I->amOnPage('/contact');
        $I->click('.locale-changer__locale a[href="/en/contact"]');
        $I->seeCurrentUrlEquals('/en/contact');
        $I->click('.locale-changer__locale a[href="/contact"]');
        $I->seeCurrentUrlEquals('/contact');
    }
}
