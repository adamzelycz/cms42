<?php declare(strict_types=1);

class StatsSummaryCest
{

    public function selectFormTest(AcceptanceTester $I): void
    {
        $I->loginAsRoot();
        $I->amOnPage('/admin/stats/summary');
//        shell_exec('bin/console stats:load');
//        $time = new \DateTimeImmutable('now', new DateTimeZone('Europe/Prague'));
//        $I->see('Last refresh: ' . $time->format('j. n. Y H:i:'));
        $I->checkOption('HTTP Code');
        $I->checkOption('IP Address');
        $I->click('Submit');
//        $I->seeElement('.badge.badge-primary.badge-pill.badge-warning');
        $I->seeElement('h4.h5');
//        $I->seeElement('ul.list-group');
        $I->seeInCurrentUrl('totalStats-columns=httpCode%26ipAddress');
    }
}
