<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Macros;

use App\Macros\NittroRouteMacro;
use Latte\CompileException;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\PhpWriter;
use Mockery;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class NittroRouteMacroTest extends TestCase
{

    /** @var NittroRouteMacro */
    private $macro;

    /** @var PhpWriter|Mockery\MockInterface */
    private $writer;

    protected function setUp(): void
    {
        $this->macro = new NittroRouteMacro(new Compiler());
        $this->writer = Mockery::mock(PhpWriter::class);
    }

    public function testEmptyParameter(): void
    {
        $node = new MacroNode($this->macro, 'nittroDom');
        Assert::exception(function () use ($node): void {
            $this->macro->nittroDomBegin($node, $this->writer);
        }, CompileException::class);
        $node = new MacroNode($this->macro, 'nittroUrl');
        Assert::exception(function () use ($node): void {
            $this->macro->nittroUrlBegin($node, $this->writer);
        }, CompileException::class);
    }

    public function testDom(): void
    {
        $node = new MacroNode($this->macro, 'nittroDom', 'superSelector');
        $this->writer->shouldReceive('write')
            ->andReturnUsing(function (string $arg): string {
                ob_start();
                eval($arg);
                return ob_get_clean();
            });
        Assert::same('window._stack.push([function (di, DOM) {
        let setup = false;
        let router = di.getService(\'router\');
        let page = di.getService(\'page\');

        router.getDOMRoute(superSelector)
            .on(\'match\', function (evt) {
                if (setup) return;
                setup = true;

                page.getSnippet(\'snippet--pageContent\')
                    .setup(function () {', $this->macro->nittroDomBegin($node, $this->writer));
    }


    public function testUrl(): void
    {
        $node = new MacroNode($this->macro, 'nittroDom', 'superSelector2');
        $this->writer->shouldReceive('write')
            ->andReturnUsing(function (string $arg): string {
                ob_start();
                eval($arg);
                return ob_get_clean();
            });
        Assert::same('window._stack.push([function (di, DOM) {
        let setup = false;
        let router = di.getService(\'router\');
        let page = di.getService(\'page\');

        router.getURLRoute(superSelector2)
            .on(\'match\', function (evt) {
                if (setup) return;
                setup = true;

                page.getSnippet(\'snippet--pageContent\')
                    .setup(function () {', $this->macro->nittroUrlBegin($node, $this->writer));
    }


    public function testEnd(): void
    {
        $node = new MacroNode($this->macro, 'nittroDom');
        $this->writer->shouldReceive('write')
            ->andReturnUsing(function (string $arg): string {
                ob_start();
                eval($arg);
                return ob_get_clean();
            });
        Assert::same('})
                    .teardown(function () {
                        setup = false;
                    });
            });
    }, {
        DOM: \'Utils.DOM\'
    }]);', $this->macro->nittroRouteEnd($node, $this->writer));
    }

}

(new NittroRouteMacroTest())->run();
