<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Plugins\Install;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Mockery;
use Nette\DI\Container;
use Plugins\AllPluginsContainer;
use Plugins\Exceptions\PluginInstallException;
use Plugins\Install\Installer;
use Plugins\Plugin;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../../bootstrap.php';

/**
 * @testCase
 */
class InstallerTest extends TestCase
{

    private const PLUGINS_ARRAY = [
        ['Medvídek', 'medvidek', 'puu', 'Medvídek Pú je hlavní postava – personifikovaný medvídek v dětské knize', 'Walt Disney', '0'],
        ['Oslík ze Shreka', 'shrek', 'oslik', 'Už tam budem?', 'Někdo', '2001'],
    ];

    /** @var EntityManager|Mockery\MockInterface */
    private $em;

    /** @var Plugin[] */
    private $plugins;

    /** @var Installer */
    private $installer;

    /** @var string */
    private $fileInstalledBackup;

    protected function setUp(): void
    {
        $this->fileInstalledBackup = file_get_contents('nette.safe://' . Installer::FILE);
        $pluginsRepository = Mockery::mock(EntityRepository::class);
        $pluginsRepository
            ->shouldReceive('findOneBy')
            ->once()
            ->andReturnUsing(function (array $parameters) {
                return $this->findOneBy(array_shift($parameters));
            });

        $em = Mockery::mock(EntityManager::class);
        $em->shouldReceive('getRepository')->once()->andReturn($pluginsRepository);
        $this->em = $em;

        $containerMock = Mockery::mock(Container::class);

        foreach (self::PLUGINS_ARRAY as $item) {
            $plugin = new Plugin();
            $plugin->setName($item[0]);
            $plugin->setIdentifier($item[1], $item[2]);
            $plugin->setDescription($item[3]);
            $plugin->setAuthor($item[4]);
            $plugin->setVersion($item[5]);
            $this->plugins[$plugin->getIdentifier()] = $plugin;
        }

        $allPluginsContainer = new AllPluginsContainer();
        $allPluginsContainer->setAllPlugins($this->plugins);

        $this->installer = new Installer($containerMock, $this->em, $allPluginsContainer);
    }

    protected function tearDown(): void
    {
        file_put_contents('nette.safe://' . Installer::FILE, $this->fileInstalledBackup);
    }

    public function testSuccessInstall(): void
    {
        $this->em
            ->shouldReceive('persist')->once()
            ->shouldReceive('flush')->once();

        //DELETE PLUGINS FROM INSTALLED
        $plugins = $this->plugins;
        $this->plugins = [];

        foreach ($plugins as $plugin) {
            $this->installer->install($plugin->getIdentifier());
        }
    }

    public function testInstallNotAvailable(): void
    {
        Assert::exception(function (): void {
            $this->installer->install('neexistuje/neexistuje');
        }, PluginInstallException::class, 'This plugin is not available, try remove and download again');
    }

    public function testInstallAlreadyInstalled(): void
    {
        foreach ($this->plugins as $plugin) {
            Assert::exception(function () use ($plugin): void {
                $this->installer->install($plugin->getIdentifier());
            }, PluginInstallException::class, 'This plugin is already installed');
        }
    }

    public function testUninstallNotInstalled(): void
    {
        Assert::exception(function (): void {
            $this->installer->uninstall('neexistuje/neexistuje');
        }, PluginInstallException::class, 'Plugin with this identifier is not installed');
    }

    private function findOneBy(string $identifier): ?Plugin
    {
        return $this->plugins[$identifier] ?? null;
    }

}

(new InstallerTest())->run();
