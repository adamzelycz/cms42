<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Plugins\Download;

use App\Helpers\FileSystem;
use Mockery;
use Plugins\AllPluginsContainer;
use Plugins\Api\ApiClient;
use Plugins\Download\Downloader;
use Plugins\Exceptions\PluginDownloadException;
use Plugins\Plugin;
use Psr\Http\Message\ResponseInterface;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../../bootstrap.php';

/**
 * @testCase
 * @phpExtension zip
 */
class DownloaderTest extends TestCase
{

    private const PLUGINS_ARRAY = [
        [1, 'Medvídek', 'medvidek', 'puu', 'Medvídek Pú je hlavní postava – personifikovaný medvídek v dětské knize', 'Walt Disney', '0'],
        [20, 'Oslík ze Shreka', 'shrek', 'oslik', 'Už tam budem?', 'Někdo', '2001'],
    ],
        PLUGINS_DIR_MOCK = __DIR__ . '/plugins',
        PLUGINS_TEMP_DIR_MOCK = __DIR__ . '/temp';

    /** @var Plugin[] */
    private $availablePlugins;

    /** @var Downloader */
    private $downloader;

    /** @var ApiClient */
    private $apiClient;

    /** @var AllPluginsContainer */
    private $allPluginsContainer;

    public function __construct()
    {
        FileSystem::createDir(self::PLUGINS_DIR_MOCK);
        FileSystem::createDir(self::PLUGINS_TEMP_DIR_MOCK);
    }

    protected function setUp(): void
    {
        foreach (self::PLUGINS_ARRAY as $item) {
            $plugin = new Plugin();
            $plugin->setId($item[0]);
            $plugin->setName($item[1]);
            $plugin->setIdentifier($item[2], $item[3]);
            $plugin->setDescription($item[4]);
            $plugin->setAuthor($item[5]);
            $plugin->setVersion($item[6]);
            $this->availablePlugins[$plugin->getIdentifier()] = $plugin;
        }
        $this->allPluginsContainer = new AllPluginsContainer();
//        $allPluginsContainer->setAllPlugins($this->availablePlugins);

        $this->apiClient = Mockery::mock(ApiClient::class);
        $this->apiClient->shouldReceive('get')
            ->once()
            ->andReturnUsing(function (string $url, array $parameters) {
                $zipFile = $parameters['sink'];
                touch($zipFile);
                return Mockery::mock(ResponseInterface::class);
            });
        $this->downloader = new Downloader(
            self::PLUGINS_DIR_MOCK,
            $this->apiClient,
            $this->allPluginsContainer,
            self::PLUGINS_TEMP_DIR_MOCK
        );
    }

    protected function tearDown(): void
    {
        FileSystem::purge(self::PLUGINS_DIR_MOCK);
    }

    public function testSuccessDownload(): void
    {
        $pluginCount = 0;
        Assert::equal($pluginCount, $this->countDownloadedPlugins());
        foreach ($this->availablePlugins as $plugin) {
            $this->downloader->download($plugin);
            $pluginCount++;
            Assert::equal($pluginCount, $this->countDownloadedPlugins());
            Assert::true(file_exists(self::PLUGINS_DIR_MOCK . '/' . $plugin->getIdentifier()));
        }
    }

    public function testAlreadyDownloaded(): void
    {
        $allPluginsContainer = new AllPluginsContainer();
        $allPluginsContainer->setAllPlugins($this->availablePlugins);
        $this->downloader = new Downloader(
            self::PLUGINS_DIR_MOCK,
            $this->apiClient,
            $allPluginsContainer,
            self::PLUGINS_TEMP_DIR_MOCK
        );

        foreach ($this->availablePlugins as $plugin) {
            Assert::exception(function () use ($plugin): void {
                $this->downloader->download($plugin);
            }, PluginDownloadException::class, 'This plugin is already downloaded');
        }
    }

    private function countDownloadedPlugins(): int
    {
        return \count($this->getDownloadedPlugins());
    }

    /** @return string[] */
    private function getDownloadedPlugins(): array
    {
        return array_diff(scandir(self::PLUGINS_DIR_MOCK, \SCANDIR_SORT_NONE), ['.', '..']);
    }

}

(new DownloaderTest())->run();
