<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Plugins;

use Nette\Application\IResponse;
use Nette\Application\Responses\TextResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class PluginsPresenterTest extends TestCase
{

    use TPresenter;

    protected function setUp(): void
    {
        $this->fakeLogin(1, 'admin');
    }

    public function testRenderDefault(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction(':Plugins:Admin:Plugins:default');
        $html = (string)$response->getSource();
        Assert::contains('">Plugins – Overview | CMS42 </title>', $html);
    }

    public function testRenderSettings(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction(':Plugins:Admin:Plugins:settings');
        $html = (string)$response->getSource();
        Assert::contains('">Plugins - Settings | CMS42 </title>', $html);
    }

}

(new PluginsPresenterTest())->run();
