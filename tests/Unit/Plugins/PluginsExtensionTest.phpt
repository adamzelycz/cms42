<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Plugins;

use Plugins\Install\InstallHelper;
use Test\Helpers\ContainerFactory;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class SettingsExtensionTest extends TestCase
{

    public function testPluginsFacadeHelperInjection(): void
    {
        $pluginsFacade = ContainerFactory::getContainer()->getByType(InstallHelper::class);
        Assert::type(InstallHelper::class, $pluginsFacade);
    }

}

(new SettingsExtensionTest())->run();
