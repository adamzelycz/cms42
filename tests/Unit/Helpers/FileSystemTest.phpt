<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Helpers;

use App\Helpers\FileSystem;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class FileSystemTest extends TestCase
{

    private const DIR = __DIR__ . '/filesystem';

    protected function setUp(): void
    {
        if (!file_exists(self::DIR)) {
            mkdir(self::DIR);
            mkdir(self::DIR . '/source');
            mkdir(self::DIR . '/source/podslozka');
            mkdir(self::DIR . '/destination');
            mkdir(self::DIR . '/random');

            touch(self::DIR . '/soubor0');
            touch(self::DIR . '/source/soubor1');
            touch(self::DIR . '/source/soubor2');
            touch(self::DIR . '/source/podslozka/soubor3');
        }
    }

    protected function tearDown(): void
    {
        if (file_exists(self::DIR)) {
            FileSystem::purge(self::DIR);
            @rmdir(self::DIR);
        }
    }

    public function testCopy(): void
    {
        FileSystem::copy(self::DIR . '/source', self::DIR . '/destination');
        Assert::equal(3, \count(glob(self::DIR . '/source/*')));
    }

    public function testCopyExceptions(): void
    {
        $source = self::DIR . '/neexistujici/cesta1';
        $destination = self::DIR . '/neexistujici/cesta2';
        Assert::exception(function () use ($source): void {
            FileSystem::copy($source, $source);
        },
            \RuntimeException::class, sprintf('Source path %s cannot be equal to Destination path %s', $source, $source));

        Assert::exception(function () use ($source, $destination): void {
            FileSystem::copy($source, $destination);
        },
            \RuntimeException::class, sprintf('Source path %s not exists', $source));
    }

    public function testPurge(): void
    {
        FileSystem::purge(self::DIR);
        Assert::equal([], glob(self::DIR . '/*'));
    }

    public function testPurgeException(): void
    {
        $dir = self::DIR . '/neexistujici/cesta';
        Assert::exception(function () use ($dir): void {
            FileSystem::purge($dir);
        },
            \RuntimeException::class, sprintf('Directory "%s" does not exists', $dir));
    }

}

(new FileSystemTest())->run();
