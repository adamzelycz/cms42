<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use Locale\Components\LocaleChanger\LocaleChanger;
use Locale\Locale;
use Locale\LocaleFacade;
use Mockery;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class LocaleChangerTest extends TestCase
{

    use TComponent;

    /** @var Locale[] */
    private $locales;

    public function __construct()
    {
        $this->locales = [
            new Locale('cs', 'čeština', true),
            new Locale('en', 'english'),
            new Locale('pl', 'polski'),
        ];
    }

    public function testRender(): void
    {
        $localeFacade = Mockery::mock(LocaleFacade::class);
        $localeFacade->shouldReceive('getAll')->once()->andReturn($this->locales);

        $control = new LocaleChanger($localeFacade);
        $html = $this->renderOutput($control);
        Assert::contains('<div class="locale-changer">', $html);
        Assert::contains('title="čeština">', $html);
        Assert::contains('title="english">', $html);
        Assert::contains('title="polski">', $html);
        Assert::contains('[cs]', $html);
        Assert::contains('[en]', $html);
        Assert::contains('[pl]', $html);
    }

}

(new LocaleChangerTest())->run();
