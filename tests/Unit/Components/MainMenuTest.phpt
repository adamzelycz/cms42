<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use Kdyby\Translation\Translator;
use Mockery;
use Navigation\Components\MainMenu\MainMenu;
use Navigation\ExternalUrl;
use Navigation\Menu;
use Navigation\MenuFacade;
use Navigation\MenuItem;
use Navigation\MenuItemTarget;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class MainMenuTest extends TestCase
{

    use TComponent;

    public function testRender(): void
    {
        $translator = Mockery::mock(Translator::class);
        $menuFacade = Mockery::mock(MenuFacade::class);
        $menu = new Menu();
        $menu->select();
        $item = new MenuItem();
        $item->setTarget(new MenuItemTarget(MenuItemTarget::BLANK));
        $item->setTitle('testicek');
        $item->setExternalUrl(new ExternalUrl('http://test.cz'));
        $menu->addItem($item);
        $menuFacade->shouldReceive('getSelectedMenu')
            ->andReturn($menu);
        $mainMenu = new MainMenu($translator, $menuFacade);
        $html = $this->renderOutput($mainMenu);
        Assert::contains('<nav role="navigation">', $html);
        Assert::contains('<ul class="nav-main-menu">', $html);
        Assert::contains('<li class="nav-item">', $html);
        Assert::contains('<a href="http://test.cz" target="_blank" class="nav-link">', $html);
    }

}

(new MainMenuTest())->run();
