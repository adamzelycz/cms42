<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

use App\Components\Meta\Meta;
use Mockery;
use Nette\Http\IRequest;
use Nette\Http\UrlScript;
use Settings\SettingsFacade;
use Test\Traits\TComponent;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class MetaTest extends TestCase
{

    use TComponent;

    protected function tearDown(): void
    {
        Mockery::close();
    }

    public function testMetaRender(): void
    {
        $request = Mockery::mock(IRequest::class);
        $request
            ->shouldReceive('getUrl')
            ->twice()
            ->andReturnUsing(function () {
                return new UrlScript('https://example.com/');
            });
        $settingsFacade = Mockery::mock(SettingsFacade::class);
        $settingsFacade
            ->shouldReceive('getOptionValue')
            ->times(4)
            ->andReturn('randomMockValue');
        $control = new Meta($request, $settingsFacade);

        $control->setMeta('test', 'testik');
        $control->setOg('test', 'testik');
        $output = $this->renderOutput($control);

        Assert::contains('<meta name="test" content="testik">', $output);
        Assert::contains('<meta property="test" content="testik">', $output);
        Assert::contains('https://example.com/android-chrome-384x384.png', $output);
    }

}

(new MetaTest())->run();
