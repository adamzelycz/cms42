<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Components;

require __DIR__ . '/../../bootstrap.php';

use App\Components\Title\Title;
use Mockery;
use Settings\SettingsFacade;
use Test\Traits\TComponent;
use Tester\TestCase;

/**
 * @testCase
 */
class TitleTest extends TestCase
{

    use TComponent;

    public function testTitleRenderWithoutTitle(): void
    {
        $settingsFacade = Mockery::mock(SettingsFacade::class);
        $settingsFacade
            ->shouldReceive('getOptionValue')
            ->withArgs(function ($key) {
                return $key === 'site_name' || $key === 'title_separator';
            })
            ->twice()
            ->andReturnUsing(function ($key) {
                return $key === 'site_name' ? 'CMS42' : '|';
            });

        $control = new Title($settingsFacade);
        $this->renderOutput($control, 'CMS42');
    }

    public function testTitleRenderWithSetTitle(): void
    {
        $settingsFacade = Mockery::mock(SettingsFacade::class);
        $settingsFacade
            ->shouldReceive('getOptionValue')
            ->withArgs(function ($key) {
                return $key === 'site_name' || $key === 'title_separator';
            })
            ->twice()
            ->andReturnUsing(function ($key) {
                return $key === 'site_name' ? 'CMS42' : '     
                      |    '; //přidání random tabů, spaců a zalomení
            });

        $control = new Title($settingsFacade);
        $control->setTitle('ahojQy');
        $this->renderOutput($control, 'ahojQy | CMS42');
    }

    public function testTitleRenderWithUnsetSeparatorAndSiteName(): void
    {
        $settingsFacade = Mockery::mock(SettingsFacade::class);
        $settingsFacade
            ->shouldReceive('getOptionValue')
            ->withArgs(function ($key) {
                return $key === 'site_name' || $key === 'title_separator';
            })
            ->twice()
            ->andReturnNull();

        $control = new Title($settingsFacade);
        $control->setTitle('ahojQy');
        $this->renderOutput($control, 'ahojQy');
    }

    protected function tearDown(): void
    {
        Mockery::close();
    }

}

(new TitleTest())->run();
