<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\AdminModule;

require __DIR__ . '/../../bootstrap.php';

use Nette\Application\Responses\TextResponse;
use Nette\Http\IResponse;
use Nette\Utils\Strings;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

/**
 * @testCase
 */
class DashboardPresenterTest extends TestCase
{

    use TPresenter;

    protected function setUp(): void
    {
        $this->fakeLogin(1, 'user');
    }

    public function testRenderDefault(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Dashboard:Admin:Dashboard:');

        $html = (string)$response->getSource();
        $html = Strings::replace($html, '#<svg[^>]*+>.+?</svg>#s', '');
        $title = DomQuery::fromHtml($html)->find('title')[0];
        Assert::contains('DashBoard', (string) $title);
    }

}

(new DashboardPresenterTest())->run();
