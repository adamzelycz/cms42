<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Pages\AdminModule;

use Nette\Application\IResponse;
use Nette\Application\Responses\TextResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../../bootstrap.php';

/**
 * @testCase
 */
class PagesPresenterTest extends TestCase
{

    use TPresenter;

    protected function setUp(): void
    {
        $this->fakeLogin(1, 'admin');
    }

    public function testRenderEdit(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction(':Pages:Admin:Pages:edit', 'GET', ['id' => 1]);
        $html = (string)$response->getSource();
        Assert::contains('Pages – Editor | CMS42 </title>', $html);
    }

}

(new PagesPresenterTest())->run();
