<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Settings;

use Nette\Application\IResponse;
use Nette\Application\Responses\TextResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class SettingsPresenterTest extends TestCase
{

    use TPresenter;

    protected function setUp(): void
    {
        $this->fakeLogin(1, 'admin');
    }

    public function testSettingsBar(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Settings:Admin:Settings:default');
        $html = (string)$response->getSource();
        $dom = DomQuery::fromHtml($html);
        $sideBar = $dom->find('.settings-bar')[0];
        $items = $sideBar->find('.settings-bar__item');
        $expected = [
            'Basic',
            'Permissions',
            'Roles and Resources',
            'Localization',
        ];
        for ($i = 0, $iMax = \count($items); $i < $iMax; $i++) {
            Assert::same($expected[$i], (string)$items[$i]);
        }
    }

    public function testRenderDefault(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Settings:Admin:Settings:default');
        $html = (string)$response->getSource();
        Assert::contains('<form action="/admin/settings" method="post" id="frm-settingsForm-form"', $html);
    }

    public function testRenderPermissions(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Settings:Admin:Settings:permissions');
        $html = (string)$response->getSource();
        Assert::contains('<form action="/admin/settings/permissions" method="post" id="frm-permissions-updateForm">', $html);
        Assert::contains('<form action="/admin/settings/permissions" method="post" id="frm-permissions-createForm">', $html);
    }

    public function testRenderRolesResources(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Settings:Admin:Settings:rolesResources');
        $html = (string)$response->getSource();
        Assert::contains('<form action="/admin/settings/roles-resources" method="post" class="form-inline form-inline" id="frm-roles-editForm-0">', $html);
        Assert::contains('<form action="/admin/settings/roles-resources" method="post" class="form-inline form-inline" id="frm-resources-newForm">', $html);
    }

}

(new SettingsPresenterTest())->run();
