<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Settings;

use Mockery;
use Nette\Forms\Form;
use Settings\Components\SettingsForm\SettingsForm;
use Settings\Option;
use Settings\Rules\Rule;
use Settings\SettingsFacade;
use Settings\Types\Color;
use Settings\Types\Email;
use Settings\Types\Text;
use Test\Traits\TComponent;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 * beforeCommit
 */
class SettingsFormTest extends TestCase
{

    use TComponent;
    use TPresenter;

    public function testRender(): void
    {
        $settingsFacade = Mockery::mock(SettingsFacade::class);
        $settingsFacade->shouldReceive('getOptions')
            ->once();
        $test1 = new Option('test1');
        $test1->setValue('valueTest1');

        $test2 = new Option('test2');
        $test2->setValue('#000000');
        $test2->setType(new Color());

        $test3 = new Option('test3');
        $test3->setValue('valuezest3@{tst.as');
        $test3->setType(new Email());

        $rule1 = new Rule();
        $rule1->setErrorMessage('Please type correct Date-Time format');
        $rule1->setValidator(Form::PATTERN);
        $rule1->setArgument('totally_fakin_date');
        $rule2 = new Rule();
        $rule2->setValidator(Form::REQUIRED);

        $test4 = new Option('datetime_format');
        $test4->setName('Date-Time format');
        $test4->setValue('j. n. Y G:i:s');
        $test4->addRule($rule1);
        $test4->addRule($rule2);
        $test4->setType(new Text());

        $html = $this->renderOutput(new SettingsForm([$test1, $test2, $test3, $test4], $settingsFacade));
        Assert::contains('<input type="text" name="test1" id="frm-SettingsForm-form-test1" value="valueTest1', $html);
        Assert::contains('input type="color" name="test2" class="h-50p form-control" id="frm-SettingsForm-form-test2"', $html);
        Assert::contains('input type="email" name="test3" id="frm-SettingsForm-form-test3"', $html);
        Assert::contains('input type="email" name="test3" id="frm-SettingsForm-form-test3"', $html);
        Assert::contains('<input type="text" name="datetime_format" pattern="totally_fakin_date" id="frm-SettingsForm-form-datetime_format" required', $html);
    }

}

(new SettingsFormTest())->run();
