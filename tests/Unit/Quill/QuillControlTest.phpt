<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Tests\Unit\Quill;

use Nette\Forms\Form;
use Quill\QuillControl;
use Tester\Assert;
use Tester\TestCase;
use function ob_get_clean;
use function ob_start;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class QuillControlTest extends TestCase
{

    public function testRender(): void
    {
        $form = new Form();
        $form->addComponent(new QuillControl([]), 'testContent');
        ob_start();
        $form->render();
        $html = ob_get_clean();
        Assert::same('<form action="" method="post">

<div data-config="[]"><input type="hidden" name="testContent" value=""><div class="quill-editor"></div></div><!--[if IE]><input type=IEbug disabled style="display:none"><![endif]-->
</form>
', $html);
    }

    public function testRenderWithData(): void
    {
        $form = new Form();
        $form->addComponent(new QuillControl([
            'modules' => [
                'toolbar' => [
                    [['font' => []]],
                    [['size' => ['small', false, 'large', 'huge']]],  // custom dropdown
//                [['header' => [1, 2, 3, 4, 5, 6, false]]],

                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    [['header' => 1], ['header' => 2]],               // custom button values
                    [['list' => 'ordered'], ['list' => 'bullet']],
                    [['script' => 'sub'], ['script' => 'super']],      // superscript/subscript
                    [['indent' => '-1'], ['indent' => '+1']],          // outdent/indent
                    [['direction' => 'rtl']],                         // text direction

                    [['color' => []], ['background' => []]],
                    [['align' => []]],

                    ['clean'],
                ],
            ],
            'theme' => 'snow',
        ]), 'testContentWithData');
        ob_start();
        $form->render();
        $html = ob_get_clean();
        Assert::same('<form action="" method="post">

<div data-config=\'{"modules":{"toolbar":[[{"font":[]}],[{"size":["small",false,"large","huge"]}],["bold","italic","underline","strike"],["blockquote","code-block"],[{"header":1},{"header":2}],[{"list":"ordered"},{"list":"bullet"}],[{"script":"sub"},{"script":"super"}],[{"indent":"-1"},{"indent":"+1"}],[{"direction":"rtl"}],[{"color":[]},{"background":[]}],[{"align":[]}],["clean"]]},"theme":"snow"}\'><input type="hidden" name="testContentWithData" value=""><div class="quill-editor"></div></div><!--[if IE]><input type=IEbug disabled style="display:none"><![endif]-->
</form>
', $html);
    }

}

(new QuillControlTest())->run();
