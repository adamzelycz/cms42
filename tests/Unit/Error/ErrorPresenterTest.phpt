<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Error;

use Nette\Application\BadRequestException;
use Nette\Application\IResponse;
use Nette\Application\Responses\TextResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class ErrorPresenterTest extends TestCase
{

    use TPresenter;

//    public function test404(): void
//    {
//        $exception = new BadRequestException('Random Message', 404);
//        /** @var IResponse|TextResponse $response */
//        $response = $this->checkAction('Error:Error:default', 'GET', ['locale' => 'cs', 'exception' => $exception]);
//
//        $html = (string)$response->getSource();
//        Assert::contains('Stránka nebyla nalezena', $html);
//        Assert::contains('Error 404', $html);
//    }

//    public function test403(): void
//    {
//        $exception = new BadRequestException('Random Message', 403);
//        /** @var IResponse|TextResponse $response */
//        $response = $this->checkAction('Error:Error:default', 'GET', ['locale' => 'cs', 'exception' => $exception]);
//
//        $html = (string)$response->getSource();
//        Assert::contains('Přístup zamítnut', $html);
//        Assert::contains('Error 403', $html);
//    }

//    public function test405(): void
//    {
//        $exception = new BadRequestException('Random Message', 405);
//        /** @var IResponse|TextResponse $response */
//        $response = $this->checkAction('Error:Error:default', 'GET', ['locale' => 'cs', 'exception' => $exception]);
//
//        $html = (string)$response->getSource();
//        Assert::contains('Metoda není povolena', $html);
//        Assert::contains('Error 405', $html);
//    }

//    public function test410(): void
//    {
//        $exception = new BadRequestException('Random Message', 410);
//        /** @var IResponse|TextResponse $response */
//        $response = $this->checkAction('Error:Error:default', 'GET', ['locale' => 'cs', 'exception' => $exception]);
//
//        $html = (string)$response->getSource();
//        Assert::contains('Stránka nebyla nalezena', $html);
//        Assert::contains('Error 410', $html);
//    }

    public function test500(): void
    {
        $exception = new BadRequestException('Random Message', 500);
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Error:Error:default', 'GET', ['locale' => 'en', 'exception' => $exception]);

        $html = (string)$response->getSource();
        Assert::contains('Server Error', $html);
        Assert::contains('Error 500', $html);
    }

}

(new ErrorPresenterTest())->run();
