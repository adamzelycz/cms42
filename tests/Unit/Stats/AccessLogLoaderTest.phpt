<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Stats;

use Nette\Utils\FileSystem;
use Stats\Loader\AccessLogLoader;
use Stats\Stat;
use Tester\Assert;
use Tester\TestCase;

require __DIR__ . '/../../bootstrap.php';

/** @testCase */
class AccessLogLoaderTest extends TestCase
{

    public function testLoad(): void
    {
        $loader = new AccessLogLoader(__DIR__ . '/access_test.log');
        $stats = $loader->load();
        Assert::count(171, $stats);
        /** @var Stat $stat */
        foreach ($stats as $stat) {
            Assert::true($stat instanceof Stat);
        }
    }

    public function testClean(): void
    {
        $fileName = __DIR__ . '/access_clear.log';
        FileSystem::copy(__DIR__ . '/access_test.log', $fileName);
        Assert::count(171, file($fileName));

        $loader = new AccessLogLoader($fileName);
        $loader->clean();
        Assert::falsey(file_get_contents($fileName));

        FileSystem::delete($fileName);
    }

}

(new AccessLogLoaderTest())->run();
