<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\FrontModule;

require __DIR__ . '/../../bootstrap.php';

use Nette\Application\Responses\TextResponse;
use Nette\Http\IResponse;
use Test\Traits\TPresenter;
use Tester\Assert;
use Tester\DomQuery;
use Tester\TestCase;

/**
 * @testCase
 */
class HomepagePresenterTest extends TestCase
{

    use TPresenter;

    public function testRenderDefault(): void
    {
        /** @var IResponse|TextResponse $response */
        $response = $this->checkAction('Front:Homepage:default', 'GET', ['locale' => 'en']);

        $html = (string)$response->getSource();
        $title = DomQuery::fromHtml($html)->find('title')[0];
        Assert::contains('Úvodní stránka', (string) $title);
    }

}

(new HomepagePresenterTest())->run();
