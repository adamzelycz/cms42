<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\Router;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Locale\Locale;
use Mockery;
use Monolog\Logger;
use Nette\Application\Request as AppRequest;
use Nette\Caching\Storages\DevNullStorage;
use Nette\Http\IRequest as HttpRequest;
use Nette\Http\UrlScript;
use Tester\Assert;
use Tester\TestCase;
use Url\Router;
use Url\Url as UrlEntity;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class RouterTest extends TestCase
{

    /** @var UrlEntity[] */
    private $routingTable;

    protected function setUp(): void
    {
        $this->routingTable = $this->getRoutingTable();
    }

    protected function tearDown(): void
    {
        Mockery::close();
    }

    /** @return \Generator|UrlScript[] */
    public function getLocaleUrls()
    {
        $urls = [
            new UrlScript('http://fake.cz/en/fake1/?fake2=fake2&fake3=fake5', '/'),
            new UrlScript('https://fake.cz/en/fake2?fake2=fake2&fake3=fake5', '/'),
            new UrlScript('http://www.fake.cz/cs/fake1', '/'),
            new UrlScript('https://www.fake.cz/cs/fake1', '/'),
            new UrlScript('https://www.fake.cz/cs/', '/'),
            new UrlScript('https://www.fake.cz/en/', '/'),
            new UrlScript('https://www.fake.cz/cs', '/'),
            new UrlScript('https://www.fake.cz/en', '/'),
        ];
        foreach ($urls as $url) {
            yield [$url];
        }
    }

    /** @return \Generator|UrlScript[] */
    public function getNonExistingUrls()
    {
        $urls = [
            new UrlScript('http://fake.cz/en/notexists1/?notexists2=notexists2&notexists3=notexists5', '/'),
            new UrlScript('https://notexists.cz/en/notexists2?notexists2=notexists2&notexists3=notexists5', '/'),
            new UrlScript('http://www.notexists.cz/cs/notexists1', '/'),
            new UrlScript('https://www.notexists.cz/cs/notexists1', '/'),
            new UrlScript('https://www.notexists.cz/en/notexists1', '/'),
            new UrlScript('https://www.nists.cz/nists/eg/ag', '/'),
        ];
        foreach ($urls as $url) {
            yield [$url];
        }
    }

    /** @return \Generator|UrlScript[] */
    public function getClassicUrls()
    {
        $urls = [
            new UrlScript('http://fake.cz/fake1/?fake2=fake2&fake3=fake5', '/'),
            new UrlScript('https://fake.cz/fake3?fake2=fake2&fake3=fake5', '/'),
            new UrlScript('http://www.fake.cz/fake1', '/'),
            new UrlScript('https://www.fake.cz/fake2/', '/'),
            new UrlScript('https://www.fake.cz/', '/'),
            new UrlScript('https://www.fake.cz', '/'),
        ];
        foreach ($urls as $url) {
            yield [$url];
        }
    }

    /** @return \Generator|UrlScript[] */
    public function getRedirectUrls()
    {
        $urls = [
            new UrlScript('http://fake.cz/fake-redirect-from?fake2=fake2&fake3=fake5', '/'),
            new UrlScript('https://www.fake.cz/en/fake-redirect-from', '/'),
            new UrlScript('https://www.fake.cz/cs/fake-redirect-from', '/'),
            new UrlScript('https://www.fake.cz/fake-redirect-from', '/'),
        ];
        foreach ($urls as $url) {
            yield [$url];
        }
    }

    /** @dataProvider getLocaleUrls */
    public function testWithLocale(UrlScript $url): void
    {
        $request = $this->match($url);
        $path = ltrim($url->getPath(), '/');
        $pos = \strpos($path, '/');
        $locale = substr($path, 0, $pos ?: \strlen($path));
        Assert::same($request->getParameter('locale'), $locale);
    }

    /** @dataProvider getNonExistingUrls */
    public function testNonExists(UrlScript $url): void
    {
        $request = $this->match($url);
        Assert::null($request);
    }

    /** @dataProvider getClassicUrls */
    public function testClassic(UrlScript $url): void
    {
        $this->match($url);
    }

    /** @dataProvider getRedirectUrls */
    public function testRedirect(UrlScript $url): void
    {
        $this->match($url);
    }

    private function match(UrlScript $url): ?AppRequest
    {
        $localeRepository = Mockery::mock(EntityRepository::class);
        $localeRepository
            ->shouldReceive('findPairs')
            ->once()
            ->andReturn(['cs' => true, 'en' => false]);

        $urlRepository = Mockery::mock(EntityRepository::class);
        /** @var UrlEntity|null $urlEntity */
        $urlRepository
            ->shouldReceive('findOneBy')
            ->once()
            ->andReturnUsing(function ($params) use (&$urlEntity) {
                $urlPath = array_shift($params);
                $urlEntity = $this->routingTable[$urlPath] ?? null;
                return $urlEntity;
            });

        $em = Mockery::mock(EntityManager::class);
        $em
            ->shouldReceive('getRepository')
            ->twice()
            ->andReturnUsing(function ($parameter) use ($localeRepository, $urlRepository) {
                if ($parameter === Locale::class) {
                    return $localeRepository;
                }
                if ($parameter === UrlEntity::class) {
                    return $urlRepository;
                }
                return null;
            });

        $monolog = Mockery::mock(Logger::class);
        $monolog
            ->shouldReceive('addError');

        $storage = new DevNullStorage();

        $router = new Router($em, $monolog, $storage);

        parse_str($url->getQuery(), $query);
        $httpRequest = Mockery::mock(HttpRequest::class);
        $httpRequest
            ->shouldReceive([
                'getUrl' => $url,
                'getRemoteAddress' => '42.42.42.42',
                'getQuery' => $query,
                'getMethod' => 'GET',
                'getPost' => [],
                'getFiles' => [],
                'isSecured' => false,
            ]);

        $appRequest = $router->match($httpRequest);
        if ($appRequest === null) {
            Assert::null($urlEntity);
        } else {
            if ($urlEntity->getRedirect()) {
                $urlEntity = $urlEntity->getRedirect();
            }
            Assert::same(
                $appRequest->getPresenterName() . ':' . $appRequest->getParameter('action'),
                $urlEntity->getDestination()
            );
        }

        return $appRequest;
    }

    /** @return UrlEntity[] */
    private function getRoutingTable(): array
    {
        $fakeUrls = $this->createFakeUrls();
        $urls = [];
        /** @var UrlEntity $url */
        foreach ($fakeUrls as $url) {
            $urls[$url->getUrlPath()] = $url;
        }
        return $urls;
    }

    /** @return UrlEntity[] */
    private function createFakeUrls(): array
    {
        $fake1 = new UrlEntity();
        $fake1->setDestination('Fake1', 'fake1');
        $fake1->setUrlPath('fake1');

        $fake2 = new UrlEntity();
        $fake2->setDestination('Fake2', 'fake2');
        $fake2->setUrlPath('fake2');

        $fake3 = new UrlEntity();
        $fake3->setDestination('Fake3', 'fake3');
        $fake3->setUrlPath('fake3');

        $fake4 = new UrlEntity();
        $fake4->setDestination('Fake4', 'fake4');
        $fake4->setUrlPath('');

        $fakeRedirectTo = new UrlEntity();
        $fakeRedirectTo->setDestination('FakeRedirectTo', 'fakeRedirectTo');
        $fakeRedirectTo->setUrlPath('fake-redirect-to');

        $fakeRedirectFrom = new UrlEntity();
        $fakeRedirectFrom->setDestination('FakeRedirectFrom', 'fakeRedirectFrom');
        $fakeRedirectFrom->setUrlPath('fake-redirect-from');
        $fakeRedirectFrom->setRedirect($fakeRedirectTo);

        return [$fake1, $fake2, $fake3, $fake4, $fakeRedirectFrom, $fakeRedirectTo];
    }

}

(new RouterTest())->run();
