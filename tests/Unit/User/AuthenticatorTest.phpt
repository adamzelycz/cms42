<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Unit\User;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Mockery;
use Nette\Http\Request;
use Nette\Security\AuthenticationException;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Security\Authenticator;
use Security\Role;
use Tester\Assert;
use Tester\TestCase;
use Users\User;

require __DIR__ . '/../../bootstrap.php';

/**
 * @testCase
 */
class AuthenticatorTest extends TestCase
{

    /** @var User[] */
    private $repository = [];

    protected function setUp(): void
    {
        $role = new Role();
        $role->setName('rohlik');
        $user2 = new User('user2', Passwords::hash('user2user2'));
        $user2->addRole($role);
        $this->repository = [
            'user1' => new User('user1', Passwords::hash('user1user1')),
            'user2' => $user2,
        ];
    }

    protected function tearDown(): void
    {
        Mockery::close();
    }

    /**
     * @dataProvider getValid
     */
    public function testAuthenticateSuccess(array $user): void
    {
        $this->authenticate($user);
    }

    /**
     * @dataProvider getBadPassword
     */
    public function testAuthenticateBadPassword(array $user): void
    {
        Assert::exception(function () use ($user): void {
            $this->authenticate($user);
        }, AuthenticationException::class, 'exceptions.authentication.badPassword');
    }

    /**
     * @dataProvider getBadLogin
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.UselessDocComment
     */
    public function testAuthenticateBadLogin(array $user): void
    {
        Assert::exception(function () use ($user): void {
            $this->authenticate($user);
        }, AuthenticationException::class, 'exceptions.authentication.badLogin');
    }

    private function authenticate(array $user): IIdentity
    {
        $userRepository = Mockery::mock(EntityRepository::class);
        $userRepository
            ->shouldReceive('findOneBy')
            ->once()
            ->andReturnUsing(function (array $parameters) {
                return $this->findOneBy(array_shift($parameters));
            });

        $em = Mockery::mock(EntityManager::class);
        $em->shouldReceive('getRepository')
            ->once()
            ->andReturn($userRepository);
        $em->shouldReceive('persist');
        $em->shouldReceive('flush');

        $request = Mockery::mock(Request::class);
        $request->shouldReceive('getRemoteAddress')
            ->andReturn('127.0.0.1');
        $authenticator = new Authenticator($em, $request);

        $roles = array_key_exists(2, $user) ? array_pop($user) : [];

        $identity = $authenticator->authenticate($user);
        Assert::equal($identity->getRoles(), $roles);
        return $identity;
    }

    private function findOneBy(string $username): ?User
    {
        return $this->repository[$username] ?? null;
    }

    /** @return \Generator|string[] */
    public function getValid()
    {
        $users = [
            ['user1', 'user1user1'],
            ['user2', 'user2user2', ['rohlik']],
        ];
        foreach ($users as $user) {
            yield [$user];
        }
    }

    /** @return \Generator|string[] */
    public function getBadPassword()
    {
        $users = [
            ['user1', 'badPass'],
            ['user2', 'badPass', ['rohlik']],
        ];
        foreach ($users as $user) {
            yield [$user];
        }
    }

    /** @return \Generator|string[] */
    public function getBadLogin()
    {
        $users = [
            ['badLogin1', 'user1user1'],
            ['badLogin2', 'user2user2', ['rohlik']],
        ];
        foreach ($users as $user) {
            yield [$user];
        }
    }

}

(new AuthenticatorTest())->run();
