<?php declare(strict_types=1);

/**
 * Copyright (C) 2018. Adam Zelycz – https://www.adamzelycz.cz
 */

namespace Test\Mocks;

use Nette\Application\Request;

class ApplicationRequestMock extends Request
{

    /**
     * @param array|mixed[] $params
     * @param array|mixed[] $post
     * @param array|mixed[] $files
     * @param array|mixed[] $flags
     */
    public function __construct(?string $name = null, ?string $method = null, array $params = [], array $post = [], array $files = [], array $flags = [])
    {
        $name = $name ?: 'Foo';
        parent::__construct($name, $method, $params, $post, $files, $flags);
    }

}
